import java.util.ArrayList;
import java.util.List;

public class AppMain {

  public static void main(String[] args) {

    //creating List of persons
    List<PersonSimple> personSimpleList = new ArrayList<>();

    //adding new objects inline
    personSimpleList.add(new PersonSimple());
    personSimpleList.add(new PersonSimple());

    PersonSimple p2 = new PersonSimple();

    //adding previously created object
    personSimpleList.add(p2);

    System.out.println(personSimpleList.size());

    personSimpleList.remove(1);
    personSimpleList.remove(p2);

    System.out.println(personSimpleList.size());

    //creating list of strings
    List<String> stringList = new ArrayList<>();

    //adding strings
    stringList.add("One");
    stringList.add("Two");
    stringList.add("Three");

    System.out.println(stringList.size());

  }

}
