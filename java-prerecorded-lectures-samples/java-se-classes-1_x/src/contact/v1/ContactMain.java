package contact.v1;

public class ContactMain {

  public static void main(String[] args) {

    Contact contact = new Contact();

    contact.name = "Alex";
    contact.surname = "Mc Genry";
    contact.phoneNumber = "+180111234567";
    contact.address = "Oxford Str. 123";

    System.out.println(contact.name);
    System.out.println(contact.surname);
    System.out.println(contact.phoneNumber);
    System.out.println(contact.address);

  }
}
