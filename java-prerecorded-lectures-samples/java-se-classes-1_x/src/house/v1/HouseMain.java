package house.v1;

public class HouseMain {

  public static void main(String[] args) {

    House house1 = new House();

    house1.address = "Oxford street 124";
    house1.color = "red";
    house1.roomsNumber = 4;
    house1.windowsNumber = 5;

    House house2 = new House();

    house2.address = "Oxford street 125";
    house2.color = "red";
    house2.roomsNumber = 4;
    house2.windowsNumber = 5;

    House house3 = new House();

    house3.address = "Oxford street 126";
    house3.color = "red";
    house3.roomsNumber = 4;
    house3.windowsNumber = 5;

    System.out.println(house1.address+", "+house1.color+", "+house1.roomsNumber+", "+house1.windowsNumber);
    System.out.println(house2.address+", "+house2.color+", "+house2.roomsNumber+", "+house2.windowsNumber);
    System.out.println(house3.address+", "+house3.color+", "+house3.roomsNumber+", "+house3.windowsNumber);

  }
}
