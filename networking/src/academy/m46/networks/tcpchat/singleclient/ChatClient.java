package academy.m46.networks.tcpchat.singleclient;

import java.io.*;
import java.net.Socket;

public class ChatClient {
    public static void main(String[] args) throws IOException {

        Socket clientSocket = new Socket("127.0.0.1",4444);

        PrintWriter networkWriter = new PrintWriter(clientSocket.getOutputStream(), true);

        BufferedReader networkReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in)); //stdIn - Standard Input

        while(true) {
            String line = stdIn.readLine();
            networkWriter.write(line+"\n");
            networkWriter.flush();
            String reply = networkReader.readLine();
            System.out.println("Server replied: "+reply);
            if (line.equals("exit")) {
                break;
            }
        }

        networkWriter.close();
        clientSocket.close();

    }
}
