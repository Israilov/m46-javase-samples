package academy.m46.networks.tcpchat.singleclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(4444);

        Socket socket = serverSocket.accept();

        System.out.println("Connection accepted");

        BufferedReader networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        PrintWriter networkWriter = new PrintWriter(socket.getOutputStream(), true);

        while (true) {
            String line = networkReader.readLine();
            System.out.println("Received command: " + line);
            networkWriter.write("dumb reply\n");
            networkWriter.flush();
            if (line.equals("exit")) {
                break;
            }
        }

        networkReader.close();
        socket.close();
        serverSocket.close();

    }

}