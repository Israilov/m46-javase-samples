package academy.m46.networks.tcpchat.conferencechat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConferenceClientMessageAcceptor extends Thread {

    private Socket socket;
    private ConferenceMessageDistributor distributor;

    public ConferenceClientMessageAcceptor(Socket socket, ConferenceMessageDistributor distributor) {
        this.socket = socket;
        this.distributor = distributor;
    }

    public void run() {

        try {

            BufferedReader networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (true) {
                String line = networkReader.readLine();
                System.out.println(Thread.currentThread().getId()+": Received command: " + line);
                if (line.trim().length()>0) {
                    distributor.putMessage(line);
                }
                if (line.equals("exit")) {
                    break;
                }
            }

            networkReader.close();
            socket.close();

        }catch(Exception e) {
            e.printStackTrace();
        }

    }
}
