package academy.m46.networks.tcpchat.conferencechat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConferenceClientReceiver {

    public static void main(String[] args) throws IOException {

        Socket clientSocket = new Socket("127.0.0.1",4444);

        BufferedReader networkReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        //TODO for students lauch two threads: 1) read 2) write > two classes: sender, receiver
        while(true) {
            String reply = networkReader.readLine();
            System.out.println("Server replied: "+reply);
            if (reply.equals("exit")) {
                break;
            }
        }

        networkReader.close();
        clientSocket.close();

    }
}
