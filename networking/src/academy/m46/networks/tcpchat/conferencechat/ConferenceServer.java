package academy.m46.networks.tcpchat.conferencechat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConferenceServer {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(4444);
        ConferenceMessageDistributor messageDistributor = new ConferenceMessageDistributor();

        while(true) {
            Socket socket = serverSocket.accept();
            ConferenceClientMessageAcceptor messageAcceptor = new ConferenceClientMessageAcceptor(socket, messageDistributor);
            messageAcceptor.start();
            messageDistributor.addOutputStream(socket.getOutputStream());
            System.out.println("Connection accepted "+socket.toString());
        }

    }

}
