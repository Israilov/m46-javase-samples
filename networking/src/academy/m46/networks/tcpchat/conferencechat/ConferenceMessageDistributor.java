package academy.m46.networks.tcpchat.conferencechat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ConferenceMessageDistributor {

    List<OutputStream> outputStreamsList = new ArrayList<OutputStream>();

    public void addOutputStream(OutputStream outputStream) throws IOException {
        outputStreamsList.add(outputStream);
    }

    public void putMessage(String line) {
        for (OutputStream out: outputStreamsList) {
            PrintWriter printWriter = new PrintWriter(out);
            printWriter.write(line+"\n");
            printWriter.flush();
        }
    }

}
