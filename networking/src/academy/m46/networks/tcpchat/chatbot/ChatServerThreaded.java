package academy.m46.networks.tcpchat.chatbot;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServerThreaded {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(4444);

        while(true) {
            Socket socket = serverSocket.accept();
            System.out.println("Connection accepted"+socket.toString());
            DumbChatBot chatBot = new DumbChatBot(socket);
            chatBot.start();
        }

    }

}
