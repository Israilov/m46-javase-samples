package academy.m46.networks.tcpchat.chatbot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class DumbChatBot extends Thread {

    private Socket socket;

    public DumbChatBot(Socket socket) {
        this.socket = socket;
    }

    public void run() {

        try {

            BufferedReader networkReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter networkWriter = new PrintWriter(socket.getOutputStream(), true);

            while (true) {
                String line = networkReader.readLine();
                System.out.println(Thread.currentThread().getId()+": Received command: " + line);
                networkWriter.write("Thank you for your message! We will reply soon!\n");
                networkWriter.flush();
                if (line.equals("exit")) {
                    break;
                }
            }

            networkReader.close();
            socket.close();

        }catch(Exception e) {
            e.printStackTrace();
        }

    }

}
