package academy.m46.networks.udp.multicast;

/*Taken from docs.oracle.com*/
public class MulticastServer {
    public static void main(String[] args) throws java.io.IOException {
        new MulticastServerThread().start();
    }
}