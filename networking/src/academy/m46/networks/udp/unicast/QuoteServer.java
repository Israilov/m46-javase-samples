package academy.m46.networks.udp.unicast;

import java.io.*;

/*Taken from docs.oracle.com*/
public class QuoteServer {
    public static void main(String[] args) throws IOException {
        new QuoteServerThread().start();
    }
}