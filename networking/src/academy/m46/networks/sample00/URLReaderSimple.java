package academy.m46.networks.sample00;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class URLReaderSimple {

    public static void main(String[] args) throws IOException {

        URL oracleWebSite = new URL("https://www.oracle.com/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(oracleWebSite.openStream()));

        String inputLine;
        while ((inputLine = bufferedReader.readLine())!=null) {
            //System.out.println(inputLine);

            if (inputLine.contains("<div")) {
                System.out.println(inputLine);
            }
        }

        bufferedReader.close();

    }

}