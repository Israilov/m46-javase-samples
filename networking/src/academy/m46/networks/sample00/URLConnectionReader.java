package academy.m46.networks.sample00;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionReader {

    public static void main(String[] args) {

        URL myUrl = null;
        try {

            myUrl = new URL("http://www.example.com");
            URLConnection myUrlConnection = myUrl.openConnection();
            myUrlConnection.connect();

            System.out.println("Successfully connected to "+myUrl.getHost()+".");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(myUrlConnection.getInputStream()));

            String line;
            while((line = bufferedReader.readLine())!=null) {
                System.out.println(line);
            }

            System.out.println("Reading from "+myUrl.getHost()+" completed.");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
