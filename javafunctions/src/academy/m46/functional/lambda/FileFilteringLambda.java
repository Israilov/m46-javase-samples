package academy.m46.functional.lambda;

import java.io.File;
import java.io.FileFilter;

public class FileFilteringLambda {

  public static void main(String[] args) {

    File dir = new File("./");

    File[] files = dir.listFiles(getFilter("java"));

    for (File f: files) {
      System.out.println(f);
    }

    dir.listFiles(getFilter("txt"));
  }

  public static FileFilter getFilter(String fileSuffix) {
    return p -> p.getName().endsWith(fileSuffix);
  }
}
