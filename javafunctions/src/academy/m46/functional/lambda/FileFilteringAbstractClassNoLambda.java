package academy.m46.functional.lambda;

import java.io.File;
import java.io.FileFilter;

public class FileFilteringAbstractClassNoLambda {

  public static void main(String[] args) {

    FileFilter filter = new FileFilter() {
      int count=0;
      @Override
      public boolean accept(File pathname) {
        count++;
        return pathname.getName().endsWith(".java");
      }
    };

    File dir = new File("./");

    File[] files = dir.listFiles(filter);

    for (File f: files) {
      System.out.println(f);
    }

  }
}
