package academy.m46.functional.lambda;

import java.io.File;
import java.io.FileFilter;

public class FileFilteringSeparateClassNoLambda {

  public static void main(String[] args) {

    new FileFilteringSeparateClassNoLambda().start();

  }

  private void start() {

    FileFilter filter = new OnlyTxtFilesFilter();

    File dir = new File("./src/academy/m46/functional/lambda/");

    File[] files = dir.listFiles(filter);

    for (File f : files) {
      System.out.println(f);
    }

  }

  class OnlyJavaFilesFilter implements FileFilter {

    int count = 0;

    public boolean accept(File pathname) {
      count++;
      return pathname.getName().endsWith(".java");
    }
  }

  class OnlyTxtFilesFilter implements FileFilter {

    public boolean accept(File pathname) {
      return pathname.getName().endsWith(".txt");
    }
  }

}
