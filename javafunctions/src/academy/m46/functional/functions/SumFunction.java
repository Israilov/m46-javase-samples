package academy.m46.functional.functions;

import java.util.function.BiFunction;

public class SumFunction {

  public static void main(String[] args) {

    final Tmp t = new Tmp();

    BiFunction<Integer, Integer, Long> sum = (a, b) -> Integer.toUnsignedLong(t.count + a + b);

    System.out.println(sum.apply(50, 50));

    t.count++;

    System.out.println(sum.apply(50, 50));

  }

  static class Tmp {
    public int count = 0;
  }

}
