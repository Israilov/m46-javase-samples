package academy.m46.functional.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Here we show how to execute single function on each stream element
 */
public class MapListsOfListsSize {

  public static void main(String[] args) {

    List<Integer> list1 = Arrays.asList(1,2,3,4);
    List<Integer> list2 = Arrays.asList(5,6,7,8);
    List<Integer> list3 = Arrays.asList(9,10,11,12);

    List<List<Integer>> list = Arrays.asList(list1, list2, list3);

    Function<List<?>, Integer> size = List::size;

    list.stream().map(size).forEach(System.out::print);

    System.out.println();
    System.out.println(list);

  }

}
