package academy.m46.functional.stream;

import java.util.Arrays;
import java.util.List;

/*
 * it is incorrect in the sense that result should not be 0 - it just does not exist, since list
 * is empty
 */
public class ReductionSumIncorrectBecauseListEmpty {

  public static void main(String[] args) {

    List<Integer> list = Arrays.asList();

    System.out.println(
        list
            .stream()
            .reduce(0, Integer::max)
    );

  }

}
