package academy.m46.functional.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Here we just take a series of collections and create a single stream
 */
public class MapListsOfListsSizeFlattened {

  public static void main(String[] args) {

    List<Integer> list1 = Arrays.asList(1,2,3,4);
    List<Integer> list2 = Arrays.asList(5,6,7,8);
    List<Integer> list3 = Arrays.asList(9,10,11,12);

    List<List<Integer>> list = Arrays.asList(list1, list2, list3);

    Function<List<Integer>, Stream<?>> streamify = List::stream;

    list.stream().flatMap(streamify).forEach(System.out::print);

    System.out.println();
    System.out.println(list);

  }

}
