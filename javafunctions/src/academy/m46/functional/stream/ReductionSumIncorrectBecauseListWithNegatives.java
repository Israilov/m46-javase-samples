package academy.m46.functional.stream;

import java.util.Arrays;
import java.util.List;

/*
 * it is incorrect in the sense that result should not be 0 - since list contain negative values,
 * and max should be selected of them
 */
public class ReductionSumIncorrectBecauseListWithNegatives {

  public static void main(String[] args) {

    List<Integer> list = Arrays.asList(-10, -10);

    System.out.println(
        list
            .stream()
            .reduce(0, Integer::max)
    );

  }

}
