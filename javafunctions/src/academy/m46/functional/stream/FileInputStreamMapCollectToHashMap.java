package academy.m46.functional.stream;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileInputStreamMapCollectToHashMap {

  public static void main(String[] args) {
    new FileInputStreamMapCollectToHashMap().start();
  }

  public void start() {

    File f = new File("./");

    System.out.println(f.getAbsoluteFile());

    try (
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                FileInputStreamMapCollectToHashMap.class.getResourceAsStream(
                    "fileinputstreammapcollect.in"
                )));

        Stream<String> stream = reader.lines();
    ) {

      List<Person> personList
          = stream
          .map(l -> {
            String[] pair = l.split(" ");
            Person p = new Person(pair[0], Integer.valueOf(pair[1]));
            return p;
          })
          .collect(Collectors.toList());

      System.out.println(personList);

      Map<Integer, List<Person>> avgAges
          = personList
          .stream()
          .collect(Collectors.groupingBy(Person::getAge));

      System.out.println(avgAges);

      Map<Integer, List<String>> avgAgesNames
          = personList
          .stream()
          .collect(
              Collectors.groupingBy(Person::getAge,
                  Collectors.mapping(Person::getName, Collectors.toCollection(
                      ArrayList::new))));

      System.out.println(avgAgesNames);

      System.out.println(avgAges);

      Map<Integer, String> avgNamesStrJoined
          = personList
          .stream()
          .collect(
              Collectors.groupingBy(Person::getAge,
                  Collectors.mapping(
                      Person::getName,
                      Collectors.joining(", ")
                  )
              )
          );

      System.out.println(avgNamesStrJoined);


    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  class Person {

    public String name;
    public int age;

    public Person() {

    }

    public Person(String name, int age) {
      this.name = name;
      this.age = age;
    }

    public Integer getAge() {
      return age;
    }

    public void setAge(Integer age) {
      this.age = age.intValue();
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String toString() {
      return "{" + name + ", " + age + "}";
    }
  }

}

