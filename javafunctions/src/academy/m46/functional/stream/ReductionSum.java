package academy.m46.functional.stream;

import java.util.Arrays;

public class ReductionSum {

  public static void main(String[] args) {

    System.out.println(
        Arrays
            .asList(1, 2, 3, 4, 5, 6, 7, 8)
            .stream()
            .reduce(0, Integer::sum)
    );

  }

}
