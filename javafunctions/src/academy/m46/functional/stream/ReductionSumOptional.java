package academy.m46.functional.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/*
 * This is a correct version of reduction for max - use Optional without default identity element
 */
public class ReductionSumOptional {

  public static void main(String[] args) {

    List<Integer> list = Arrays.asList();

    Optional<Integer> max = list.stream().reduce(Integer::max);

    System.out.println(max);

  }

}
