package academy.m46.functional.stream;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileInputStreamMapCollect {

  public static void main(String[] args) {
    new FileInputStreamMapCollect().start();
  }

  public void start() {

    File f = new File("./");

    System.out.println(f.getAbsoluteFile());

    try (
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                FileInputStreamMapCollect.class.getResourceAsStream(
                    "fileinputstreammapcollect.in"
                )
            )
        );

        Stream<String> stream = reader.lines();
    ) {

      List<Person> personList
          = stream
          .peek(System.out::println)
          .map(line -> {
            String[] pair = line.split(" ");
            Person p = new Person(pair[0], Integer.valueOf(pair[1]));
            return p;
          })
          .peek(p -> System.out.println(p.name))
          .collect(Collectors.toList());

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  class Person {

    public String name;
    public int age;

    public Person() {

    }

    public Person(String name, int age) {
      this.name = name;
      this.age = age;
    }

    public String toString() {
      return name + ", " + age;
    }
  }

}

