package academy.m46.functional.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReductionMinCollectionOptional {

  public static void main(String[] args) {
    new ReductionMinCollectionOptional().start();
  }

  public void start() {

    try (
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                ReductionMinCollectionOptional.class.getResourceAsStream(
                    "fileinputstreammapcollect.in"
                )));

        Stream<String> stream = reader.lines();
    ) {

      List<Person> personList
          = stream
          .map(l -> {
            String[] pair = l.split(" ");
            Person p = new Person(pair[0], Integer.valueOf(pair[1]));
            return p;
          })
          .collect(Collectors.toList());

      System.out.println(personList);

      Optional<Integer> ageOpt
          = personList
          .stream()
          .map(p -> p.age)
          .min(Comparator.naturalOrder());

      if (ageOpt.isPresent()) {
        Integer age = ageOpt.get();
      } else {
        System.out.println("It was an empty list, no min element found");
      }

      Integer ageOrZero = ageOpt.orElse(0);

      Supplier<Integer> supplierRandom = () -> Math.random() > 0.5 ? 1:2;
      //#1
      ageOpt.orElseGet(supplierRandom);
      //#2
      ageOpt.orElseGet(() -> Math.random() > 0.5 ? 1:2);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  class Person {

    public String name;
    public int age;

    public Person() {

    }

    public Person(String name, int age) {
      this.name = name;
      this.age = age;
    }

    public Integer getAge() {
      return age;
    }

    public void setAge(Integer age) {
      this.age = age.intValue();
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String toString() {
      return "{" + name + ", " + age + "}";
    }
  }

}

