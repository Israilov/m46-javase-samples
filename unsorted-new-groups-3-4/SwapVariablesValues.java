package arithmetics;

import java.util.Scanner;

public class SwapVariablesValues {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int firstVariable = scan.nextInt();
        int secondVariable = scan.nextInt();

        System.out.println("You entered: "+firstVariable+", "+secondVariable);

        int temporaryVariable = firstVariable;
        firstVariable = secondVariable;
        secondVariable = temporaryVariable;

        System.out.println("Same variables after swap: "+firstVariable+", "+secondVariable);

    }

}
