package loops;

import java.util.Scanner;

public class SeriesOfStarsWithRegularSubstitutionOfAAndB_SolutionA {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = 0;
        int n = scanner.nextInt();

        while(i<n) {

            if (i%3==0) {
                System.out.print("A");
            } else if (i%4==0) {
                System.out.print("B");
            } else {
                System.out.print("*");
            }

            i++;
        }

    }

}
