package loops;

import java.util.Scanner;

public class HelloWorldNTimes {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = 0;
        int n = scanner.nextInt();

        while (i < n) {

            System.out.println("Hello World!");
            i++;

        }

    }

}
