package arithmetics;

import java.util.Scanner;

public class SumOfFiveInts {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        int d = scan.nextInt();
        int e = scan.nextInt();

        int sum = a+b+c+d+e;

        System.out.println("Sum of all integers is: " + sum);

    }

}
