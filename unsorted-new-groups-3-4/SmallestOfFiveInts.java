package conditionals;

import java.util.Scanner;

public class SmallestOfFiveInts {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        int d = scan.nextInt();
        int e = scan.nextInt();

        int min = a;

        if (b < min) {
            min = b;
        } else {
            if (c < min) {
                min = c;
            } else {
                if (d < min) {
                    min = d;
                } else {
                    if (e < min) {
                        min = e;
                    }
                }
            }
        }

        System.out.println("Smallest integer is: " + min);

    }

}
