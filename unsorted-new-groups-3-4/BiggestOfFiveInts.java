package conditionals;

import java.util.Scanner;

public class BiggestOfFiveInts {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        int d = scan.nextInt();
        int e = scan.nextInt();

        int max = a;

        if (b > max) {
            max = b;
        } else {
            if (c > max) {
                max = c;
            } else {
                if (d > max) {
                    max = d;
                } else {
                    if (e > max) {
                        max = e;
                    }
                }
            }
        }

        System.out.println("Biggest integer is: " + max);

    }

}
