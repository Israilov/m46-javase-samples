package loops;

import java.util.Scanner;

public class SortedIntegers {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        int d = scan.nextInt();
        int e = scan.nextInt();
        int tmp;

        int i = 0;

        while (i<5) {

            if (a > b) {
                tmp = a;
                a = b;
                b = tmp;
            }

            if (b > c) {
                tmp = b;
                b = c;
                c = tmp;
            }

            if (c > d) {
                tmp = c;
                c = d;
                d = tmp;
            }

            if (d > e) {
                tmp = d;
                d = e;
                e = tmp;
            }

            i++;
        }

        System.out.println(a+","+b+","+c+","+d+","+e);

    }

}
