import java.util.Scanner;

public class TelephoneBookApplication {

    public static void main(String[] args) {

        System.out.println("Hello this is my phonebook");

        Scanner scanner = new Scanner(System.in);

        TelBook telBook = new TelBook();

        while(true) {

            System.out.println("Menu: ");
            System.out.println("1 - Add page;");
            System.out.println("2 - Delete page;");
            System.out.println("3 - Show all pages;");

            System.out.println("Enter option: ");
            int optionSelected = scanner.nextInt();

            if (optionSelected == 1) {

                Birthday birthday = new Birthday();

                System.out.println("Birthday:");
                System.out.println("day:");
                birthday.setDay(scanner.nextInt());
                System.out.println("month:");
                birthday.setMonth(scanner.nextInt());
                System.out.println("year:");
                birthday.setYear(scanner.nextInt());

                Name name = new Name();

                System.out.println("Name:");
                System.out.println("firstname:");
                name.setFirstname(scanner.next());
                System.out.println("surname:");
                name.setSurname(scanner.next());
                System.out.println("paternalname:");
                name.setPaternal(scanner.next());
                System.out.println("maternalname:");
                name.setMaternal(scanner.next());
                System.out.println("prefix:");
                name.setPrefix(scanner.next());

                MobilePhone mobilePhone = new MobilePhone();

                System.out.println("Mobile phone:");
                System.out.println("country code:");
                mobilePhone.setCountryCode(scanner.next());
                System.out.println("msisdn:");
                mobilePhone.setMsisdn(scanner.next());

                Person person = new Person();

                person.setBirthday(birthday);

                Sex sex = new Sex();

                person.setSex(sex);

                Profession profession = new Profession();

                person.setProfession(profession);

                person.setName(name);

                Email email = new Email();

                person.setEmail(email);

                FacebookAccount facebookAccount = new FacebookAccount();

                person.setFacebookAccount(facebookAccount);
                person.setMobilePhone(mobilePhone);

                //TODO Homework: добавить все поля присущие объекту класса Person

                Page page = new Page();

                page.setPerson(person);

                telBook.add(page);

            } else if (optionSelected == 3) {

                int i = 0;

                Page[] pages = telBook.getPages();

                while (i < telBook.getLastEmptyPageIndex()) {

                    System.out.print(pages[i].getPerson().getName().getFirstname() + " ");
                    System.out.print(pages[i].getPerson().getName().getSurname() + " ");
                    System.out.print(pages[i].getPerson().getName().getPaternal() + " ");
                    System.out.print(pages[i].getPerson().getName().getMaternal() + " ");
                    System.out.print(pages[i].getPerson().getName().getPrefix() + " \n");

                    System.out.print(pages[i].getPerson().getBirthday().getDay() + ".");
                    System.out.print(pages[i].getPerson().getBirthday().getMonth() + ".");
                    System.out.print(pages[i].getPerson().getBirthday().getYear() + " ");
                    System.out.print(pages[i].getPerson().getSex().getType() + " ");
                    System.out.print(pages[i].getPerson().getMobilePhone().getCountryCode() + " ");
                    System.out.print(pages[i].getPerson().getMobilePhone().getMsisdn() + " \n");

                    System.out.print(pages[i].getPerson().getProfession().getProfessionName() + " ");
                    System.out.print(pages[i].getPerson().getProfession().getProfessionGroup() + " ");
                    System.out.print(pages[i].getPerson().getEmail().getUsername() + " ");
                    System.out.print(pages[i].getPerson().getEmail().getMailProvider() + " ");
                    System.out.print(pages[i].getPerson().getFacebookAccount().getUsername() + " \n");
                    
                    //TODO Homework: добавить все остальные поля, чтобы отображались в консоли

                    i++;
                }

            }
        }

    }

}
