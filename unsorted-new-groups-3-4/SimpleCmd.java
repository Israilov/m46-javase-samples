package MohmadkarimTasks.Lessons.Lesson5;

import java.io.File;
import java.util.Scanner;

public class SimpleCmd {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String basePath = "c:\\";

        while (true) {
            System.out.println(basePath);
            String operation = scanner.nextLine();

            File file = new File(basePath);

            if (operation.equals("dir")) {

                String[] files = file.list();

                for (int i = 0; i < files.length; i++) {
                    System.out.println(files[i]);
                }

            } else if (operation.startsWith("cd ")) {

                String[] str = operation.split(" "); //operation = cd c:\Windows
                basePath = str[1];

            } else if (operation.startsWith("del ")) {

                String[] str = operation.split(" ");
                File tmpFile = new File(str[1]);

                if(tmpFile.exists()) {

                    tmpFile.delete();
                    System.out.println("Operation was successful");

                }

                else{

                    System.out.println("This file does not exist");

                }

            } else {
                System.out.println("Unknown operation");
            }
        }
    }

}