package loops;

import java.util.Scanner;

public class SeriesOfStarsWithRegularSubstitutionOfAAndB_SolutionB {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = 0;
        int n = scanner.nextInt();
        int counter = 0;

        while (i < n) {

            if (counter == 3) {
                System.out.print("A");
            } else if (counter == 4) {
                System.out.print("B");
                counter = 0;
            } else {
                System.out.print("*");
            }

            i++;
            counter++;
        }

    }

}
