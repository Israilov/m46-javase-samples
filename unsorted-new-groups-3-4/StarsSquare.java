package loops;

import java.util.Scanner;

public class StarsSquare {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int col = 0;
        int sideSize = scanner.nextInt();
        int symbolsCouter = 1;

        while(symbolsCouter<sideSize*sideSize) {

            if (col<sideSize) {
                System.out.print("*");
                col++;
            } else {
                col = 0;
                System.out.println();
            }

            symbolsCouter++;
        }

    }

}
