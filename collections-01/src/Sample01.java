import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Sample01 {

  public static void main(String[] args) {

    ArrayList<Long> arrayList = new ArrayList<>();

    LinkedList<Long> linkedList = new LinkedList<>();

    for (long i=0; i<1_000_000L; i++) {
      arrayList.add(i);
    }

    for (long i=0; i<1_000_000L; i++) {
      linkedList.add(i);
    }

    Long val2 = linkedList.get(10_000); //get element in 10'000th position

    for (int i=0; i<linkedList.size(); i++) {
      System.out.println(linkedList.get(i));
    }

    Iterator<Long> llIterator = linkedList.iterator(); //starts from 0th position

    while(llIterator.hasNext()) {
      Long val = llIterator.next();
    }

    for (int i=0; i<arrayList.size(); i++) {
      System.out.println(arrayList.get(i));
    }

    Iterator<Long> alIterator = arrayList.iterator(); //starts from 0th position

    while(llIterator.hasNext()) {
      Long val = llIterator.next();
    }

  }
}
