import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Sample03Comparable {

  public static void main(String[] args) {

    List<TelContactComparable> telContacts = new ArrayList<>();

    telContacts.add(new TelContactComparable("77012222222"));
    telContacts.add(new TelContactComparable("77013333333"));
    telContacts.add(new TelContactComparable("77011111111"));

    Collections.sort(telContacts);

    Iterator<TelContactComparable> iterator = telContacts.iterator();

    while (iterator.hasNext()) {
      TelContactComparable tc = iterator.next();
      System.out.println(tc.tel);
    }

  }
}

class TelContactComparable implements Comparable<TelContactComparable> {

  public String tel;

  public TelContactComparable(String tel) {
    this.tel = tel;
  }

  public int compareTo(TelContactComparable b) {
    return b.tel.compareTo(this.tel)*-1;
  }
}

