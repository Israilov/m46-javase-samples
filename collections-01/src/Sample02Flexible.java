import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Sample02Flexible {

  public static void main(String[] args) {
    PositiveNumbersStorageFlexible pns = new PositiveNumbersStorageFlexible();

    pns.store(-1L);
    pns.sublist(0,1);

    ArrayList<Long> list = new ArrayList<>();
    list.add(1L);
    list.add(-1L);
    list.add(-100L);

    pns.store(list);

  }
}

class PositiveNumbersStorageFlexible {

  List<Long> positiveNumbers = new LinkedList<>();

  public void store(Long num) {
    if (num >= 0) {
      positiveNumbers.add(num);
    }
  }

  public List<Long> sublist(int fromIndex, int toIndex) {
    return positiveNumbers.subList(fromIndex, toIndex);
  }

  public void store(List<Long> list) {

    Iterator<Long> iterator = list.iterator();

    while (iterator.hasNext()) {
      this.store(iterator.next());
      iterator.next();
    }
  }

}
