import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Sample04Comparator {

  public static void main(String[] args) {

    List<TelContact> telContacts = new LinkedList<>();

    telContacts.add(new TelContact("77012222222", "Almaty"));
    telContacts.add(new TelContact("77013333333", "Kyzylorda"));
    telContacts.add(new TelContact("77011111111", "Astana"));

    telContacts.sort(new RegionComparator());

    print(telContacts);

    telContacts.sort(new TelComparator());

    print(telContacts);

  }

  private static void print(List<TelContact> telContacts) {

    System.out.println();

    Iterator<TelContact> iterator = telContacts.iterator();

    while (iterator.hasNext()) {
      TelContact tc = iterator.next();
      System.out.println(tc.tel + ", " + tc.region);
    }
  }
}

class TelContact {

  public String tel;
  public String region;

  public TelContact(String tel, String region) {
    this.tel = tel;
    this.region = region;
  }

}

class RegionComparator implements Comparator<TelContact> {

  @Override
  public int compare(TelContact a, TelContact b) {
    return a.region.compareTo(b.region);
  }

}

class TelComparator implements Comparator<TelContact> {

  @Override
  public int compare(TelContact a, TelContact b) {
    return a.tel.compareTo(b.tel);
  }

}