public class Sample05Stack {

  public static void main(String[] args) {

    Stack<Integer> stackA = new LinkedStack<>();
    stackA.push(1);
    stackA.push(2);
    stackA.push(3);
    stackA.pop();
    stackA.peek();

    ((ArrayStack)stackA).mambaJamba(); //casting

    Stack<Long> stackB = new LinkedStack<>();
    stackB.push(100000L);
    stackB.push(45L);

  }

}

interface Stack<T> {

  void clear();
  T peek();
  void push(T obj);
  T pop();
  int size();

}

class ArrayStack<T> implements Stack<T> {

  private Object[] stack = new Object[100];
  private int topIndex;

  public void clear() {
    stack = null;
  }

  public T peek() {
    return (T) stack[topIndex];
  }

  public void push(T obj) {
    if (topIndex == stack.length-1) {
      return;
    }
    topIndex++;
    stack[topIndex] = obj;
  }

  public T pop() {
    if (topIndex == -1) return null;
    topIndex--;
    return (T) stack[topIndex+1];
  }

  public int size() {
    return topIndex+1;
  }

  public void mambaJamba() {}

}

class LinkedStack<T> implements Stack<T> {

  private Node<T> topNode;
  int size = 0;

  public void clear() {
    topNode = null;
  }

  public T peek() {
    return (T) topNode.getObject();
  }

  public void push(T obj) {

    Node<T> newNode = new Node<T>();

    newNode.setObject(obj);
    newNode.setPrev(topNode);

    topNode = newNode;

    size++;

  }

  public T pop() {

    if (topNode == null) return null;

    Object obj = topNode.getObject();
    topNode = topNode.getPrev();

    size--;

    return (T) obj;
  }

  public int size() {
    return size;
  }

}

class Node<T> {

  private Node<T> prev;
  private Object object;

  public Node<T> getPrev() {
    return prev;
  }

  public void setPrev(Node<T> prev) {
    this.prev = prev;
  }

  public Object getObject() {
    return object;
  }

  public void setObject(Object object) {
    this.object = object;
  }
}