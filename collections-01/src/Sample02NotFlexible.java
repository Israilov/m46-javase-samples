import java.util.ArrayList;
import java.util.Iterator;

public class Sample02NotFlexible {

  public static void main(String[] args) {

    PositiveNumbersStorageNotFlexible pns = new PositiveNumbersStorageNotFlexible();

    pns.store(1L);
    pns.store(1000L);
    pns.store(-1L);
    pns.store(2000L);
    ArrayList<Long> subList = pns.sublist(0,1);

    ArrayList<Long> list = new ArrayList<>();
    list.add(1L);
    list.add(-1L);
    list.add(-100L);

    pns.store(list);

  }
}

class PositiveNumbersStorageNotFlexible {

  ArrayList<Long> positiveNumbers = new ArrayList<>();

  public void store(Long num) {
    if (num >= 0) {
      positiveNumbers.add(num);
    }
  }

  public ArrayList<Long> sublist(int fromIndex, int toIndex) {
    return (ArrayList<Long>) positiveNumbers.subList(fromIndex, toIndex);
  }

  public void store(ArrayList<Long> arrayList) {

    Iterator<Long> iterator = arrayList.iterator();

    while (iterator.hasNext()) {
      this.store(iterator.next());
      iterator.next();
    }
  }

}
