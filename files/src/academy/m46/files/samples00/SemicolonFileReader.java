package academy.m46.files.samples00;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SemicolonFileReader {

    public static void main(String[] args) {

        try {


                FileReader fin = new FileReader(new File("C:\\Users\\mranzorbey\\Workspaces\\m46\\2DObjectsPolymorphism\\out\\production\\ObjectsPolymorphism\\semicolonfile.txt"));

                PersonCard[] pcards = new PersonCard[2];
                pcards[0] = new PersonCard();
                pcards[1] = new PersonCard();

                int fieldCounter = 0;

                String tmp = "";

                int i = 0;

                while(true) {

                    int c = fin.read();

                    if (c<0) break;

                    else if (c==';') {

                        switch(fieldCounter) {
                            case 0: pcards[i].name = tmp; break;
                            case 1: pcards[i].surname = tmp; break;
                            case 2:
                                if (tmp.trim().length()>0) {
                                    pcards[i].contactList.add(new TelContact(tmp));
                                }
                                break;
                            case 3:
                                if (tmp.trim().length()>0) {
                                    pcards[i].contactList.add(new EmailContact(tmp));
                                }
                                break;
                            case 4:
                                if (tmp.trim().length()>0) {
                                    pcards[i].contactList.add(new TwitterContact(tmp));
                                }
                        }

                        if (fieldCounter<4) {
                            fieldCounter++;
                        } else {
                            fieldCounter=0;
                            i++;
                        }

                        tmp = "";

                    }

                    if (c!=';' && c!='\r' && c!='\n') {
                        tmp+=(char)c;
                    }

                }

                for (i=0; i<pcards.length; i++) {
                    System.out.println(pcards[i].toString());
                }

                fin.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
