package academy.m46.files.samples00;

import java.io.*;

public class InputStreamSample1 {

    public static void main(String[] args) {

        try {

            InputStream inputStream = new FileInputStream(new File("C:\\Users\\mranzorbey\\Workspaces\\m46\\2DObjectsPolymorphism\\src\\sample.txt"));

            int a = inputStream.read();

            System.out.println((char)a);

            while(true) {

                a = inputStream.read();

                if (a==-1) {
                    break;
                }

                System.out.print((char)a);

            }

            inputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
