package academy.m46.files.samples00;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SemicolonFileFormatWriter {

    public static void main(String[] args) {

        try {

            FileWriter fout = new FileWriter(new File("C:\\Users\\mranzorbey\\Workspaces\\m46\\2DObjectsPolymorphism\\out\\production\\ObjectsPolymorphism\\semicolonfile.txt"));

            PersonCard pcA = new PersonCard();
            pcA.name = "Vasya";
            pcA.surname = "Vasiliy";
            pcA.contactList.add(new TwitterContact("@vasya"));
            pcA.contactList.add(new EmailContact("vasya@gmail.com"));
            pcA.contactList.add(new TelContact("77012115060"));

            PersonCard pcB = new PersonCard();
            pcB.name = "Trump";
            pcB.surname = "Nash";
            pcB.contactList.add(new TwitterContact("@trump"));
            pcB.contactList.add(new TelContact("77022222222"));

            String pcATxt = pcA.toString();
            String pcBTxt = pcB.toString();

            System.out.println(pcATxt);
            System.out.println(pcBTxt);

            fout.write(pcATxt);
            fout.write("\r\n");
            fout.write(pcBTxt);

            fout.flush();
            fout.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

class PersonCard {
    public String name;
    public String surname;
    public List contactList = new ArrayList();

    public String toString() {
        String me = (name==null ? "":name)+";"+(surname==null ? "":surname)+";";

        String telContact = "";
        String emailContact = "";
        String twitterContact = "";

        for (Object c: contactList) {
            if (c instanceof TelContact) {
                telContact = c.toString();
            } else if (c instanceof EmailContact) {
                emailContact = c.toString();
            } else if (c instanceof TwitterContact) {
                twitterContact = c.toString();
            }
        }

        me+=telContact+";"+emailContact+";"+twitterContact+";";

        return me;
    }
}

abstract class Contact {}

class TwitterContact extends Contact {
    public TwitterContact(String twitter) {
        this.twitter = twitter;
    }
    public String twitter;

    public String toString() {
        return twitter;
    }
}

class TelContact extends Contact {
    public TelContact(String tel) {
        this.tel = tel;
    }
    public String tel;

    public String toString() {
        return tel;
    }
}

class EmailContact extends Contact {
    public EmailContact(String email) {
        this.email = email;
    }
    public String email;

    public String toString() {
        return email;
    }
}