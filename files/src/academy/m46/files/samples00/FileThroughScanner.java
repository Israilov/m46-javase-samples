package academy.m46.files.samples00;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileThroughScanner {

    public static void main(String[] args) throws FileNotFoundException {

//        Scanner scan = new Scanner(System.in);

        Scanner scan = new Scanner(new FileInputStream("C:\\Users\\mranzorbey\\Workspaces\\m46\\M46JavaSESamples\\src\\sample.txt"));

        int i = scan.nextInt();

        System.out.println(i);

    }

}
