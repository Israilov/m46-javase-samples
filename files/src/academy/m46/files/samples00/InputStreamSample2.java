package academy.m46.files.samples00;

import java.io.*;

public class InputStreamSample2 {

    public static void main(String[] args) {

        try {

            InputStream inputStream = new FileInputStream(new File("C:\\Users\\mranzorbey\\Workspaces\\m46\\2DObjectsPolymorphism\\src\\sample.txt"));

            String str = new String("");

            char c;

            while((c=(char)inputStream.read())!=-1) {
                str+=c;
            }

            System.out.println(str);

            inputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
