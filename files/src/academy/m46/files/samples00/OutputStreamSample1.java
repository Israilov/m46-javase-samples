package academy.m46.files.samples00;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamSample1 {

    public static void main(String[] args) {

        try {
            OutputStream outputStream = new FileOutputStream("C:\\Users\\mranzorbey\\Workspaces\\m46\\2DObjectsPolymorphism\\src\\sample.txt");

            outputStream.write('A');

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
