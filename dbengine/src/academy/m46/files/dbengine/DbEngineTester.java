package academy.m46.files.dbengine;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class DbEngineTester {

    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {

        SimpleDatabaseEngine simpleDatabaseEngine = new SimpleDatabaseEngine("C:\\Users\\mranzorbey\\Workspaces\\m46\\M46JavaSESamples\\data\\dbengine");

        fillTestData(simpleDatabaseEngine);

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            System.out.print("query>");

            String query = consoleReader.readLine();

            ArrayList queriedOwners1 = simpleDatabaseEngine.query2(query);

            printResult(queriedOwners1, query);

        }

    }

    private static void printResult(ArrayList queryResultList, String query) throws ClassNotFoundException {

        if (query.contains("select *")) {

            printHeader(query);

//            if (query.contains("from owners")) {
//                System.out.println("id\tname\tsurname");
//            } else if (query.contains("from tickets")) {
//                System.out.println("id\tsubject\tticketNumber\towner_id\tname\tsurname");
//            }

        } else {
            //...
        }

        for (Object obj: queryResultList) {
            if (query.contains("select *")) {
                System.out.println(obj.toString());
            } else {
                //...
            }
        }
    }

    private static void printHeader(String query) throws ClassNotFoundException {

        String fileName = query.split(" ")[3];
        String className = "academy.m46.files.dbengine."+fileName.substring(0,1).toUpperCase()+fileName.substring(1,fileName.length()-1);

        Class clazz = Class.forName(className);

        Field[] fields = clazz.getDeclaredFields();

        for (int i=0; i<fields.length; i++) {
            System.out.print(fields[i].getName()+"\t");
        }

        System.out.println();

    }

    private static void fillTestData(SimpleDatabaseEngine simpleDatabaseEngine) throws IOException {
        ArrayList ownersToWrite = new ArrayList();

        Owner owner1 = new Owner();
        owner1.id=1;
        owner1.name="Iskhak";
        owner1.surname="Malevich";

        Owner owner2 = new Owner();
        owner2.id=2;
        owner2.name="Isaak";
        owner2.surname="Newton";

        ownersToWrite.add(owner1);
        ownersToWrite.add(owner2);

        simpleDatabaseEngine.saveOwners(ownersToWrite);

        ArrayList readOwners = simpleDatabaseEngine.readOwners();

//        for (Object owner: readOwners) {
//
//            System.out.println(owner);
//
//        }

    }


}
