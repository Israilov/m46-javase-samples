package academy.m46.files.dbengine;

/* stores objects records in owners.db file */
public class Owner {

    public Integer id;
    public String name;
    public String surname;

    public String toString() {
        return "id: "+id+", name:"+name+", surname:"+surname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
