package academy.m46.files.dbengine;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class SimpleDatabaseEngine {

    private String folderPath;

    public SimpleDatabaseEngine(String folderPath) {
        this.folderPath = folderPath;
    }

    public void saveOwners(ArrayList owners) throws IOException {

        FileWriter fileWriter = new FileWriter(folderPath+"\\owners.db");

        for(Object owner: owners) {
            fileWriter.write(getOwnerRecord((Owner)owner));
            fileWriter.write("\r\n");
        }

        fileWriter.flush();
        fileWriter.close();

    }

    public ArrayList readOwners() throws IOException {

        ArrayList ownersList = new ArrayList();

        BufferedReader bufFileReader = new BufferedReader(new FileReader(folderPath+"\\owners.db"));

        String objectRecord = null;

        while((objectRecord = bufFileReader.readLine())!=null) {

            if (objectRecord.trim().length()>0) {

                String[] objectRecordArr = objectRecord.split(",");

                Owner owner = new Owner();

                for (int i = 0; i < objectRecordArr.length; i++) {

                    String[] keyValue = objectRecordArr[i].split(":");

                    String fieldName = keyValue[0];
                    String fieldValue = keyValue[1];

                    if ("name".equals(fieldName)) {
                        owner.name = fieldValue;
                    } else if ("surname".equals(fieldName)) {
                        owner.surname = fieldValue;
                    } else if ("id".equals(fieldName)) {
                        owner.id = new Integer(fieldValue);
                    }

                }

                ownersList.add(owner);

            }

        }

        return ownersList;
    }

    public ArrayList readAnyRecords(String fileName) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        ArrayList ownersList = new ArrayList();

        BufferedReader bufFileReader = new BufferedReader(new FileReader(folderPath+"\\"+fileName+".db"));

        String objectRecord = null;

        while((objectRecord = bufFileReader.readLine())!=null) {

            if (objectRecord.trim().length()>0) {

                String[] objectRecordArr = objectRecord.split(",");

                String className = "academy.m46.files.dbengine."+fileName.substring(0,1).toUpperCase()+fileName.substring(1,fileName.length()-1);

                Class clazz = Class.forName(className);

                Object object = clazz.newInstance();

                for (int i = 0; i < objectRecordArr.length; i++) {

                    String[] keyValue = objectRecordArr[i].split(":");

                    String fieldName = keyValue[0];
                    String fieldValue = keyValue[1];

                    if ("id".equals(fieldName)) {
                        Integer id = Integer.parseInt(fieldValue);
                        Method method = clazz.getMethod("set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1), Integer.class);
                        method.invoke(object, id);
                    } else {
                        Method method = clazz.getMethod("set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1), String.class);
                        method.invoke(object, fieldValue);
                    }

                }

                ownersList.add(object);

            }

        }

        return ownersList;
    }


    public String getOwnerRecord(Owner owner) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("id:");
        stringBuilder.append(owner.id);
        stringBuilder.append(",name:");
        stringBuilder.append(owner.name);
        stringBuilder.append(",surname:");
        stringBuilder.append(owner.surname);

        return stringBuilder.toString();

    }

    public ArrayList query2(String query) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        ArrayList result = null;
        String[] splittedArray = query.split(" ");

        if (splittedArray[0].equals("select")) {

            String fileName = query.split(" ")[3];

            result = readAnyRecords(fileName);

            if (splittedArray[4].equals("where")) {

                ArrayList tmpResult = new ArrayList();

                String[] keyVal = splittedArray[5].split("=");
                String fieldName = keyVal[0];
                String value = keyVal[1];

                if (fieldName.equals("name")) {

                    for (Object obj: result) {
                        if (((Owner)obj).name.equals(value)) tmpResult.add(obj);
                    }

                    result = tmpResult;

                } else if (fieldName.equals("id")) {

                } else if (fieldName.equals("surname")) {

                }

            }

        } else if (splittedArray[0].equals("update")) {

        } else if (splittedArray[1].equals("delete")) {

        }

        return result;

    }

    public ArrayList query(String query) throws IOException {

        ArrayList result = null;
        String[] splittedArray = query.split(" ");

        if (splittedArray[0].equals("select")) {

            if (splittedArray[3].equals("owners")) {

                result = readOwners();

                if (splittedArray[4].equals("where")) {

                    ArrayList tmpResult = new ArrayList();

                    String[] keyVal = splittedArray[5].split("=");
                    String fieldName = keyVal[0];
                    String value = keyVal[1];

                    if (fieldName.equals("name")) {

                        for (Object obj: result) {
                            if (((Owner)obj).name.equals(value)) tmpResult.add(obj);
                        }

                        result = tmpResult;

                    } else if (fieldName.equals("id")) {

                    } else if (fieldName.equals("surname")) {

                    }

                }

            } else if (splittedArray[3].equals("tickets")) {

            }

        } else if (splittedArray[0].equals("update")) {

        } else if (splittedArray[1].equals("delete")) {

        }

        return result;

    }
}
