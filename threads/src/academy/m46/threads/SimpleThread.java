package academy.m46.threads;

public class SimpleThread {
    public static void main(String[] args) {
        int count = 0;
        int numThreads=0;

        while(numThreads<10) {
            Thread thread = new MyThread();
            thread.start();
        }

        while(count<1000000) {
            System.out.println(Thread.currentThread().getId() + ":" + (count++));
        }
    }
}

class MyThread extends Thread {
    public void run() {
        int count = 0;

        while(count<1000000) {
            System.out.println(Thread.currentThread().getId()+":"+(count++));
        }
    }
}