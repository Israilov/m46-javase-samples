package academy.m46.threads.l5;

//Заготовка для программы поиска по лабиринту используя треды, т.е. это модификация стандартного алгоритма поиска по
//лабиринту, но используя треды
public class Labirint {

    public static void main(String[] args) {

        int[][] x = new int[][]
                {
                        {1, 1, 0, 1, 1},
                        {1, 1, 0, 1, 1},
                        {0, 0, 0, 0, 1},
                        {1, 1, 0, 1, 1},
                        {1, 1, 1, 1, 1}
                };

        int col = 2;
        int row = 2;

        LabirintSeeker seeker1 = new LabirintSeeker(x, 's', row, col);//s-south
        //...

        seeker1.start();
//        seeker2.start();
//        seeker3.start();
//        seeker4.start();

        while(seeker1.isAlive()) {}
        //...

        System.out.println("Exit found: "+seeker1.isExitFound);
        //..

    }

}

class LabirintSeeker extends Thread {

    private int[][] lab;
    private char direction;
    private int row;
    private int col;
    public boolean isExitFound;

    public LabirintSeeker(int[][] lab, char direction, int row, int col) {
        this.lab = lab;
        this.direction = direction;
        this.row = row;
        this.col = col;
    }

    public void run() {

        if (direction == 's') {

            int i = row;

            while (i < lab.length) {
                if (lab[i][col] == 1) {
                    isExitFound = false;
                    return;
                }
                i++;
            }

            isExitFound = true;
        }

    }

}
