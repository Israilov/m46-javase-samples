package academy.m46.threads.l3;

public class TwoNumbersSum {

    public static void main(String[] args) {

        SumThread t1 = new SumThread(23, 4);
        SumThread t2 = new SumThread(45, 100);

        t1.start();
        t2.start();

        while(true) {
            if (t1.isAlive()==false && t2.isAlive()==false) break;
        }

        SumThread t3 = new SumThread(t1.getSum(), t2.getSum());
        t3.start();

        while(true) {
            if (t3.isAlive()==false) break;
        }

        System.out.println("sum="+t3.getSum());

    }

}

class SumThread extends Thread {

    private int a;
    private int b;
    private int sum;

    public SumThread(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void run() {

        try {
            Thread.sleep(5000);
        } catch(Exception e) {}

        sum = a+b;

    }

    public int getSum() {
        return sum;
    }

}
