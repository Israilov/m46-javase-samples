package academy.m46.threads.l2;

//Задача: изучение функций join() и interrupt() на примере
//5ть тредов каждый сам по-себе считает до 1000 тот кто досчитает быстрее считается победителем
//после каждого цикла инкремента собственной переменной i все треды выжидают 5ms
//тред который первый досчитает до 1000 останавливает всех остальных конкурентов используя стандартную функцию interrupt()
//main() тред ждет 7 секунд и после этого использует метод join() чтобы дождаться выполнения всех запущенных тредов (чтобы
// не завершиться раньше чем они досчитают до 1000)
//main() тред также запускает треды в два этапа: 1) создание объектов тредов 2) запуск всех тредов в одном цикле
public class AppInterrupts {

    public static void main(String[] args) throws InterruptedException {

        Thread[] concurrents = new RunnerThread[5];

        for (int i=0; i<5; i++) {
            concurrents[i] = new RunnerThread(concurrents);
        }

        for (int i=0; i<5; i++) {
            concurrents[i].start();
        }

        Thread.sleep(7000); //дать всем тредам время поувеличивать свои переменные i

        for (int i=0; i<5; i++) {
            concurrents[i].join();
        }
    }

}

class RunnerThread extends Thread {

    private Thread[] concurrents;

    public RunnerThread(Thread[] concurrents) {
        this.concurrents = concurrents;
    }

    public void run() {

        int i=0;
        while(i<=1000) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getId()+": Ээээх разбудили...");
            }

            if (isInterrupted()) {
                System.out.println(Thread.currentThread().getId()+": Елки палки проиграл ((");
                return;
            }

            if (i == 1000) {
                System.out.println(Thread.currentThread().getId()+": Эгегеге выиграл!!! )))");
                for (int j=0; j<concurrents.length; j++) {
                    concurrents[j].interrupt();
                }
            }

            i++;
        }

    }

}
