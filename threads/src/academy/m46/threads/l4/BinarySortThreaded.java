package academy.m46.threads.l4;

import java.util.Arrays;

public class BinarySortThreaded {

    public static void main(String[] args) {

        int[] arr = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

        new MultiThreadSort(arr).start();

        Arrays.parallelSort(arr);

    }

}

class MultiThreadSort extends Thread {

    private int[] arr;

    public MultiThreadSort(int[] arr) {
        this.arr = arr;
    }

    public void run() {

        if (this.arr.length > 3) {

            MultiThreadSort firstHalfOfArr = new MultiThreadSort(halfArray(arr, 0));
            MultiThreadSort secondHalfOfArr = new MultiThreadSort(halfArray(arr, arr.length / 2));

            firstHalfOfArr.start();
            secondHalfOfArr.start();

            while (firstHalfOfArr.isAlive() || secondHalfOfArr.isAlive()) ;

            int[] smallerArr = firstHalfOfArr.getArr().length > secondHalfOfArr.getArr().length ? secondHalfOfArr.getArr() : firstHalfOfArr.getArr();
            int[] biggerArr = firstHalfOfArr.getArr().length < secondHalfOfArr.getArr().length ? secondHalfOfArr.getArr() : firstHalfOfArr.getArr();

            int[] jointArr = new int[arr.length];

            for (int i = 0; i < biggerArr.length; i++) {
                jointArr[i] = biggerArr[i];
            }

            for (int i=0; i<smallerArr.length; i++) {

                for (int j=0; j<biggerArr.length; j++) {
                    //...
                }

            }

            arr = jointArr;

            //wait until both complete
        } else {
            Arrays.sort(arr);
        }

    }

    public int[] getArr() {
        return arr;
    }

    public int[] halfArray(int[] arr, int start) {
        int[] tmp = new int[arr.length / 2];
        for (int i = start; i < tmp.length + start; tmp[i - start] = arr[i], i++) ;
        return tmp;
    }

}


