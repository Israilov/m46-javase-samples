package academy.m46.threads;

public class SynchronizedThreadsDemo {

    public static MyIntSynchronized a = new MyIntSynchronized();

    public static void main(String[] args) {

        for (int i=0; i<1000; i++) {
            new XThread().start();
        }

        try {
            Thread.sleep(15000);
        } catch(Exception e) {
            e.printStackTrace();
        }

        System.out.println(a.k);

    }

}

class MyInt {
    public int k = 0;

    public void increment() {
        int tA = SynchronizedThreadsDemo.a.k;
        int tS = tA+1;
        SynchronizedThreadsDemo.a.k = tS;
    }
}

class MyIntSynchronized {
    public int k;

    public synchronized void increment() {
        int tA = SynchronizedThreadsDemo.a.k;
        int tS = tA+1;
        SynchronizedThreadsDemo.a.k = tS;
    }
}

class XThread extends Thread {

    public void run() {

        try {
            Thread.sleep(200);
        } catch(Exception e) {e.printStackTrace();}

        SynchronizedThreadsDemo.a.increment();
    }

}
