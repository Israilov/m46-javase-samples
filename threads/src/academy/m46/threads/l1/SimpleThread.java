package academy.m46.threads.l1;

public class SimpleThread {

    public static boolean isStop = false;

    public static void main(String[] args) {
        int count = 0;
        int numThreads=0;

        while(numThreads < 10) {
            Thread thread = new MyThread(15000);
            thread.start();
            numThreads++;
        }

        while(count<1000000) {
            if (isStop == true) {
                return;
            }
            if (count == 15000) {
                isStop = true;
                return;
            }
            System.out.println(Thread.currentThread().getId() + ":" + (count++));
        }
    }
}

class MyThread extends Thread {

    private int max = 0;

    public MyThread(int max) {
        this.max = max;
    }

    public void run() {

        int count = 0;

        while(count<1000000) {

            if (count == max) {
                SimpleThread.isStop = true;
                return;
            }
            if (SimpleThread.isStop == true) {
                return;
            }

            System.out.println(Thread.currentThread().getId()+":"+(count++));
        }
    }

}
