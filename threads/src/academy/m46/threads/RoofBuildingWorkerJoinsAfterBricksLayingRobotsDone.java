package academy.m46.threads;

import java.util.Stack;

public class RoofBuildingWorkerJoinsAfterBricksLayingRobotsDone {
    //Two types of threads: roof building and bricks laying, roof building robot builds roof only after bricks were layed done
    //join should be used
    //manager waits for 5 seconds until work is done if not then should a bit "hey don't sleep!"

    static Stack<String> stackBricks = new Stack<>();

    public static void main(String[] args) {

        stackBricks.push("brick");
        stackBricks.push("brick");
        stackBricks.push("brick");
        stackBricks.push("brick");
        stackBricks.push("brick");

        stackBricks.pop();

    }

}
