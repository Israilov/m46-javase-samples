package academy.m46.threads;

public class ThreadDemoGuma {

    public static void main(String[] args) {

        int i = 0;

        MyDemoThread t = new MyDemoThread();
        MyDemoThread t2 = new MyDemoThread();
        MyDemoThread t3 = new MyDemoThread();

        t.start();
        t2.start();
        t3.start();

        while(i<10) {
            System.out.println("i: "+i);
            i++;
        }

        while(t.isAlive()) {}

    }

}

class MyDemoThread extends Thread {

    public void run() {

        int i = 100;

        while(i>0) {
            System.out.println(i);
            i--;
        }

    }

}
