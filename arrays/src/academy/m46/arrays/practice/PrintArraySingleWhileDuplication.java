package academy.m46.arrays.practice;

public class PrintArraySingleWhileDuplication {

    public static void main(String[] args) {

        int i=0;
        int max=5;

        while(i<18) {
            System.out.print(i);

            if (i%max==0 && i!=0) {
                System.out.println();
                max=max+6;
            } else if (i<17) {
                System.out.println(",");
            } else {
                System.out.println(".");
            }

            i++;
        }

    }

}
