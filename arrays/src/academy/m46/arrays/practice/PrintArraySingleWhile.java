package academy.m46.arrays.practice;

public class PrintArraySingleWhile {

    public static void main(String[] args) {

        int i=0;

        while(i<101) {

            if (i%2==0) {
                System.out.print(i);

                if (i<100) {
                    System.out.println(",");
                } else {
                    System.out.println(".");
                }

            }

            i++;
        }

    }

}
