package academy.m46.arrays.practice;

public class PrintArraySingleWhile2 {

    public static void main(String[] args) {

        int i=0;

        while(i<101) {

            System.out.print(i);

            if (i<100) {
                System.out.print(",");
            } else {
                System.out.println(".");
            }

            i+=2;
        }

    }

}
