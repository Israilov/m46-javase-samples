package academy.m46.arrays.games.harvester;

import java.util.*;

public class HarvesterGame {

    public static void main(String[] args) {

        String[] tmpArr = new String[]{ "+++++++++++++++++++++++++++++++++++++++++++++++",
                                        "+   x                    x                    +",
                                        "+   x                    xxxxx                +",
                                        "+   xxxxxxxxx       xxxxxx                    +",
                                        "+   x       x       x                         +",
                                        "+   x       x       x                         +",
                                        "+   x       x       x                         +",
                                        "+           x       x       n         00000000+",
                                        "+     n                               00000000+",
                                        "+           n                         00000000+",
                                        "+                                     00000000+",
                                        "+++++++++++++++++++++++++++++++++++++++++++++++" };

        char[][] level1 = new char[tmpArr.length][tmpArr[0].length()];

        int i=0, j=0;

        while(i<tmpArr.length) {
            while(j<tmpArr[0].length()) {
                level1[i][j] = tmpArr[i].charAt(j);
                j++;
            }
            j=0;
            i++;
        }

        int robotPosX = 34, robotPosY = 3;
        char oriented = 'S';

        Scanner scanner = new Scanner(System.in);

        while(true) {

            //print entire level
            int r=0, c=0;
            while(r<level1.length) {
                while(c<level1[0].length) {
                    System.out.print(level1[r][c]);
                    c++;
                }
                System.out.println();
                c=0;
                r++;
            }

            //move robot (move forward (w), back (s), left (a), right (d), rotate (r))
            String yourMove = scanner.next();

            //check if move is possible
            //if ...

                //if TRUE we do move robot
                    //if BOX on the way then move BOX
                //if FALSE we do nothing

            //if some BOX is not possible to move - GAME FAILED!
            while() {
                while() {
                    //search for all n's and check if GAME FAILED
                }
            }

        }



    }

}


/*

+++++++++++++++++++++++++++++++++++++++++++++++
+   x                    x                    +
+   x                    xxxxx     #          +
+   xxxxxxxxx       xxxxxx         E          +
+   x       x       x                         +
+   x       x       x       nnn               +
+   x       x       x       nnn               +
+           x       x                 00000000+
+    nn                               00000000+
+    nn     n            nn           00000000+
+                                     00000000+
+++++++++++++++++++++++++++++++++++++++++++++++


*/
