package academy.m46.arrays.labirint;

import java.util.Scanner;

public class LabirintRecursive {

    public static void main(String[] args) {

        char[][] labirint = new char[][]{
                {'*', '*', '0', '*', '0', '*', '*', '*', '*', '*'},
                {'*', '*', '0', '*', '0', '0', '0', '0', '*', '*'},
                {'*', '*', '0', '*', '*', '*', '*', '0', '*', '*'},
                {'*', '0', '0', '0', '0', '0', '*', '0', '*', '*'},
                {'*', '*', '0', '*', '*', '0', '*', '0', '*', '*'},
                {'*', '*', '0', '*', '0', '0', '0', '0', '*', '*'},
                {'*', '0', '0', '*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '0', '*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*', '*', '*', '*'}
        };

        int entryBotR = 0, entryBotC = 2;

        goBot(entryBotR, entryBotC, entryBotR, entryBotC, 0, labirint);

    }

    private static boolean goBot(int currentBotR, int currentBotC, int previousBotR, int previousBotC, int steps, char[][] labirint) {

        Scanner scanner = new Scanner(System.in);

        int i = 0, j = 0;

        while (i < labirint.length) {
            while (j < labirint[0].length) {
                if (i == currentBotR && j == currentBotC) {
                    System.out.print('A');
                } else {
                    System.out.print(labirint[i][j]);
                }
                j++;
            }
            j=0;
            i++;
            System.out.println();
        }

        System.out.println();

        scanner.next();

        if ((currentBotR==labirint.length-1 || currentBotC==labirint[0].length-1 || currentBotR==0 || currentBotC==0) && steps>0) {
            System.out.println("Here is the way out!");
            return true;
        }

        //todo: show the difference between !(x==c && y==c) and (x!=c && y!=c)
        //todo: show the difference between ++steps and steps++
        if (labirint[currentBotR+1][currentBotC]=='0' && (!(previousBotR==currentBotR+1 && previousBotC==currentBotC) || steps==0)) {
            boolean result = goBot(currentBotR+1, currentBotC, currentBotR, currentBotC, ++steps, labirint);
            if (result) return result;
        }
        if (currentBotR!=0 && labirint[currentBotR-1][currentBotC]=='0' && (!(previousBotR==currentBotR-1 && previousBotC==currentBotC) || steps==0)) {
            boolean result = goBot(currentBotR - 1, currentBotC, currentBotR, currentBotC, ++steps, labirint);
            if (result) return result;
        }
        if (labirint[currentBotR][currentBotC+1]=='0' && (!(previousBotR==currentBotR && previousBotC==currentBotC+1) || steps==0)) {
            boolean result = goBot(currentBotR, currentBotC+1, currentBotR, currentBotC, ++steps, labirint);
            if (result) return result;
        }
        if (currentBotC!=0 && labirint[currentBotR][currentBotC-1]=='0' && (!(previousBotR==currentBotR && previousBotC==currentBotC-1) || steps==0)) {
            boolean result = goBot(currentBotR, currentBotC-1, currentBotR, currentBotC, ++steps, labirint);
            if (result) return result;
        }

        return false;

    }

}
