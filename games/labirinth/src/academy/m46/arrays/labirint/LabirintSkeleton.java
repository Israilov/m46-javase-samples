package academy.m46.arrays.labirint;

import java.util.Scanner;

public class LabirintSkeleton {

    public static void main(String[] args) {
        char[][] labirint = new char[][]{
                {'*', '*', '0', '*', '0', '*', '*', '*', '*', '*'},
                {'*', '*', '0', '*', '0', '0', '0', '0', '*', '*'},
                {'*', '*', '0', '*', '*', '*', '*', '0', '*', '*'},
                {'*', '0', '0', '0', '0', '0', '*', '0', '*', '*'},
                {'*', '*', '0', '*', '*', '0', '*', '0', '*', '*'},
                {'*', '*', '0', '*', '0', '0', '0', '0', '*', '*'},
                {'*', '0', '0', '*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '0', '*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*', '*', '*', '*'}
        };

        int robotR = 0, robotC = 2, stepsPassed = 0;
        char oriented = 'S';
        Scanner scanner = new Scanner(System.in);

        while (true) {

            int i = 0, j = 0;
            int rightR = robotR, rightC = robotC-1;
            int frontR = robotR+1, frontC = robotC;

            while (i < labirint.length) {
                while (j < labirint[0].length) {
                    if (i == robotR && j == robotC) {
                        System.out.print('A');
                    } else if (i == rightR && j == rightC) {
                        System.out.print('R');
                    } else if (i == frontR && j == frontC) {
                        System.out.print('F');
                    } else {
                        System.out.print(labirint[i][j]);
                    }
                    j++;
                }
                j=0;
                i++;
                System.out.println();
            }

            scanner.next();

            //Here goes logic of your bot to find way out of the labirint. Please check out "rule of the right hand" as a reference algorithm.

            System.out.println();

        }

    }

}
