package academy.m46.arrays.games.mariner.singlethreaded;

public enum OrientedEnum {
    SOUTH,
    NORTH,
    WEST,
    EAST
}
