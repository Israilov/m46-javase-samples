package academy.m46.arrays.games.mariner.singlethreaded;

import java.util.List;

public abstract class AbstractGameField {

    public abstract void drawGameField();
    public abstract List<AbstractWall> getVisibleWalls(AbstractBot bot);
    public abstract List<AbstractBot> getVisibleBots(AbstractBot bot);

}
