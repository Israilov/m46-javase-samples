import java.util.Scanner;

public class MsisdnGeneratorNoDuplicates {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    System.out.println("Enter prefix: ");

    int prefix = scanner.nextInt();

    System.out.println("Enter number of digits after prefix: ");

    int digitsNumber = scanner.nextInt();

    int degree = 1;

    int digitsCounter = 0;

    while (digitsCounter < digitsNumber) {
      degree *= 10;
      digitsCounter++;
    }

    long[] allMsisdnNumbers = new long[9999999];

    int end = degree - 1;

    int counter = 0;

    while (counter < end) {
      long generatedMsisdnNumber = (long) (prefix * degree + counter);

      allMsisdnNumbers[counter] = generatedMsisdnNumber;

      counter++;
    }

    int i = 0;

    while (i < end) {
      System.out.println(allMsisdnNumbers[i]);
      i++;
    }

  }
}