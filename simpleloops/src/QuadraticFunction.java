import java.util.Scanner;

public class QuadraticFunction {

  public static void main(String[] args) {

    int x=0;
    int y=0;
    int k=0;
    int b=0;

    Scanner scan = new Scanner(System.in);

    System.out.println("Type value of k: ");
    k = scan.nextInt();
    System.out.println("Type value of b: ");
    b = scan.nextInt();
    System.out.println("Type max value of X: ");
    int maxX = scan.nextInt();

    while (x<=maxX) {

      y = k*x*x + b;

      System.out.println("x="+x+", y="+y);

      x++;

    }

  }
}
