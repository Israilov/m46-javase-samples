import java.util.Scanner;

public class LinearFunction {

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);

    System.out.println("Type in value of k: ");
    int k = scan.nextInt();
    System.out.println("Type in value of b: ");
    int b = scan.nextInt();
    System.out.println("Type in value of maxX: ");
    int maxX = scan.nextInt();

    int x = 0;

    while(x<maxX) {
      int y = k*x+b;
      System.out.println("(x="+x+", y="+y+"),");
      x++;
    }


  }

}
