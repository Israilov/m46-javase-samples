import java.util.Scanner;

public class LeftReverseTriangle {

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);
    System.out.println("Type in side size: ");
    int n = scan.nextInt();

    int row = 0;
    int column = 0;

    while(row<n && column<n) {

      if (column<(n-row)) {
        System.out.print('*');
      }

      if (column==n-1) {
        System.out.println();
        row++;
        column = 0;
      } else {
        column++;
      }

    }


  }

}
