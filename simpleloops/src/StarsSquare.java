import java.util.Scanner;

public class StarsSquare {

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);

    System.out.println("Type in size of square side: ");

    int n = scan.nextInt();

    int row = 0;
    int column = 0;

    while(row<n && column<n) {

      if (column==0 || column==n-1 || row==0 || row==n-1) {
        System.out.print('*');
      } else {
        System.out.print('-');
      }

      if (column==n-1) {
        System.out.println();
        row++;
        column = 0;
      } else {
        column++;
      }

    }

  }

}
