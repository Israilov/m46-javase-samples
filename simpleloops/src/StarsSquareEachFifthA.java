package loops;

import java.util.Scanner;

/*
 * Hint: This is not the optimal solution, but it is good to check
 * student abilities to traverse the code since it
 * contains enough if-else statements and increments
 * */
public class StarsSquareEachFifthA {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int col = 0;
        int sideSize = scanner.nextInt();
        int symbolsCounter = 0;

        while (symbolsCounter < sideSize * sideSize) {

            if (col < sideSize) {
                if ((symbolsCounter+1) % 5 == 0) {
                    System.out.print("A");
                } else {
                    System.out.print("*");
                }
                col++;
                symbolsCounter++;
            } else if (col == sideSize) {
                System.out.println();
                col = 0;
            } else {
                symbolsCounter++;
            }
        }

    }

}
