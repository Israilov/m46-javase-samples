import java.util.Scanner;

public class KratnoDvum {

  public static void main(String[] args) {
    int i = 0;

    Scanner scan = new Scanner(System.in);
    int n = scan.nextInt();

    while(i<n) {

      if (i%2==0) {
        System.out.print("*");
      } else {
        System.out.print("-");
      }

      i++;
    }
  }
}
