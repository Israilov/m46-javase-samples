public class MsisdnGenerator {

  public static void main(String[] args) {

    long[] allMsisdnNumbers = new long[9999999];

    int end = 9999999;

    int counter = 0;

    while (counter < end) {
      allMsisdnNumbers[counter] = (long) ((701 * Math.pow(10, 7)) + counter);
      counter++;
    }

    int i = 0;

    while (i < end) {
      System.out.println(allMsisdnNumbers[i]);
      i++;
    }

  }
}