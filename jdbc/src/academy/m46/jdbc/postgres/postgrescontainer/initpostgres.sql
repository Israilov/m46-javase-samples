CREATE SCHEMA m46_samples;

CREATE SEQUENCE m46_samples.users_id_seq INCREMENT BY 1;

CREATE TABLE m46_samples.users
(
  id INTEGER DEFAULT nextval('m46_samples.users_id_seq'::regclass) NOT NULL,
  username VARCHAR(16) NOT NULL,
  name VARCHAR(16) NOT NULL
);

/* Add Primary Key */
ALTER TABLE m46_samples.users ADD CONSTRAINT pkusers
PRIMARY KEY (id);

