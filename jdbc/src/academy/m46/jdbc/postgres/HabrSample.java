package academy.m46.jdbc.postgres;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
* Taken from: https://habr.com/sandbox/41444/
* */
public class HabrSample {

    public static void main(String args[]) {

        Connection connection = null;
        //URL к базе состоит из протокола:подпротокола://[хоста]:[порта_СУБД]/[БД] и других_сведений
        String url = "jdbc:postgresql://127.0.0.1:5432/m46postgres_sample_db";
        //Имя пользователя БД
        String name = "m46postgres_sample_user";
        //Пароль
        String password = "q";
        try {
            //Loading PostgreSQL DB driver
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver loaded");
            //Creation connection to database
            connection = DriverManager.getConnection(url, name, password);
            System.out.println("Connection established");
            //3 types of objects exist to execute SQL:
            //1.Statement class is used for the most simple SQL statements with no input or output paramters
            Statement statement = null;

            statement = connection.createStatement();
            //Executing SQL statement
            ResultSet result1 = statement.executeQuery(
                    "SELECT * FROM m46_samples.users where id>2 and id<10");
            //'result1' is a reference to the first row of resulting table
            //to access data of a row we will use various getX methods like getInt, getString after calling
            //of a method next() - this method will also move result1 to the next row of a resulting table
            System.out.println("Outputting statement result");
            while (result1.next()) {
                System.out.println("Row number in a resulting set #" + result1.getRow()
                        + "\t Unique id in a table #" + result1.getInt("id")
                        + "\t" + result1.getString("username"));
            }
            // Inserting into a PostgreSQL table
            statement.executeUpdate(
                    "INSERT INTO m46_samples.users(name, username) values ('nameX','usernameX')");
            // Update entry in a PostgreSQL table
            statement.executeUpdate(
                    "UPDATE m46_samples.users SET username = 'admin' where id = 1");

            //2.PreparedStatement: предварительно компилирует запросы,
            //которые могут содержать входные параметры
            PreparedStatement preparedStatement = null;
            // ? - место вставки нашего значеня
            preparedStatement = connection.prepareStatement(
                    "SELECT * FROM m46_samples.users where id > ? and id < ?");
            //Устанавливаем в нужную позицию значения определённого типа
            preparedStatement.setInt(1, 2);
            preparedStatement.setInt(2, 10);
            //executing statement
            ResultSet result2 = preparedStatement.executeQuery();

            System.out.println("Outputting PreparedStatement");
            while (result2.next()) {
                System.out.println("Row number in a resulting set #" + result2.getRow()
                        + "\t Unique id in table #" + result2.getInt("id")
                        + "\t" + result2.getString("username"));
            }

            preparedStatement = connection.prepareStatement(
                    "INSERT INTO m46_samples.users(name, username) values(?,?)");
            preparedStatement.setString(1, "name");
            preparedStatement.setString(2, "user_name");
            //then executeUpdate() method is called (that takes no input params)
            //the same executeUpdate() method should be called when doing UPDATE sql statement
            preparedStatement.executeUpdate();

            //3.CallableStatement is used to call prepared functions, that might contain input or output parameters
            CallableStatement callableStatement = null;
            //calling myfunc method
            callableStatement = connection.prepareCall(
                    " { call myfunc(?,?) } ");
            //Setting input parameters
            callableStatement.setString(1, "Kolka");
            callableStatement.setString(2, "Alex");
            //Executing SQL statement
        //    ResultSet result3 = callableStatement.executeQuery(); //executeUpdate() is called instead of executeQuery() when called function modifies data
            //If CallableStatement returns several ResultSet objects, then result set should be called in a loop accessing row by row with method next()
        //    result3.next();
        //    System.out.println(result3.getString("MESSAGE"));

        } catch (Exception ex) {
            Logger.getLogger(HabrSample.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HabrSample.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

}