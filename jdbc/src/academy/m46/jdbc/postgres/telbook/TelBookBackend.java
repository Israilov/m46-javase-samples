package academy.m46.jdbc.postgres.telbook;

import java.nio.channels.InterruptedByTimeoutException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TelBookBackend {

    public List<AbstractContact> getContacts(int personsId) throws ClassNotFoundException, SQLException {
        String url = "jdbc:postgresql://127.0.0.1:5432/m46postgres_sample_db";
        String username = "m46postgres_sample_user";
        String password = "q";
        Class.forName("org.postgresql.Driver");

        Connection connection = DriverManager.getConnection(url, username, password);

        PreparedStatement stmt = connection.prepareStatement("select * from contacts where personsId = ?");
        stmt.setInt(1, personsId);

        ResultSet rs = stmt.executeQuery();

        List<AbstractContact> resultContacts = new ArrayList<AbstractContact>();

        while(rs.next()) {
            int contactId = 0;
            String contactType = null;

            contactId = rs.getInt("id");
            contactType = rs.getString("contact_type");

            AbstractContact contact = null;

            switch (contactType) {
                case "TEL":
                    String tel = rs.getString("tel");
                    contact = new TelContact(contactId, tel, personsId);
                    break;
                case "EMAIL":
                    String email = rs.getString("email");
                    contact = new EmailContact(contactId, email, personsId);
                    break;
                case "FBACC":
                    String fbacc = rs.getString("fbacc");
                    contact = new FbContact(contactId, fbacc, personsId);
                    break;
                case "ADDRESS":
                    int countryId = rs.getInt("country_id");
                    int cityId = rs.getInt("city_id");
                    String streetName = rs.getString("street_name");
                    String buildingNumber = rs.getString("building_number");
                    String flatNumber = rs.getString("flat_number");
                    contact = new AddressContact(contactId, personsId, countryId, cityId, streetName, buildingNumber, flatNumber);
                    break;
            }

            resultContacts.add(contact);

        }

        return resultContacts;

    }

}

abstract class AbstractContact {
    public int id;
    public int personsId;

    public AbstractContact(int contactId, int personsId) {
        this.id = contactId;
        this.personsId = personsId;
    }
}

class TelContact extends AbstractContact {
    public String tel;

    public TelContact(int contactId, String tel, int personsId) {
        super(contactId, personsId);
        this.tel = tel;
    }
}

class EmailContact extends AbstractContact {
    public String email;

    public EmailContact(int contactId, String email, int personsId) {
        super(contactId, personsId);
        this.email = email;
    }
}

class FbContact extends AbstractContact {
    public String fbAccount;

    public FbContact(int contactId, String fbacc, int personsId) {
        super(contactId, personsId);
        this.fbAccount = fbacc;
    }
}

class AddressContact extends AbstractContact {
    public int countryId;
    public int cityId;
    public String streetName;
    public String buildingNumber;
    public String flatNumber;

    public AddressContact(int contactId, int personsId, int countryId, int cityId, String streetName, String buildingNumber, String flatNumber) {
        super(contactId, personsId);
        this.streetName = streetName;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
    }
}