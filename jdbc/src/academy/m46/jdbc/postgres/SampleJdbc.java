package academy.m46.jdbc.postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class SampleJdbc {

    public static void main(String[] args) throws Exception {

        Class.forName("org.postgresql.Driver");

        Connection connection = DriverManager.getConnection("jdbc:postgresql://178.88.175.72:5432/kmk109", "komek_109_user", "123");

        Statement statement = connection.createStatement();

        statement.execute("insert into events (address, event_name, event_description) values ('!!!addr 6', 'test event', 'test event description')");

        connection.close();

    }

}
