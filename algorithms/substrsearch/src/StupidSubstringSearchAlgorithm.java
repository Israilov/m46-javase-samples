public class StupidSubstringSearchAlgorithm {

  public static void main(String[] args) {

    char[] charArr = new char[]
        {'v', 'e', ' ',
            'e', 's', 'e', 'v', 'e', 'o', 'n', 'e', ',', ' ',
            'l', 'i', 's', 'e', 'r', 'y', 'e', 'v', 'e'};

    char[] searchableArr = new char[]{'v', 'e', 'r', 'y'};

    int i = 0, j = 0;

    while (i < charArr.length) {

      if (charArr[i] == searchableArr[0]) {

        j = 1;

        while (j < searchableArr.length && (i+j) < charArr.length) {
          if (charArr[i + j] != searchableArr[j]) {
            break;
          }
          j++;
        }

        if (j == searchableArr.length) {
          System.out.println("Searchable searchableArr[] subsequence found at position: " + i + " in charArr[]");
          break;
        }

      }

      i++;
    }

    if (j < searchableArr.length) {
      System.out.println("Searchable searchableArr[] subsequence was not found in charArr[]");
    }


  }
}
