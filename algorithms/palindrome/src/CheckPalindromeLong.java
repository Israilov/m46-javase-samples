public class CheckPalindromeLong {

  public static void main(String[] args) {

    long palindrome = 123454321; //only positive numbers
    long tmp = palindrome;
    long rev = 0;

    if (palindrome != 0 && (palindrome - palindrome % 10) > 0) {

      while (tmp > 0) {
        rev = rev * 10 + tmp % 10;
        if (rev == tmp || rev > tmp) {
          break;
        }
        tmp = tmp / 10;
      }

      System.out.println(rev == tmp ? "It is a palindrome" : "No it is not a palindrome");

    } else {
      System.out.println("Number is empty or a single digit number - not a palindrome");
    }
  }

}