public class CheckPalindromeStr {

  public static void main(String[] args) {

    char[] palindrome = new char[]{'a','b','g','b','f'};

    for (int i=0; i<palindrome.length/2; i++) {
      if (palindrome[i]!=palindrome[palindrome.length-i-1]) {
        System.out.println("Not a palindrome");
        return;
      }
    }

    System.out.println("It's a palindrome");
  }

}
