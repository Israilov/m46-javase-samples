import java.util.Arrays;

public class MinSortDigits {

  public static void main(String[] args) {
    int[] array = new int[]{5,6,3,4,7,9,1,0,-1,-2};
    minSort(array);
    Arrays.stream(array).forEach(System.out::print);
  }

  private static void minSort(int[] array) {

    for (int i=0; i<array.length; i++) {
      int min = array[i];
      int minIndex = i;
      for (int j=i; j<array.length; j++) {
        if (min>array[j]) {
          min = array[j];
          minIndex = j;
        }
      }
      int tmp = array[i];
      array[i] = min;
      array[minIndex] = tmp;
    }

  }

}
