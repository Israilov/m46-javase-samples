import java.util.Arrays;

public class BubbleSort {

  public static void main(String[] args) {

    int[] array = bubbleSortIt();

    Arrays.stream(array).forEach(System.out::print);

  }

  private static int[] bubbleSortIt() {

    int[] array = new int[]{1, 2, 3, 4, 5, 6, 7};
    int count = 0;
    boolean isPairMutuallyExchanged = false;

    for (int i = 0; i < array.length; i++) {

      isPairMutuallyExchanged = false;

      for (int j = 1; j < array.length; j++) {

        if (array[j-1]>array[j]) {

          int tmp = array[j-1];
          array[j-1] = array[j];
          array[j] = tmp;
          isPairMutuallyExchanged = true;

        }

        System.out.println(count++);
      }

      if (!isPairMutuallyExchanged) {
        break;
      }

    }
    return array;
  }
}
