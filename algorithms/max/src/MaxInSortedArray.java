public class MaxInSortedArray {

  public static void main(String[] args) {
    int[] array = new int[]{1,2,3,4,5,6,7,8,9};
    int maxElement = getMaxElement(array);
    System.out.println(maxElement);
  }

  private static int getMaxElement(int[] array) {

    int max = array[0];

    for (int i=1; i<array.length; i++) {
      if (max<array[i]) {
        max = array[i];
      }
    }

    return max;
  }
}