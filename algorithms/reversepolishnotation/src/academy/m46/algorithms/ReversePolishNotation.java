package academy.m46.algorithms;

import java.util.Stack;

public class ReversePolishNotation {

  public static void main(String[] args) {

    String opn = "4 5 + 6 *";

    Stack stack = new Stack();

    /*
     * split() разбивает строку на подстроки разделённые пробелом и возвращает массив
     * строк. Например по строке "4 5 + 6 *" split() вернёт следующий массив:
     * {"4","5","+","6","*"}
     */
    String[] opnArr = opn.split(" ");

    for (int i=0; i<opnArr.length; i++) {

      if (opnArr[i].trim().length()==1 && (opnArr[i].charAt(0)<48 || opnArr[i].charAt(0)>48)) {
        stack.push(new Operand(opnArr[i].charAt(0)));
      } else {
        stack.push(new Integer(opnArr[i]));
      }

      if (stack.peek() instanceof Operand) {

        Operand operand = (Operand) stack.pop();

        int number1 = (Integer) stack.pop();
        int number2 = (Integer) stack.pop();

        if (operand.getOperand()=='+') stack.push(number1 + number2);
        if (operand.getOperand()=='-') stack.push(number1 - number2);
        if (operand.getOperand()=='*') stack.push(number1 * number2);
        if (operand.getOperand()=='/') stack.push(number2 / number1);
      }

    }

    System.out.println(opn+"="+stack.peek());

  }

}

class Operand {

  public char getOperand() {
    return operand;
  }

  public void setOperand(char operand) {
    this.operand = operand;
  }

  private char operand;
  public Operand(char operand) {
    this.operand = operand;
  }

}
