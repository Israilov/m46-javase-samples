public class ArraySum {

  public static void main(String[] args) {
    int[] array = new int[]{1,2,3,4,5,6,7};
    int totalSum = getSum(array);
    System.out.println(totalSum);
  }

  public static int getSum(int[] array) {
    int sum = 0;

    for (int i=0; i<array.length; i++) {
      sum+=array[i];
    }

    return sum;
  }
}
