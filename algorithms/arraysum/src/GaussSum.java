public class GaussSum {

  public static void main(String[] args) {

    int[] array = new int[]{1,3,5,7,9,11,13,15,17,19,21};
    int sumResult = getSumGauss(array);
    System.out.println(sumResult);
  }

  public static int getSumGauss(int[] array) {
    return (int)((array[0]+array[array.length-1])*(array.length/2.0));
  }

}
