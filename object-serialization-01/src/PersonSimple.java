import java.io.Serializable;

public class PersonSimple implements Serializable {

  public String name;
  public String surname;
  public Phone phone;

}
