import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class AppMain {

  public static void main(String[] args) throws IOException, ClassNotFoundException {

    PersonSimple personSimple = new PersonSimple();

    personSimple.name = "Anzor";
    personSimple.surname = "Israilov";

    personSimple.phone = new Phone();
    personSimple.phone.countryCode = "+7";
    personSimple.phone.msisdn = "7012003040";

    FileOutputStream outputStream = new FileOutputStream("./data/person.bin");
    ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

    System.out.println("Writing PersonSimple object as series of bytes: ");
    objectOutputStream.writeObject(personSimple);
    System.out.println("Done.");

    objectOutputStream.close();

    FileInputStream inputStream = new FileInputStream("./data/person.bin");
    ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

    PersonSimple personSimple2 = (PersonSimple) objectInputStream.readObject();

    System.out.println("\nReading PersonSimple object as series of bytes: ");
    System.out.println("Name: "+personSimple2.name);
    System.out.println("Surname: "+personSimple2.surname);
    System.out.println("Phone.countryCode: "+personSimple2.phone.countryCode);
    System.out.println("Phone.msisdn: "+personSimple2.phone.msisdn);

    inputStream.close();

  }

}
