import com.google.gson.Gson;

public class AppMain {

  public static void main(String[] args) {

    PersonSimple personSimple1 = new PersonSimple();

    personSimple1.name = "Anzor";
    personSimple1.surname = "Israilov";

    personSimple1.phone = new Phone();
    personSimple1.phone.countryCode = "+7";
    personSimple1.phone.msisdn = "7012003040";

    Gson gson = new Gson();

    String jsonStr = gson.toJson(personSimple1);

    System.out.println("Print object as json: " + jsonStr);

    PersonSimple personSimple2 = gson.fromJson(jsonStr, PersonSimple.class);

    System.out.println("\n Pring object param: "
        + "\n name: " + personSimple2.name
        + "\n surname: " + personSimple2.surname
        + "\n phone.countryCode: " + personSimple2.phone.countryCode
        + "\n phone.msisdn: " + personSimple2.phone.msisdn);

  }

}
