package models;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class PlanEntity {
    private int planId;
    private String planType;
    private String planTerm;

    public PlanEntity(String type, String term) {
        this.planType= type;
        this.planTerm = term;
    }

}
