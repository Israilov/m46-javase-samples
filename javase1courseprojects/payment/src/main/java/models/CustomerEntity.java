package models;

import dto.Customer;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class CustomerEntity {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String telephone;
    private String iin;
    private String city;
    private String street;
    private int houseNumber;
    private int flatNumber;
    private String stripeId;

    public CustomerEntity(String stripeId, String name, String surname, String iin, String city, String street, int houseNumber, int flatNumber, String email, String telephone) {
        this.stripeId = stripeId;
        this.name = name;
        this.surname = surname;
        this.iin = iin;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.email = email;
        this.telephone = telephone;
    }
}
