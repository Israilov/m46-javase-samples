package models;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class CardEntity {
    private int id;
    private int customerId;
    private String token;

    public CardEntity(int customer_id, String token) {
        this.customerId = customer_id;
        this.token = token;
    }
}
