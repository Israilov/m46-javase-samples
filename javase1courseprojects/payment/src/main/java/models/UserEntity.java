package models;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class UserEntity {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private boolean activation;
}


