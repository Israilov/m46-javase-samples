package models;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class InvoiceEntity {
    private int id;
    private int orderId;
    private int paymentAmount;
    private String stripeId;
    private Date paymentDate;
    private Date paidDate;
    private String comment;

    public InvoiceEntity(int orderId,int paymentAmount) {
        this.paymentAmount = paymentAmount;
        this.orderId = orderId;
    }
}
