package models;

import lombok.*;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class OrderEntity {
    private int id;
    private int customerId;
    private int paymentPlanId;
    private int orderCode;
    private Date orderDate;
    private int totalAmount;
    private int leftAmount;
    private String status;
    private CustomerEntity customerEntity;


    public OrderEntity(int id, int customerId, int paymentPlanId, int orderCode, Date orderDate, int totalAmount, int leftAmount, String status) {
        this.id = id;
        this.customerId = customerId;
        this.paymentPlanId = paymentPlanId;
        this.orderCode = orderCode;
        this.orderDate = orderDate;
        this.totalAmount = totalAmount;
        this.leftAmount = leftAmount;
        this.status = status;
    }

    public OrderEntity(String status, Date date, int totalAmount, int leftAmount, int orderCode) {
        this.status = status;
        this.leftAmount= leftAmount;
        this.orderDate = date;
        this.totalAmount = totalAmount;
        this.orderCode = orderCode;
    }
}
