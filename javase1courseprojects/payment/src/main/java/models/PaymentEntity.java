package models;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class PaymentEntity {
    private int id;
    private int cardId;
    private int orderId;
    private int invoiceId;
    private Date paymentDate;
    private int amount;
    private String comment;
    private OrderEntity orderEntity;

    public PaymentEntity(int id, int cardId, int orderId, int invoiceId, Date paymentDate, int amount, String comment) {
        this.id = id;
        this.cardId = cardId;
        this.orderId = orderId;
        this.invoiceId = invoiceId;
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.comment = comment;
    }
}
