package controlles;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import models.*;
import org.modelmapper.ModelMapper;
import services.PaymentService;
import dto.Order;

import java.sql.Timestamp;
import java.util.Date;
import services.CheckOrder;
import java.io.IOException;

public class PaymentsController {
    ObjectMapper mapper = new ObjectMapper();
    public String charge(String jsonBody) throws PaymentSysException {

        try {

            ModelMapper modelMapper = new ModelMapper();
            PaymentService paymentService = new PaymentService();

            Order order = mapper.readValue(jsonBody,Order.class);
            Date date = new Date();

            order.setOrderDate(date);

            CustomerEntity customerEntity = modelMapper.map(order.getCustomer(),CustomerEntity.class);

            PlanEntity planEntity = modelMapper.map(order.getPlan(),PlanEntity.class);

            OrderEntity orderEntity = modelMapper.map(order,OrderEntity.class);

            orderEntity.setPaymentPlanId(paymentService.addPlanInDB(planEntity));
            orderEntity.setCustomerId(paymentService.addCustInDB(customerEntity));

            InvoiceEntity invoiceEntity = new InvoiceEntity(paymentService.addOrderInDB(orderEntity), orderEntity.getTotalAmount());
            order.getCustomer().setStripeId("cus_DlaZi5lcQoQoRO");
            return mapper.writeValueAsString(paymentService.addInvoice(invoiceEntity, order.getCustomer().getStripeId()));

        } catch (PaymentSysException e) {

            throw e;

        } catch (JsonProcessingException e) {

            throw new PaymentSysException(PaymentSysErrors.API_ERROR);

        } catch (IOException e) {

            throw new PaymentSysException(PaymentSysErrors.API_ERROR);

        }
        return "{}";
    }
}

