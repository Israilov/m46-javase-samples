package routers;

import models.PaymentEntity;
import services.PaymentService;
import services.UserService;
import util.SecurityUtil;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@WebServlet("/web/*")
public class WebRouterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        route(req, resp);
    }

    private void route(HttpServletRequest req, HttpServletResponse resp){
        String uri =  req.getRequestURI();
        try {
            switch (uri) {
                case "/web/login": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    if (email == null || password == null) {
                        req.getRequestDispatcher("/login.jsp").forward(req, resp);
                    }else{
                        UserService userService = new UserService();
                        boolean bln = userService.checkMail(email, password);
                        if(bln){
                            HttpSession httpSession = req.getSession(true);
                            httpSession.setAttribute("email", email);
                            resp.sendRedirect("/web/home");
                        }else{
                            req.getRequestDispatcher("/login.jsp").forward(req, resp);
                        }
                    }
                    break;
                }
                case "/web/home": {
                    String sessionEmail = (String) req.getSession().getAttribute("email");
                    if(sessionEmail == null){
                        resp.sendRedirect("/web/login");
                    }else {
                        req.getRequestDispatcher("/search.jsp").forward(req,resp);
                    }
                }
                case "/web/index": {
                    String name = req.getParameter("name");
                    resp.getWriter().write(name + " ");
                    break;
                }
                case "/web/search": {
//                    String sessionEmail = (String) req.getSession().getAttribute("email");
//                    if(sessionEmail == null){
//                        resp.sendRedirect("/web/login");
//                    }else {
                    String since = req.getParameter("start");
                    String to = req.getParameter("end");

                    String[] arr1 = since.split("-");
                    String[] arr2 = to.split("-");

                    Calendar cal1 = new GregorianCalendar(Integer.parseInt(arr1[0]), Integer.parseInt(arr1[1])-1, Integer.parseInt(arr1[2]));
                    Date start = cal1.getTime();
                    Calendar cal2 = new GregorianCalendar(Integer.parseInt(arr2[0]), Integer.parseInt(arr2[1])-1, Integer.parseInt(arr2[2]));
                    Date end = cal2.getTime();

                    System.out.println(start.toString());
                    System.out.println(end.toString());

                    PaymentService paymentService = new PaymentService();
                    ArrayList<PaymentEntity> arrayList = (ArrayList) paymentService.searchPaymentByDate(start,end);
                    for(PaymentEntity pe: arrayList){
                        System.out.println(pe.toString());
                    }
//                        String type = req.getParameter("type");
//                        switch (type) {
//                            case "order": {
//                                req.getRequestDispatcher("/order.jsp").forward(req, resp);
//                            }
//                            case "payment": {
//                                req.getRequestDispatcher("/payment.jsp").forward(req, resp);
//                            }
//
//                        }

                        break;
                    }
                }
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }
}
