package routers;

import com.fasterxml.jackson.databind.ObjectMapper;
import controlles.PaymentSysException;
import controlles.PaymentsController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet("/api/*")
public class ApiRouterServlet extends HttpServlet {

  static ObjectMapper mapper = new ObjectMapper();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    route(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    route(req, resp);
  }

  private void route(HttpServletRequest req, HttpServletResponse resp) throws IOException {

    String uri = req.getRequestURI();

    BufferedReader reader = req.getReader();

    String s = "";
    String body = "";

    while ((s = reader.readLine()) != null) {
      body += s;
    }
    System.out.println("#BodyInRouter: " + body);
    String respJson = "";
    int httpStatus = 200;

    try {
      switch (uri) {
        case "/api/payment/add": {
          PaymentsController payments = new PaymentsController();
          respJson = payments.charge(body);
        }
        case "/api/payment/status": {
          PaymentsController payments = new PaymentsController();
          //respJson = payments.getStatus(body);
        }
      }
    } catch (PaymentSysException e) {
      respJson = mapper.writeValueAsString(e);

      switch (e.getError()) {
        case CARD_ERROR:
          httpStatus = HttpServletResponse.SC_NOT_FOUND;
          break;
        default:
          httpStatus = HttpServletResponse.SC_SEE_OTHER;
      }

    }

    resp.setStatus(httpStatus);
    resp.getWriter().write(respJson + "\n");
    resp.getWriter().flush();
  }

  private void restRequest(String url, String json, int countdown) throws IOException {
//      http://localhost:8080/api/payment/add
    URL obj = new URL(url);
    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json");

    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(json);
    wr.flush();
    wr.close();

    int responseCode = con.getResponseCode();
    if (responseCode != 200 && countdown > 0) {
      try {
        Thread.sleep(500);
      } catch (Exception e) {
      }
      restRequest(url, json, countdown--);
    }


  }
}
//curl -H "Content-Type: application/json" -X POST -d '{"orderCode":1,"totalAmount":1000,"date":1540562406391,"status":"status","plan":{"term":"11","type":"22"},"customer":{"stripeId":"1","name":"Bilal","surname":"Ukhayev","iin":"777","city":"Almaty","street":"Botakara","houseNumber":5,"flatNumber":5,"email":"email@email","telephone":"7766","card":{"token":"111"}}}' http://localhost:8080/api/payment/add
