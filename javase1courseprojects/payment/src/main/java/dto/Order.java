package dto;

import java.util.Date;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Order {
    private int orderCode;
    private int totalAmount;
    private Date orderDate;
    private String status;
    private Plan plan;
    private Customer customer;

}
