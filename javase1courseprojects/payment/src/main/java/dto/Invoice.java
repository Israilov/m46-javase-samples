package dto;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Invoice {
    private int paymentAmount;
    private String comment;
    private String stripeId;
    private String paymentDate;

}
