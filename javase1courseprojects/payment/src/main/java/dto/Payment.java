package dto;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Payment {
    private String cardId;
    private String orderId;
    private String invoiceId;
    private String paymentDate;
    private String amount;
    private String comment;

}
