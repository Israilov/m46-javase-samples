package dto;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Plan {
    private String  term;
    private String type;
}
