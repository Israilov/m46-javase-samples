package dto;
import dto.Card;
import lombok.*;
import models.CustomerEntity;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Customer{
    private String stripeId;
    private String name;
    private String surname;
    private String iin;
    private String city;
    private String street;
    private int houseNumber;
    private int flatNumber;
    private String email;
    private String telephone;
    private Card card;

}
