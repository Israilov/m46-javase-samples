package dto;

import lombok.*;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Builder
public class Card  {
    private String token;
}
