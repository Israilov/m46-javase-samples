package services;

import models.OrderEntity;
import org.apache.ibatis.session.SqlSession;
import repositories.CustomerMapper;
import repositories.OrderMapper;
import util.MyBatisSqlSessionFactory;

import java.util.List;

public class OrderService {
    public OrderEntity getOrderById(int id){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            return orderMapper.getOrderById(id);
        }finally {
            sqlSession.close();
        }
    }
}
