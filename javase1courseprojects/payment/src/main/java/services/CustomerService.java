package services;

import org.apache.ibatis.session.SqlSession;
import repositories.CustomerMapper;
import util.MyBatisSqlSessionFactory;

import java.util.List;

public class CustomerService {
    public List getAllCustomers(){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            return customerMapper.getAllCustomers();
        }finally {
            sqlSession.close();
        }
    }
    public List searchByNameAndSurname(String request){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            return customerMapper.searchByNameAndSurname(request);
        }finally {
            sqlSession.close();
        }
    }
}
