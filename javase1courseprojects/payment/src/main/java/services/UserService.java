package services;

import models.UserEntity;
import org.apache.ibatis.session.SqlSession;
import repositories.UserMapper;
import util.MyBatisSqlSessionFactory;

public class UserService {
    public boolean checkMail(String email, String password) {
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            UserEntity userEntity = userMapper.checkData(email, password);
            if (userEntity != null) {
                return true;
            }
        }
        return false;
    }
}
