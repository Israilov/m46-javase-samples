package services;
import com.stripe.exception.StripeException;
import controlles.PaymentSysErrors;
import controlles.PaymentSysException;
import dto.Invoice;
import models.*;
import org.apache.ibatis.session.SqlSession;
import repositories.*;
import util.MyBatisSqlSessionFactory;
import com.stripe.model.Charge;
import com.stripe.Stripe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentService  {
    public List getPayments(){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try {
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            return paymentMapper.getPayments();
        }finally {
            sqlSession.close();
        }
    }

    public List searchPaymentByDate(Date start, Date end){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            PaymentMapper paymentMapper = sqlSession.getMapper(PaymentMapper.class);
            return paymentMapper.searchPaymentByDate(start,end);
        }finally {
            sqlSession.close();
        }
    }


    public Invoice addInvoice(InvoiceEntity invoiceEntity,String custStripeId){
        try {
            Stripe.apiKey = "sk_test_NZxWajazDoCTx898BK7iHZSl";

            Map<String, Object> chargeParam = new HashMap<String, Object>();
            chargeParam.put("amount", invoiceEntity.getPaymentAmount());
            chargeParam.put("currency", "usd");
            chargeParam.put("customer", custStripeId);

            Date paymentDate = new Date();
            invoiceEntity.setComment("ISSUED");
            invoiceEntity.setStripeId(Charge.create(chargeParam).getId());
            invoiceEntity.setPaymentDate(paymentDate);
        }
        catch (StripeException e){
            System.out.println(e.getStatusCode());//200
            System.out.println(e.getCode());//Код ошибки
            throw new PaymentSysException(PaymentSysErrors.CARD_ERROR);
        }
        catch (Exception e){
            invoiceEntity.setComment("TECHNICAL_ERROR");
        }
        finally {
            return addInvoiceInDB(invoiceEntity);
        }
    }

    private Invoice addInvoiceInDB(InvoiceEntity invoiceEntity) {
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            InvoiceMapper invoiceMapper= sqlSession.getMapper(InvoiceMapper.class);
            invoiceMapper.insertInvoice(invoiceEntity);
            sqlSession.commit();

        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally{
            sqlSession.close();
        }
        return new Invoice();
    }


    public int addCustInDB(CustomerEntity customer){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            customerMapper.insertCustomer(customer);

            int i= customerMapper.getLastCustomerId();

            sqlSession.commit();
            return i;
        }
        finally{
            sqlSession.close();
        }
    }

    public int addOrderInDB(OrderEntity order){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            orderMapper.insertOrder(order);

            int i = orderMapper.getLastOrderId();

            sqlSession.commit();

            return i;
        }finally{
            sqlSession.close();
        }
    }
    public int addPlanInDB(PlanEntity plan){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            PlanMapper planMapper = sqlSession.getMapper(PlanMapper.class);
            planMapper.insertPlan(plan);

            int id = planMapper.getLastPlanId();

            sqlSession.commit();
            return id;
        }finally{
            sqlSession.close();
        }
    }
    public int addCardInDB(CardEntity card){
        SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
        try{
            CardMapper cardMapper = sqlSession.getMapper(CardMapper.class);
            cardMapper.insertCard(card);

            int id = cardMapper.getLastCardId();

            sqlSession.commit();
            return id;
        }finally{
            sqlSession.close();
        }
    }

    //Error processing STRIPE
    private String treatmentStripeError(String codeError){
        if(codeError.equalsIgnoreCase("200")){
            return "ISSUED";
        }
        else if(codeError.equalsIgnoreCase("400")){
            return "BAD REQUEST";
        }
        else if(codeError.equalsIgnoreCase("401")){
            return "UNAUTHORIZED";
        }
        else if(codeError.equalsIgnoreCase("402")){
            return "REQUEST FAILED";
        }
        else if(codeError.equalsIgnoreCase("404")){
            return "NOT FOUND";
        }
        else if(codeError.equalsIgnoreCase("409")){
            return "CONFLICT";
        }
        else if(codeError.equalsIgnoreCase("429")){
            return "TOO MANY REQUESTS";
        }
        else if(codeError.equalsIgnoreCase("500") || codeError.equalsIgnoreCase("502")|| codeError.equalsIgnoreCase("503") || codeError.equalsIgnoreCase("504")){
            return "SERVET STRIPE ERRORS";
        }
        else if(codeError.equalsIgnoreCase("api_connection_error")){
            return "api_connection_error";
        }
        else if(codeError.equalsIgnoreCase("api_error")){
            return "api_error";
        }
        else if(codeError.equalsIgnoreCase("authentication_error")){
            return "authentication_error";
        }
        else if(codeError.equalsIgnoreCase("card_error")){
            return "card_error";
        }
        else if(codeError.equalsIgnoreCase("idempotency_error")){
            return "idempotency_error";
        }
        else if(codeError.equalsIgnoreCase("invalid_request_error")){
            return  "invalid_request_error";
        }
        else if(codeError.equalsIgnoreCase("rate_limit_error")){
            return "rate_limit_error";
        }
        else if(codeError.equalsIgnoreCase("validation_error")){
            return "validation_error";
        }
        return null;
    }
}
