package repositories;

import models.CardEntity;

public interface CardMapper {
    void insertCard(CardEntity cardEntity);
    void deleteCard(Integer cardId);
    void updateCard(CardEntity cardEntity);
    int getLastCardId();
}
