package repositories;

import models.InvoiceEntity;

public interface InvoiceMapper {
    void insertInvoice(InvoiceEntity invoiceEntity);
    void deleteInvoice(Integer invoiceId);
    void upadteInvoice(InvoiceEntity invoiceEntity);

    int getLastInvoiceId();
}
