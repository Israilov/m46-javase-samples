package repositories;

import models.UserEntity;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    void insertUser(UserEntity userEntity);
    void deleteUser(Integer userId);
    void updateUser(UserEntity userEntity);
    UserEntity checkData(@Param("email") String email, @Param("password")String password);
}
