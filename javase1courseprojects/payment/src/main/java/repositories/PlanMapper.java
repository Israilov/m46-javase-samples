package repositories;

import models.PlanEntity;

public interface PlanMapper {
    int insertPlan(PlanEntity planEntity);
    void deletePlan(Integer planId);
    void updatePlan(PlanEntity plan);

    int getLastPlanId();
}
