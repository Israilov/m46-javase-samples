package repositories;

import dto.Order;
import models.OrderEntity;

public interface OrderMapper {
    void insertOrder(OrderEntity orderEntity);
    void deleteOrder(Integer orderId);
    void updateOrder(OrderEntity orderEntity);
    int getLastOrderId();
    OrderEntity getOrderById(int orderId);
}
