package repositories;

import models.PaymentEntity;
import org.apache.ibatis.annotations.Param;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public interface PaymentMapper {
    List getPayments();
    List searchPaymentByDate(@Param("start") Date start, @Param("end") Date end);
    void insertPayment(PaymentEntity paymentEntity);
    void deletePayment(Integer paymentId);
    void updatePayment(PaymentEntity paymentEntity);
}
