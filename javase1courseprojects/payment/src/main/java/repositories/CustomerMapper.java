package repositories;

import models.CustomerEntity;

import java.util.List;

public interface CustomerMapper {
    List getAllCustomers();
    List searchByNameAndSurname(String request);
    void insertCustomer(CustomerEntity customerEntity);
    void deleteCustomer(Integer custId);
    void updateCustomer(CustomerEntity customerEntity);
    int getLastCustomerId();
}
