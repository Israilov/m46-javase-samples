package util;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;

public class MessageSender {
    private static String USER_NAME = "cloudaddressbookmessagesender";
    private static String PASSWORD = "messageSender9520";


    public static void sendMessage(String to) {
//        int userId = SequenceModel.getSequence("users_id_seq");
        String uuid = UUID.randomUUID().toString();
//        RegistrationModel.newUserRegistration(userId, uuid);

        String from = USER_NAME;
        String pass = PASSWORD;
        String subject = "Hi";
        String body = "Click this link to activate your new account:" +
                "\nhttp://localhost:8080/telBookServlet/activation?id="+uuid +
                "\nIf you did not sign up for this account, you may ignore this message" +
                "\nand the account will expire by itself.";

        final Properties properties = System.getProperties();
        String host = "smtp.gmail.com";

        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", host);
        properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.password", pass);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(body);

            Transport transport = session.getTransport("smtp");
            transport.connect(host,from,pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
