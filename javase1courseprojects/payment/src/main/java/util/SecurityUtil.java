package util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecurityUtil {
    public static String getMd5Hash(String valueToHash){
        if (valueToHash!=null) {
            String hashed = null;
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(valueToHash.getBytes());
                byte[] digest = md.digest();
                hashed = DatatypeConverter.printHexBinary(digest).toLowerCase();
            }catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return hashed;
        } else {
            return null;
        }
    }

}
