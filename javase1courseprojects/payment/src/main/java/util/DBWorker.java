package util;

import dto.Order;
import models.CustomerEntity;
import models.OrderEntity;
import models.PaymentEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBWorker {
    private Connection connection;

    public void getConnection()  {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:9520/payment", "postgres", "828487969520");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            if(!connection.isClosed()){
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List getPayments(){
        getConnection();
        List<PaymentEntity> paymentList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select id,card_id,order_id,invoice_id,payment_date,amount,comment from payments"
            );
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                int id = resultSet.getInt(1);
                int cardId = resultSet.getInt(2);
                int orderId = resultSet.getInt(3);
                int invoiceId = resultSet.getInt(4);
                Date paymentDate = resultSet.getDate(5);
                int amount = resultSet.getInt(6);
                String comment = resultSet.getString(7);

                PaymentEntity paymentEntity = new PaymentEntity(id, cardId, orderId, invoiceId, paymentDate, amount, comment);
                paymentEntity.setOrderEntity(getOrderById(orderId));
                paymentList.add(paymentEntity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeConnection();
        return paymentList;
    }

    public OrderEntity getOrderById(int orderId){
        OrderEntity orderEntity = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from orders where id = ?"
            );
            preparedStatement.setInt(1,orderId);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int id = resultSet.getInt(1);
                int customerId = resultSet.getInt(2);
                int paymentPlanId = resultSet.getInt(3);
                int orderCode = resultSet.getInt(4);
                Date orderDate = resultSet.getDate(5);
                int totalAmount = resultSet.getInt(6);
                int leftAmount = resultSet.getInt(7);
                String status = resultSet.getString(8);

                orderEntity = new OrderEntity(id, customerId, paymentPlanId, orderCode, orderDate, totalAmount, leftAmount, status);
                orderEntity.setCustomerEntity(getCustomerById(customerId));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderEntity;
    }

    public CustomerEntity getCustomerById(int customerId){
        CustomerEntity customerEntity = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from customers where id = ?"
            );
            preparedStatement.setInt(1,customerId);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String surname = resultSet.getString(3);
                String email = resultSet.getString(4);
                String telephone = resultSet.getString(5);
                String iin = resultSet.getString(6);
                String city = resultSet.getString(7);
                String street = resultSet.getString(8);
                int houseNumber = resultSet.getInt(9);
                int flatNumber = resultSet.getInt(10);
                String stripeId = resultSet.getString(11);

                customerEntity = new CustomerEntity(id,name,surname,email,telephone,iin,city,street,houseNumber,flatNumber,stripeId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerEntity;
    }
}

