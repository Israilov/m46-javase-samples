<%@ page import="services.OrderService" %>
<%@ page import="models.OrderEntity" %><%--
  Created by IntelliJ IDEA.
  User: lenovo ideapad 110
  Date: 31.10.2018
  Time: 2:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%!OrderService orderService = new OrderService();
    OrderEntity orderEntity = orderService.getOrderById(1);
%>

<div>
    <div>
        <table>
            <thead>
            <tr>
                <th>Status</th>
                <th>Left Amount</th>
                <th>Order Code</th>
                <th>Order Date</th>
                <th>Total Amount</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><%=orderEntity.getStatus()%></td>
                <td><%=orderEntity.getLeftAmount()%></td>
                <td><%=orderEntity.getOrderCode()%></td>
                <td><%=orderEntity.getOrderDate()%></td>
                <td><%=orderEntity.getTotalAmount()%></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
