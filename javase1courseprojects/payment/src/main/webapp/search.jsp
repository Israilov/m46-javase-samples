<%--
  Created by IntelliJ IDEA.
  User: lenovo ideapad 110
  Date: 28.10.2018
  Time: 0:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="date.css">
</head>
<body>
<div>
    <div>
        <form action="/web/search" method="get">
            <fieldset>
                <legend>Search by Date</legend>
                <div>
                    <label for="start">Start</label>
                    <input required type="date" id="start" name="start"
                           value="2018-10-30"
                           min="2018-01-01" max="2020-01-01"/>
                </div>
                <div>
                    <label for="end">End</label>
                    <input required type="date" id="end" name="end"
                           value="2018-10-31"
                           min="2018-01-01" max="2020-01-01"/>
                </div>
                <div>
                    <input style="background-color: #000;color: #fff;" type="submit" value="submit">
                </div>
            </fieldset>
        </form>
    </div>
    <div>
        <form action="/web/search" method="get">
            <fieldset>
                <legend>Search by Type</legend>
                <div>
                    <label for="type">Get All</label>
                    <select required name="type" id="type">
                        <option value="payment">Payments</option>
                        <option value="order">Orders</option>
                    </select>
                </div>
                <div>
                    <input style="background-color: #000;color: #fff;" type="submit" value="submit">
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>
