<%@ page import="services.PaymentService" %>
<%@ page import="models.PaymentEntity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="util.DBWorker" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
    <link rel="stylesheet" type="text/css" href="payment.css">
</head>
<body>
    <%--<%!DBWorker dbWorker = new DBWorker();--%>
    <%--ArrayList<PaymentEntity> paymentList = (ArrayList<PaymentEntity>) dbWorker.getPayments();%>--%>
    <%!PaymentService paymentService = new PaymentService();
    ArrayList<PaymentEntity> paymentList = (ArrayList) paymentService.getPayments();%>
    <div class="limiter">
        <div class="container-table100">
            <div class="wrap-table100">
                <div class="table100">
                    <table>
                        <thead>
                        <tr class="table100-head">
                            <th class="column1">Date</th>
                            <th class="column2">Amount</th>
                            <th class="column3">Comment</th>
                            <th class="column4">Total Amount</th>
                            <th class="column5">Status</th>
                            <th class="column6">Order Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%for(PaymentEntity paymentEntity: paymentList){%>
                        <tr>
                            <td class="column1"><%=paymentEntity.getPaymentDate()%></td>
                            <td class="column2"><%=paymentEntity.getAmount()%></td>
                            <td class="column3"><%=paymentEntity.getComment()%></td>
                            <td class="column4"><%=paymentEntity.getOrderEntity().getTotalAmount()%></td>
                            <td class="column5"><%=paymentEntity.getOrderEntity().getStatus()%></td>
                            <td class="column6"><%=paymentEntity.getOrderEntity().getOrderDate()%></td>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>