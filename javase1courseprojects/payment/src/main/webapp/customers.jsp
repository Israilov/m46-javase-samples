<%@ page import="services.CustomerService" %>
<%@ page import="models.CustomerEntity" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: lenovo ideapad 110
  Date: 27.10.2018
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<%!CustomerService paymentService = new CustomerService();
ArrayList<CustomerEntity> customerList = (ArrayList) paymentService.getAllCustomers();%>;
<body>
    <div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Telephone</th>
                        <th>IIN</th>
                        <th>City</th>
                        <th>Street</th>
                        <th>House Number</th>
                        <th>Flat Number</th>
                    </tr>
                </thead>
                <tbody>
                <%for(CustomerEntity customerEntity: customerList){%>
                    <tr>
                        <td><%=customerEntity.getName()%></td>
                        <td><%=customerEntity.getSurname()%></td>
                        <td><%=customerEntity.getEmail()%></td>
                        <td><%=customerEntity.getTelephone()%></td>
                        <td><%=customerEntity.getIin()%></td>
                        <td><%=customerEntity.getCity()%></td>
                        <td><%=customerEntity.getStreet()%></td>
                        <td><%=customerEntity.getHouseNumber()%></td>
                        <td><%=customerEntity.getFlatNumber()%></td>
                    </tr>
                <%}%>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
