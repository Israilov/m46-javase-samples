create table products
(
  id          serial not null
    constraint products_pkey
    primary key,
  product_id  integer
    constraint products_product_id_fkey
    references order_products,
  brand       varchar(100),
  model       varchar(100),
  color       varchar(100),
  memory_size integer,
  link        varchar(100),
  vendor      varchar(100),
  price       integer,
  quantity    integer
);