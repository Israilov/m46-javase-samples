create table manager
(
  id                  serial not null
    constraint manager_pkey
    primary key,
  email               varchar(100),
  press_mark_password varchar(100)
);