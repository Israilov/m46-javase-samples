create table customers
(
  id        serial not null
    constraint customers_pkey
    primary key,
  name      varchar(100),
  surname   varchar(100),
  iin       varchar(100),
  city      varchar(100),
  street    varchar(100),
  house     varchar(5),
  flat      varchar(10),
  email     varchar(100),
  telephone varchar(100)
);
