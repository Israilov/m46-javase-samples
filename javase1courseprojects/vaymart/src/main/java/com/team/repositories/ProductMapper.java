package com.team.repositories;

import com.team.dto.Product;
import com.team.entity.ProductEntity;

public interface ProductMapper {
    ProductEntity[] getAllMobilePhone();
    void addProduct(Product product);
}
