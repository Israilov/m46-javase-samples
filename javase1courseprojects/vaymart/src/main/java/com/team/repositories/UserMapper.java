package com.team.repositories;

import com.team.dto.User;
import com.team.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    UserEntity checkMail(@Param("email")String email,@Param("password") String password);
    void addUsersInDataBase(User user);
    void updateActivation(User user);
    UserEntity mailCheckForEmployment(@Param("email")String email);
}
