package com.team.repositories;


import com.team.entity.OrderEntity;

public interface OrderMapper {
    void addOrder(OrderEntity orderEntity);
}
