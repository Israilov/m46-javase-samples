package com.team.repositories;

import com.team.dto.Manager;
import com.team.entity.ManagerEntity;
import org.apache.ibatis.annotations.Param;

public interface ManagerMapper {
    void addManager(Manager manager);
    ManagerEntity getManager(@Param("email") String email, @Param("password") String password);
}
