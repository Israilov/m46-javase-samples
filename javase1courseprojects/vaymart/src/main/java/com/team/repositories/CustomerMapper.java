package com.team.repositories;

import com.team.dto.Customer;
import com.team.entity.CustomerEntity;
import org.apache.ibatis.annotations.Param;

public interface CustomerMapper {
    void addCustomer(Customer customer);
    CustomerEntity getCustomer(@Param("customerId")int customerId);
    int getCustomerId();
}
