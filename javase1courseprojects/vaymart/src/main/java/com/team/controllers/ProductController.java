package com.team.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.team.dto.Product;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.ProductService;

import java.util.ArrayList;
import java.util.HashMap;

public class ProductController {

    private static Gson GSON = new GsonBuilder().create();

    public static void setProductFromJsonInDataBase(String json) {

    }

    public static String getAllProductsFromMapToJson() throws VaymartDataBaseException {
        HashMap<String,Product> products = ProductService.getAllProducts();
        return GSON.toJson(products);
    }

    public static String getProductsBySizeFromMapToJson() throws VaymartDataBaseException {
        HashMap<Integer, ArrayList<Product>> products = ProductService.getProductsBySize();
        return GSON.toJson(products);

    }

}
