package com.team.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.team.dto.Cart;
import com.team.dto.CartProducts;
import com.team.dto.Product;
import com.team.exсeptions.VaymartDataBaseException;
import org.modelmapper.ModelMapper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class CartService {

    private static ModelMapper modelMapper = new ModelMapper();
    private static Gson GSON = new GsonBuilder().create();

    public static String routerCookie(HttpServletRequest req, HttpServletResponse resp){
        String vendorCode = req.getParameter("mobile");
        String removeCode = req.getParameter("remove");
        String[] quantityMobilePhone = req.getParameterValues("quantity");

        Cookie[] cookies = req.getCookies();
        Cookie cookie = null;
        for (Cookie cookieTmp : cookies) {
            if (cookieTmp.getName().equals("cookieMart")) {
                cookie = cookieTmp;
            }
        }
        try {
            if (vendorCode != null) {
                return addCookie(resp, cookie, vendorCode);
            } else if (cookie != null && removeCode != null) {
                return removeCookie(cookie, removeCode, resp);
            } else if (quantityMobilePhone != null) {
                return setQuantity(quantityMobilePhone, cookie, resp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String addCookie(HttpServletResponse resp,Cookie cookie,String vendor) throws IOException {
        if(cookie == null){
            ArrayList<Cart> cartList = new ArrayList<>();
            cartList.add(new Cart(vendor,"1"));
            String json = GSON.toJson(cartList);
            resp.addCookie(new Cookie("cookieMart", URLEncoder.encode(json, "UTF-8")));
        }else {
            String json = URLDecoder.decode(cookie.getValue(), "UTF-8");
            ArrayList<Cart> carts = GSON.fromJson(json, new TypeToken<ArrayList<Cart>>(){}.getType());
            if(vendor != null){
                boolean flag = true;
                for(Cart temp : carts){
                    if(temp.getVendorCode().equals(vendor)){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    carts.add(new Cart(vendor, "1"));
                    resp.addCookie(new Cookie("cookieMart",URLEncoder.encode(GSON.toJson(carts), "UTF-8")));
                }
            }
        }
        return "/webui/shop";
    }

    private static String removeCookie(Cookie cookie,String removeVendor,HttpServletResponse resp) throws IOException {
        String json = URLDecoder.decode(cookie.getValue(), "UTF-8");
        ArrayList<Cart> carts = GSON.fromJson(json, new TypeToken<ArrayList<Cart>>(){}.getType());
        for (int i = 0;i<carts.size();i++) {
            Cart cart = carts.get(i);
            if (cart.getVendorCode().equals(removeVendor)) {
                carts.remove(i);
                break;
            }
        }
        resp.addCookie(new Cookie("cookieMart",URLEncoder.encode(GSON.toJson(carts), "UTF-8")));
        return "/webui/cart";
    }

    private static String setQuantity(String [] quantityMobilePhone, Cookie cookie, HttpServletResponse resp){
        for(String string: quantityMobilePhone){
            //System.out.println(string);
        }
        return "/webui/checkout";
    }
}
