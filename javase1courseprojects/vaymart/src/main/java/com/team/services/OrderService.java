package com.team.services;

import com.team.entity.OrderEntity;
import com.team.enums.*;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.exсeptions.VaymartStripeException;
import com.team.repositories.OrderMapper;
import com.team.utils.MyBatisSqlSessionFactory;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.Date;

public class OrderService {

    public static void addOrderInDataBase(String customerId, Date date, String month, String token) throws VaymartDataBaseException, VaymartStripeException {
        long totalAmount = 0;//StripeApiService.getAmountToken(token);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCustomerId(Integer.parseInt(customerId));
        orderEntity.setStatus(GlobalStatus.DEFAULT);
        orderEntity.setDate(date);
        orderEntity.setTotalAmount(totalAmount);
        if (month != null){
            orderEntity.setMonth(Integer.parseInt(month));
        }
        orderEntity.setPaymentStatus(PaymentStatus.DEFAULT);
        orderEntity.setWareHouseStatus(WareHouseStatus.DEFAULT);
        orderEntity.setDeliveryStatus(DeliveryStatus.DEFAULT);
        // Узнать и реализовать enum в Data base...

        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            orderMapper.addOrder(orderEntity);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

}
