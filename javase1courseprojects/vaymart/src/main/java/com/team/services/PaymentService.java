package com.team.services;

import com.team.dto.Card;
import com.team.dto.Customer;
import com.team.dto.Plan;
import com.team.exсeptions.VaymartStripeException;
import org.modelmapper.ModelMapper;

public class PaymentService {

    private static ModelMapper modelMapper = new ModelMapper();

    public static void addOrder(String month, Customer customer,String token) throws VaymartStripeException {
        String paymentPlan;
        long totalAmount = 0;//StripeApiService.getAmountToken(token);

        if(month == null){
            paymentPlan = "full-payment";
        }else{
            paymentPlan = "installment-payment";
        }
        Plan plan = new Plan(paymentPlan, month);
        Card card = new Card(token);
    }
}
