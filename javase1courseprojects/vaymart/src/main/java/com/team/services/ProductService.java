package com.team.services;
import com.team.dto.Product;
import com.team.entity.ProductEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.repositories.ProductMapper;
import com.team.utils.MyBatisSqlSessionFactory;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ProductService {

    private static ModelMapper modelMapper = new ModelMapper();

    public static HashMap<String,Product> getAllProducts() throws VaymartDataBaseException {
        HashMap<String,Product> hashMap = new HashMap<>();
        try{
            SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            ProductEntity [] productEntities = productMapper.getAllMobilePhone();
            for (ProductEntity p : productEntities){
                Product product = new Product(p.getBrand(),p.getModel(),p.getColor(),p.getMemorySize(),p.getLink(),p.getVendor(),p.getPrice(),p.getQuantity());
                hashMap.put(product.getVendorCode(),product);
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return hashMap;
    }

    public static void addProduct(Product product) throws VaymartDataBaseException {
//        ProductEntity productEntity = new ProductEntity();
//        modelMapper.map(product,productEntity);
//        System.out.println(productEntity);
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            productMapper.addProduct(product);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public static HashMap<Integer, ArrayList<Product>> getProductsBySize() throws VaymartDataBaseException {
        int size = 12;
        HashMap<Integer,ArrayList<Product>> hashMap = new HashMap<>();
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            ProductEntity [] productEntities = productMapper.getAllMobilePhone();
            ArrayList<Product> list = new ArrayList<>();
            for (int i = 0; i<productEntities.length; i++) {
                ProductEntity p = productEntities[i];
                list.add(new Product(p.getBrand(),p.getModel(),p.getColor(),p.getMemorySize(),p.getLink(),p.getVendor(),p.getPrice(),p.getQuantity()));
                if ((i + 1) == productEntities.length && list.size() != size) {
                    hashMap.put((i + 1) / size, list);
                }else if ((i + 1) % size == 0) {
                    hashMap.put((i + 1) / size, list);
                    list = new ArrayList<>();
                }
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return hashMap;
    }

}
