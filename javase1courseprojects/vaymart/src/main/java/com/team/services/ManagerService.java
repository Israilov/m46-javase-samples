package com.team.services;

import com.team.dto.Manager;
import com.team.entity.ManagerEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.repositories.ManagerMapper;
import com.team.utils.MyBatisSqlSessionFactory;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public class ManagerService {

    private static ModelMapper modelMapper = new ModelMapper();

    public static boolean checkManager(Manager manager) throws VaymartDataBaseException {
        try {
            SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
            ManagerEntity managerEntity = sqlSession.getMapper(ManagerMapper.class).getManager(manager.getEmail(),manager.getPassword());
            sqlSession.close();
            return managerEntity != null;
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

}
