package com.team.services;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Token;
import com.team.enums.StripeError;
import com.team.exсeptions.VaymartStripeException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class StripeApiService {

    public static final String STRIPE_API_KEY = Stripe.apiKey = "sk_test_4eC39HqLyjWDarjtT1zdp7dc";

    public static String getCustomerId(com.team.dto.Customer customer, String token) throws VaymartStripeException {
        Map<String, Object> customerParam = new HashMap<>();
        customerParam.put("email", customer.getEmail());
        try {
            Customer customerTmp = Customer.create(customerParam);
            try {
                Token tokenStr = Token.retrieve(token);
                Map <String , Object> source = new HashMap<>();
                source.put("source", tokenStr.getId());
                Objects.requireNonNull(customerTmp).getSources().create(source);
                return customerTmp.getId();
            } catch (StripeException e) {
                throw new VaymartStripeException(StripeError.CARD_NUMBER_ERROR, "Incorrect token");
            }
        } catch (StripeException e) {
            throw new VaymartStripeException(StripeError.EMAIL_ERROR, "Incorrect email");
        }
    }

    public static long getAmountToken(String token) throws VaymartStripeException {
        // Кидает null pointer exception нужно разобраться в чем проблема!
        try {
            return Token.retrieve(token).getAmount();
        } catch (StripeException e) {
            throw new VaymartStripeException(StripeError.CARD_NUMBER_ERROR, "Incorrect token");
        }
    }
}
