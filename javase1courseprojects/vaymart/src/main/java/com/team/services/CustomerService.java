package com.team.services;

import com.team.dto.Customer;
import com.team.entity.CustomerEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.repositories.CustomerMapper;
import com.team.utils.MyBatisSqlSessionFactory;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public class CustomerService {

    private static ModelMapper modelMapper = new ModelMapper();

    public static int addCustomerInDataBase(Customer customer) throws VaymartDataBaseException {
//        CustomerEntity customerEntity = new CustomerEntity();
//        modelMapper.map(customer,customerEntity);
//        System.out.println(customerEntity);
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            customerMapper.addCustomer(customer);
            sqlSession.commit();
            return customerMapper.getCustomerId();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public static Customer getCustomerById(int customerId) throws VaymartDataBaseException {
        Customer customer = new Customer();
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            CustomerEntity customerEntity = customerMapper.getCustomer(customerId);
            modelMapper.map(customerEntity,customer);
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return customer;
    }

}
