package com.team.services;

import com.team.entity.UserEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.utils.ActivatorMail;
import com.team.utils.MyBatisSqlSessionFactory;
import com.team.dto.User;
import com.team.repositories.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import javax.mail.MessagingException;
import java.io.IOException;

public class UserService {

    private static ModelMapper modelMapper = new ModelMapper();

    public boolean checkMail(String email, String password) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            UserEntity userEntity = userMapper.checkMail(email, password);
            if (userEntity != null && userEntity.isActivation()) {
                return true;
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return false;
    }

    public static boolean isCheckSession(String email,String password){
        if(email==null || password == null) return false;
        else{
            try {
                SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
                UserEntity userEntity = sqlSession.getMapper(UserMapper.class).checkMail(email,password);
                sqlSession.close();
                return userEntity != null;
            } catch (IOException e) {
                try {
                    throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
                } catch (VaymartDataBaseException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean mailCheckForEmployment(String email) throws VaymartDataBaseException {
        try {
            SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession();
            UserEntity userEntity = sqlSession.getMapper(UserMapper.class).mailCheckForEmployment(email);
            return userEntity == null;
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void addUsersInDataBase(User user) throws VaymartDataBaseException {
//        UserEntity userEntity = new UserEntity();
//        modelMapper.map(user,userEntity);
//        System.out.println(userEntity);
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            ActivatorMail.sendMail(user.getEmail(), "\nhttp://localhost:8080/activate?email=" + user.getEmail() + "&md=" + user.getPassword());
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.addUsersInDataBase(user);
            sqlSession.commit();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void updateActivation(String email, String password) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSqlSessionFactory.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.updateActivation(new User(email, password, true));
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }
}
