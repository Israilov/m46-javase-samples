package com.team.dto;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    private String totalAmount;
    private String orderCode;
    private Customer customer;
    private Card card;
    private Plan plan;
}
