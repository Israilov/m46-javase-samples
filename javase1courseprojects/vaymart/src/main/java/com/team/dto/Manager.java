package com.team.dto;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Manager {
    private String email;
    private String password;
}
