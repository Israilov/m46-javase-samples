package com.team.dto;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private String brand;
    private String model;
    private String color;
    private int memorySize;
    private String link;
    private String vendorCode;
    private int price;
    private int quantity;
}
