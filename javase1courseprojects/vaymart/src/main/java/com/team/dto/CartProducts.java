package com.team.dto;

import lombok.*;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartProducts {
    private ArrayList<Cart> carts;
    private int totalPrice;
}
