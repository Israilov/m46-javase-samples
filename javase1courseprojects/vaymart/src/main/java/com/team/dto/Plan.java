package com.team.dto;

import lombok.*;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Plan {
    private String type;
    private String term;
}
