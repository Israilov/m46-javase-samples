package com.team.dto;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cart {
    private String vendorCode;
    private String quantity;
}
