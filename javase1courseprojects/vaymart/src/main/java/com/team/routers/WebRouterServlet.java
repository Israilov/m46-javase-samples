package com.team.routers;

import com.team.dto.*;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.exсeptions.VaymartStripeException;
import com.team.services.*;
import com.team.utils.PressMark;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;

@WebServlet("/webui/*")
public class WebRouterServlet extends HttpServlet {

    private void route(HttpServletRequest req, HttpServletResponse resp){
    String uri = req.getRequestURI();
    HttpSession session = req.getSession(true);
    UserService userService = new UserService();
    try {
        switch (uri) {
            case "/webui/login": {
                String email = req.getParameter("email");
                String password = req.getParameter("password");
                if(email == null || password == null){
                    req.getRequestDispatcher("/web/login.jsp").forward(req,resp);
                }
                else{
                    boolean flag = userService.checkMail(email,PressMark.getMD5(password));
                    if(flag){
                        session.setAttribute("email",email);
                        session.setAttribute("password", PressMark.getMD5(password));
                        resp.sendRedirect("/webui/index");
                    }else{
                        req.getRequestDispatcher("/web/login.jsp").forward(req,resp);
                    }
                }
                break;
            }
            case "/webui/main": {
                req.getRequestDispatcher("/web/main.jsp").forward(req,resp);
                break;
            }
            case "/webui/signup": {
                String email = req.getParameter("email");
                String password = req.getParameter("password");
                if(email!=null && password!=null) {
                    boolean flag = userService.mailCheckForEmployment(email);
                    if (flag) {
                        resp.sendRedirect("/webui/activate");
                        userService.addUsersInDataBase(new User(email, PressMark.getMD5(password), false));
                        return;
                    } else {
                        resp.sendRedirect("/webui/main");
                        return;
                    }
                }
                req.getRequestDispatcher("/web/signup.jsp").forward(req,resp);
                break;
            }
            case "/webui/index": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());
                req.getRequestDispatcher("/web/index.jsp").forward(req,resp);
                break;
            }
            case "/webui/shop": {
                String number = req.getParameter("number");
                if(number==null){
                    req.setAttribute("number","1");
                }else req.setAttribute("number",number);

                req.setAttribute("ProductsBySize", ProductService.getProductsBySize());
                req.getRequestDispatcher("/web/shop.jsp").forward(req,resp);
                break;
            }
            case "/webui/checkout": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());

                String name = req.getParameter("name");
                String surname = req.getParameter("surname");
                String city = req.getParameter("city");
                String street = req.getParameter("street");
                String houseNumber = req.getParameter("house");
                String flatNumber = req.getParameter("flat");
                String email = req.getParameter("email");
                String telephone = req.getParameter("telephone");
                String iin = req.getParameter("iin");
                String payment = req.getParameter("plan");

                if(name != null){
                    Customer customer = new Customer(name,surname,iin,email,telephone,city,street,houseNumber,flatNumber,"null");
                    int customerId = CustomerService.addCustomerInDataBase(customer);
                    req.setAttribute("customerId",customerId);
                }
                System.out.println(payment);
                if(payment!=null && payment.equals("full")){
                    req.getRequestDispatcher("/web/full-payment.jsp").forward(req,resp);
                }else if(payment!=null && payment.equals("installment")){
                    req.getRequestDispatcher("/web/installment-payment.jsp").forward(req,resp);
                }else{
                    req.getRequestDispatcher("/web/checkout.jsp").forward(req, resp);
                }
                break;
            }
            case "/webui/account": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());
                if(UserService.isCheckSession((String)session.getAttribute("email"),(String)session.getAttribute("password"))){
                    req.getRequestDispatcher("/web/account.jsp").forward(req,resp);
                }else{
                    req.getRequestDispatcher("/web/index.jsp").forward(req,resp);
                }
                break;
            }
            case "/webui/activate": {
                String email = req.getParameter("email");
                String password = req.getParameter("password");
                userService.updateActivation(email,PressMark.getMD5(password));
                req.getRequestDispatcher("/web/activate.jsp").forward(req,resp);
                break;
            }
            case "/webui/cart": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());

                String link =  CartService.routerCookie(req,resp);
                if(link == null) {
                    req.getRequestDispatcher("/web/cart.jsp").forward(req, resp);
                }else{
                    resp.sendRedirect(link);
                }
                break;
            }
            case "/webui/payment": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());

                String customerId = req.getParameter("customer");
                String token = req.getParameter("stripeToken");
                String month = req.getParameter("month");

                if(customerId != null && token != null) {
                    Customer customer = CustomerService.getCustomerById(Integer.parseInt(customerId));
                    customer.setStripeId(StripeApiService.getCustomerId(customer, token));
                //  Сделать что бы дата прилетала из формы!
                    Date date = new Date();
                    OrderService.addOrderInDataBase(customerId,date,month,token);
                    PaymentService.addOrder(month,customer,token);
                }

                if(token == null){
                    if(month == null) {
                        req.getRequestDispatcher("/web/full-payment.jsp").forward(req, resp);
                    }else{
                        req.getRequestDispatcher("/web/installment-payment.jsp").forward(req, resp);
                    }
                }else{
                    req.getRequestDispatcher("/web/checkout.jsp").forward(req, resp);
                }
                break;
            }
            case "/webui/single-product": {
                req.setAttribute("AllProducts", ProductService.getAllProducts());
                req.getRequestDispatcher("/web/single-product.jsp").forward(req,resp);
                break;
            }
            case "/webui/logout": {
                session.removeAttribute("email");
                session.removeAttribute("password");
                resp.sendRedirect("/webui/main");
                break;
            }
        }

    } catch (VaymartDataBaseException e) {
        try {
            switch (e.getVaymartDataBaseError()) {
                case INTERNAL_ERROR:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                    break;
                case STORAGE_ERROR:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                    break;
                default:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    resp.getWriter().write("{\"msg\":\"Unknown error\"}");
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    } catch (VaymartStripeException e) {
        try {
            switch (e.getStripeError()) {
                case EMAIL_ERROR:
                    resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                    resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                    break;
                case CARD_NUMBER_ERROR:
                    resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                    resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                    break;
                case CVC_ERROR:
                    resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                    resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                    break;
                default:
                    resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                    resp.getWriter().write("{\"msg\":\"Unimplemented error type\"}");
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    } catch (IOException | ServletException e) {
        try {
            resp.getWriter().write(e.getMessage());
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }
}
