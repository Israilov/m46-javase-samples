package com.team.routers;

import com.team.dto.Manager;
import com.team.dto.Product;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.ManagerService;
import com.team.services.ProductService;
import com.team.utils.PressMark;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/manager/*")
public class ManagerRouterServlet extends HttpServlet {

    private void route(HttpServletRequest req, HttpServletResponse resp){

        String uri = req.getRequestURI();
        try {
            switch (uri) {
                case "/manager/main": {
                    req.getRequestDispatcher("/web/main-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/login": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    if(email!=null && password!=null){
                        boolean flag = ManagerService.checkManager(new Manager(email, PressMark.getMD5(password)));
                        if(flag){
                            req.getRequestDispatcher("/web/add-manager.jsp").forward(req, resp);
                            break;
                        }
                    }
                    req.getRequestDispatcher("/web/login-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/signup": {
                    //Нужно в эту систему внедрить superuser который будет делать регис.
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    req.getRequestDispatcher("/web/signup-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/add": {
                    String brand = req.getParameter("brand");
                    String model = req.getParameter("model");
                    String color = req.getParameter("color");
                    String memoryStr = req.getParameter("memory");
                    String priceStr = req.getParameter("price");
                    String photo = req.getParameter("photo");
                    if(brand != null){
                        int memory = Integer.parseInt(memoryStr);
                        int price = Integer.parseInt(priceStr);
                        String vendor = PressMark.getMD5(brand.toUpperCase() + model.toUpperCase() + color.toUpperCase() + memory);
                        Product product = new Product(brand,model,color,memory,"/web/img/"+photo,vendor,price,1);
                        ProductService.addProduct(product);
                    }
                    req.getRequestDispatcher("/web/add-manager.jsp").forward(req, resp);
                    break;
                }
            }
        } catch (ServletException | IOException e) {
            try {
                resp.getWriter().write(e.getMessage());
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        } catch (VaymartDataBaseException e) {
            try {
                switch (e.getVaymartDataBaseError()) {
                    case INTERNAL_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    case STORAGE_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    default:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"Unimplemented error type\"}");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }
}
