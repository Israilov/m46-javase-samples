package com.team.entity;

import lombok.*;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor

public class UserEntity {
    private int id;
    private int customer_id;
    private String email;
    private String password;
    private boolean activation;
}
