package com.team.entity;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {
    private int id;
    private int productId;
    private String brand;
    private String model;
    private String color;
    private int memorySize;
    private String link;
    private String vendor;
    private int price;
    private int quantity;
}
