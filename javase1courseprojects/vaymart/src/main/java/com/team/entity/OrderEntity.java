package com.team.entity;

import com.team.enums.DeliveryStatus;
import com.team.enums.GlobalStatus;
import com.team.enums.PaymentStatus;
import com.team.enums.WareHouseStatus;
import lombok.*;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderEntity {
    private int orderCode;
    private int customerId;
    private GlobalStatus status;
    private Date date;
    private long totalAmount;
    private int initialAmount;
    private int month;
    private PaymentStatus paymentStatus;
    private WareHouseStatus wareHouseStatus;
    private DeliveryStatus deliveryStatus;
}
