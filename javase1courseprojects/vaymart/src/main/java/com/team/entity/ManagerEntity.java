package com.team.entity;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ManagerEntity {
    private String id;
    private String email;
    private String password;
}
