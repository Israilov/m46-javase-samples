<!DOCTYPE html>
<%@ page import="com.team.services.UserService" %>
<%@ page import="com.team.dto.Product" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.team.dto.Cart" %>

<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>VAYMART</title>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/style.css">
    <link rel="stylesheet" href="/web/css/responsive.css">

</head>
<body>
<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                        <%if(UserService.isCheckSession((String)session.getAttribute("email"),(String)session.getAttribute("password"))){ %>
                        <li><a href="/webui/account"><i class="fa fa-user"></i> My Account</a></li>
                        <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                        <%}else{%>
                        <li><a href="/webui/login"><i class="fa fa-user"></i> Login</a></li>
                        <li><a href="/webui/signup"><i class="fa fa-user"></i> Sign Up</a></li>
                        <%}%>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="/webui/index">VAYMART</a></h1>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="/webui/cart"><span class="amount"></span> <i class="fa fa-shopping-cart"></i> <span id="product-count-id" class="product-count"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/webui/index">Home</a></li>
                        <li><a href="/webui/shop">Shop page</a></li>
                        <li><a href="/webui/cart">Cart</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="slider-area">
		<div class="block-slider block-slider4">
			<ul id="bxslider-home4">
				<li><img src="/web/img/2.jpg" alt="Slide"></li>
                <li><img src="/web/img/3.jpg" alt="Slide"></li>
                <li><img src="/web/img/4.jpg" alt="Slide"></li>
                <li><img src="/web/img/1.png" alt="Slide"></li>
            </ul>
		</div>
    </div>

    <div class="promo-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo1">
                        <i class="fa fa-refresh"></i>
                        <p>30 Days return</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo2">
                        <i class="fa fa-truck"></i>
                        <p>Free shipping</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo3">
                        <i class="fa fa-lock"></i>
                        <p>Secure payments</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo promo4">
                        <i class="fa fa-gift"></i>
                        <p>New products</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="section-title">Latest Products</h2>
                        <div id="prodcaro" class="product-carousel">
                            <%
                                HashMap<String, Product> hashMap = (HashMap<String, Product>) request.getAttribute("AllProducts");
                                for (Product mob : hashMap.values()) {
                            %>
                            <div class="single-product">
                                <div style="padding-top: 20px; height: 270px" class="product-f-image">
                                    <img src="<%=mob.getLink()%>" alt="">
                                    <div class="product-hover">
                                        <a href="/webui/cart?mobile=<%=mob.getVendorCode()%>" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        <a href="/webui/single-product" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                    </div>
                                </div>
                                <h2><a style="color:darkblue" href=""><%=mob.getBrand().toUpperCase()%> <%=mob.getModel().toUpperCase()%><br>COLOR: <%=mob.getColor().toUpperCase()%><br>PRICE: <ins><%=mob.getPrice()%>.00 $</ins></a></h2>
                            </div>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">
                        <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script src="/web/js/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="/web/js/bootstrap.min.js"></script>
<script src="/web/js/jquery.sticky.js"></script>
<script src="/web/js/owl.carousel.min.js"></script>
<script src="/web/js/jquery.easing.1.3.min.js"></script>
<script src="/web/js/main.js"></script>
<script src="/web/js/bxslider.min.js"></script>
<script src="/web/js/script.slider.js"></script>
<%--<script src="/web/js/carousel.js"></script>--%>
<script src="/web/js/price.js"></script>

  </body>
</html>