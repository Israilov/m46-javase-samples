var container = $('#container');
var arrProduct;
var count = 1;
var totalPriceClass = document.getElementsByClassName("amount");
var countProduct = $('#product-count-id');

$.ajax({
    url: "/api/products/get",
    type: "GET",
    dataType: "json",
    success: function (data) {
        arrProduct = data;
        fetchDataAndDisplay();
    }
});

function getCookie() {
    var cookie = $.cookie("cookieMart");
    if(cookie !== undefined){
        return JSON.parse(cookie);
    }else{
        return 0;
    }
}

function fetchDataAndDisplay() {
    var jsonCookie = getCookie();
    countProduct.append(jsonCookie !== 0 ? jsonCookie.length : 0);
    var totalPrice = 0;
    if(jsonCookie !== 0) {
        $.each(jsonCookie, function (index, cookie) {
            $.each(arrProduct, function (index, item) {
                if (cookie.vendorCode === index) {
                    container.append(
                        '<tr class="cart_item">' +
                        '<td class="product-thumbnail">' +
                        '<a href="/webui/single-product"><img width="150" height="150" class="shop_thumbnail" src="' + item.link + '"></a><h2 style="float:right;margin-right: 35%;padding-top: 20px">X  1</h2>' +
                        '</td>' +
                        '<td class="product-name">' + item.brand.toUpperCase() + ' ' + item.model.toUpperCase() + '</td>' +
                        '<td class="product-price">' +
                        '<span class="inner-amount">$' + item.price + '.00</span>' +
                        '</td>' +
                        '</tr>'
                    );
                    totalPrice += item.price;
                }
            });
        });
    }
    for(var i = 0; i < totalPriceClass.length;i++){
        totalPriceClass[i].append(totalPrice !== 0 ? '$'+totalPrice+'.00' : '$'+0+'.00');
    }
    $("#my-script").attr("data-amount",totalPrice+'00');
}