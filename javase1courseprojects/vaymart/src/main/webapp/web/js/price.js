var countProduct = $('#product-count-id');
var totalPriceClass = document.getElementsByClassName("amount");
var dataProducts;

$.ajax({
    url: "/api/products/get",
    type: "GET",
    dataType: "json",
    success: function (data) {
        dataProducts = data;
        fetchDataAndDisplayPriceAndCount();
    }
});

function getCookie() {
    var cookie = $.cookie("cookieMart");
    if(cookie !== undefined){
        return JSON.parse(cookie);
    }else{
        return 0;
    }
}

function fetchDataAndDisplayPriceAndCount() {
    var jsonCookie = getCookie();
    countProduct.append(jsonCookie !== 0 ? jsonCookie.length : 0);
    var totalPrice = 0;
    if(jsonCookie !== 0) {
        $.each(jsonCookie, function (index, cookie) {
            $.each(dataProducts, function (index, item) {
                if (cookie.vendorCode === index) {
                    totalPrice += item.price;
                }
            });
        });
    }
    for (var i = 0; i < totalPriceClass.length; i++) {
        totalPriceClass[i].append(totalPrice !== 0 ? '$' + totalPrice + '.00' : '$' + 0 + '.00');
    }
    $("#my-script").attr("data-amount",totalPrice+'00');
}
