var container = $('#my-container');
var arrProduct;
var count = 1;
$(".btn").click(function(){
    count = $(this).val();
    fetchDataAndDisplay();
});

$.ajax({
    url: "/api/products/size",
    type: "GET",
    dataType: "json",
    success: function(data) {
        arrProduct = data;
//                buttonNumberMethod();
        fetchDataAndDisplay();
    }
});
// function buttonNumberMethod() {
//     var size = Object.keys(arrProduct).length + 1;
//     for(var i = 1; i<size; i++){
//         buttonNumber.append(
//             '<li>'+
//                 '<button name="button" value="'+i+'">'
//                     +i+
//                 '</button>'+
//             '</li>');
//     }
// }

function fetchDataAndDisplay() {
    $.each(arrProduct, function (index, item) {
        if (count == index) {
            container.empty();
            $.each(item, function (indexInner, value) {
                container.append(
                    '<div class="col-md-3 col-sm-6">'+
                    '<div style="margin-bottom: 30px;" class="single-product">'+
                    '<div class="product-f-image">'+
                    '<img src="' + value.link + '" alt="">' +
                    '<div class="product-hover">' +
                    '<a href="/webui/cart?mobile=' + value.vendorCode + '" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i> Add to cart</a>' +
                    '<a href="/webui/single-product" class="view-details-link"><i class="fa fa-link"></i> See details</a>' +
                    '</div>' +
                    '</div>' +
                    '<h2><a style="color:darkblue" href="">' + value.brand.toUpperCase() + ' ' + value.model.toUpperCase() + '<br>COLOR: ' + value.color.toUpperCase() + '<br>PRICE: <ins>' + value.price + '.00 $</ins></a></h2>'+
                    '</div>'+
                    '</div>');
            });
        }
    });
}