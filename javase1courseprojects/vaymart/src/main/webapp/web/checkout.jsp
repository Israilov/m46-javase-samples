<!DOCTYPE html>
<%@ page import="com.team.services.UserService" %>

<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Checkout Page</title>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/style.css">

</head>
<body>
<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                        <%if(UserService.isCheckSession((String)session.getAttribute("email"),(String)session.getAttribute("password"))){ %>
                        <li><a href="/webui/account"><i class="fa fa-user"></i> My Account</a></li>
                        <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                        <%}else{%>
                        <li><a href="/webui/login"><i class="fa fa-user"></i> Login</a></li>
                        <li><a href="/webui/signup"><i class="fa fa-user"></i> Sign Up</a></li>
                        <%}%>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="/webui/index">VAYMART</a></h1>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="/webui/cart"><span class="amount"></span> <i class="fa fa-shopping-cart"></i> <span id="product-count-id" class="product-count"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/webui/index">Home</a></li>
                        <li><a href="/webui/shop">Shop page</a></li>
                        <li><a href="/webui/cart">Cart</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Checkout</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <div class="woocommerce-info">Returning customer? <a href="main.jsp">Click here to login</a></div>
                            <form action="/webui/checkout" method="POST">
                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <p class="woocommerce-billing-fields">
                                            <h3>Billing Details</h3>
                                            <p class="form-row form-row-first validate-required">
                                                <label for="billing_first_name">First Name <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_first_name" name="name" class="input-text">
                                            </p>

                                            <p class="form-row form-row-last validate-required">
                                                <label for="billing_last_name">Last Name <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_last_name" name="surname" class="input-text">
                                            </p>

                                            <p class="form-row form-row-wide address-field validate-required">
                                                <label for="billing_city">Town / City <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" placeholder="Town / City" id="billing_city" name="city" class="input-text">
                                            </p>

                                            <p class="form-row form-row-wide address-field validate-required">
                                                <label for="billing_address_1">Address <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" placeholder="Street" id="billing_address_1" name="street" class="input-text ">
                                            </p>

                                            <p class="form-row form-row-wide address-field">
                                                <input type="text" placeholder="House number *" id="billing_address_2" name="houseNumber" class="input-text ">
                                            </p>

                                            <p class="form-row form-row-wide address-field">
                                                <input type="text" placeholder="Flat number *" id="billing_address_3" name="flatNumber" class="input-text ">
                                            </p>

                                            <p class="form-row form-row-first validate-required validate-email">
                                                <label for="billing_email">Email Address <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_email" name="email" class="input-text ">
                                            </p>

                                            <p class="form-row form-row-last validate-required validate-phone">
                                                <label for="billing_phone">Phone <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_phone" name="telephone" class="input-text ">
                                            </p>

                                            <p class="form-row form-row-last validate-required validate-phone">
                                                <label for="billing_iin">Iin <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_iin" name="iin" class="input-text ">
                                            </p>
                                        </div>
                                    </div>
                                <div id="order_review">
                                    <hr>
                                    <div id="payment">
                                        <ul class="payment_methods methods">
                                            <li class="payment_method_bacs">
                                                <input type="radio" checked="checked" value="full" name="plan" class="input-radio" id="payment_method_bacs">
                                                <label for="payment_method_bacs">Full payment </label>
                                            </li>
                                            <li class="payment_method_cheque">
                                                <input type="radio" data-order_button_text="" value="installment" name="plan" class="input-radio" id="payment_method_cheque">
                                                <label for="payment_method_cheque">Installment payment </label>
                                            </li>
                                            <li class="payment_method_paypal">
                                                <label>PayPal & MasterCard
                                                    <img alt="PayPal Acceptance Mark" src="https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png">
                                                </label>
                                            </li>
                                        </ul>

                                        <table class="shop_table">
                                            <thead>
                                                <tr>
                                                    <th class="product-name">Product</th>
                                                    <th class="product-total">Model</th>
                                                    <th class="product-total">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id="container"></tbody>
                                            <tfoot>
                                                <tr class="cart-subtotal">
                                                    <th>Cart Total</th>
                                                    <th> </th>
                                                    <td><span class="amount"></span></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="form-row place-order">
                                            <input type="submit" data-value="Place order" value="Place order" id="place_order" name="woocommerce_checkout_place_order" class="button alt">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-area">
        <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                        </div>
                </div>

                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script src="/web/js/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="/web/js/view.js"></script>
<script src="/web/js/bootstrap.min.js"></script>
</body>
</html>
