import com.m46academy.telbook.TelBook;
import com.m46academy.telbook.exception.*;
import com.m46academy.telbook.model.contact.*;
import com.m46academy.telbook.model.personalinfo.*;
import com.m46academy.telbook.model.Person;

import java.io.IOException;
import java.util.Scanner;

public class TelephoneBookApplication {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException, InputValidationException {

        System.out.println("Hello this is my phonebook");

        TelBook telBook = new TelBook();

        // STEP - 2 - START
        telBook.loadData();
        // STEP - 2 - END

        while (true) {

            System.out.println("Menu: ");
            System.out.println("(add) - Add person;");
            System.out.println("(del) - Delete person;");
            System.out.println("(show) - Show all persons;");
            System.out.println("(rep) - Replace person");
            System.out.println("(search) - Search persons");
            System.out.println("(sort) - Sort persons");
            System.out.println("(exit) - Exit");

            System.out.println("Enter option: ");
            String optionSelected = scanner.nextLine();

            if ("add".equals(optionSelected)) {

                Person person = fillPersonInfo();

                try {
                    telBook.add(person);
                } catch (IOException e) {
                    System.err.println("Cannot save to file");
                    continue;
                } catch (EmptyPersonException e) {
                    System.err.println("Cannot save empty person");
                    continue;
                } catch (TelBookFullException e) {
                    System.out.println("Telephone book is full, please remove some entries first");
                    continue;
                }

                System.out.println("Person successfully added");

            } else if ("del".equals(optionSelected)) {

                //STEP 12 - START

                while (true) {

                    showPersons(telBook.getPersons());

                    System.out.println("Select index to delete:");

                    int index = Integer.parseInt(scanner.nextLine());

                    try {
                        telBook.delete(index);
                        System.out.println("Successfully deleted person entry with id#"+index);
                    } catch (PersonNotFoundException e) {

                        System.out.println("No such person found, try again (Y/N)?");

                        String option = scanner.nextLine();

                        if (option.equals("Y")) {
                            continue;
                        } else {
                            break;
                        }

                    } catch (CannotDeletePersonException e) {

                        System.out.println("Cannot delete person from db, try again (Y/N)?");

                        String option = scanner.nextLine();

                        if (option.equals("Y")) {
                            continue;
                        } else {
                            break;
                        }

                    }

                    break;

                }

                //STEP 12 - END

            } else if ("rep".equals(optionSelected)) {

                //STEP 12 - START
                while (true) {

                    showPersons(telBook.getPersons());

                    System.out.println("Which index to replace?");

                    int index = scanner.nextInt();

                    Person person = telBook.getPerson(index);

                    try {

                        while (true) {

                            Person replacePerson = fillPersonInfo();

                            try {
                                telBook.replacePerson(index, replacePerson);
                            } catch (CannotSavePersonException e) {

                                System.out.println("Cannot save person in Database! Try again (Y/N)?");

                                String option = scanner.nextLine();

                                if (option.equalsIgnoreCase("Y")) {
                                    continue;
                                } else {
                                    break;
                                }
                            }

                            System.out.println("Person was replaced, old person: ");
                            System.out.println(person.toString());

                            break;
                        }

                        break;

                    } catch (PersonNotFoundException e) {

                        System.out.println("No such person found, please check your input");
                        continue;

                    } catch (ArrayIndexOutOfBoundsException e) {

                        System.err.println("There is no such index, enter again");
                        continue;

                    } catch (EmptyPersonException e) {

                        System.err.println("Person info is empty");
                        break;
                    }
                }

                //STEP 12 - END

            } else if ("show".equalsIgnoreCase(optionSelected)) {
                //STEP 12 - START
                showPersons(telBook.getPersons());
                //STEP 12 - END
            } else if ("sort".equalsIgnoreCase(optionSelected)) {
                //STEP 14 - START

                System.out.println("Sort by?:");
                System.out.println("(birthday) Birthday");
                System.out.println("(name) By name");
                System.out.println("(profession) By profession");
                System.out.println("(email) By email");
                System.out.println("(fb) By facebook account");
                System.out.println("(tel) By mobile Phone");
                System.out.println("(vk) By vk account");
                System.out.println("(twitter) By twitter account");

                String sortBy = scanner.nextLine();

                Class byClass = Name.class;

                switch (sortBy) {
                    case "birthday": byClass = Birthday.class; break;
                    case "name": byClass = Name.class; break;
                    case "profession": byClass = Profession.class; break;
                    case "email": byClass = Email.class; break;
                    case "fb": byClass = FacebookAccount.class; break;
                    case "tel": byClass = MobilePhone.class; break;
                    case "vk": byClass = VkAccount.class; break;
                    case "twitter": byClass = TwitterAccount.class; break;
                    default: //do nothing
                }

                Person[] sortedPersons = telBook.sort(byClass);

                showPersons(sortedPersons);

                //STEP 14 - END
            } else if ("search".equalsIgnoreCase(optionSelected)) {
                //STEP 13 - START
                System.out.println("Enter searchable keyword: ");
                String keyword = scanner.nextLine();

                Person[] foundPersons = telBook.search(keyword);

                if (foundPersons.length>0) {

                    System.out.println("\nFound persons:\n");

                    for (int i=0; i<foundPersons.length; i++) {
                        System.out.println(foundPersons[i].toString());
                    }

                } else {
                    System.out.println("Nothing found.");
                }

                //STEP 13 - END
            } else if ("exit".equalsIgnoreCase(optionSelected)) {
                //STEP 12 - START
                System.out.println("Exiting application...");
                break;
                //STEP 12 - END
            } else {
                //STEP 12 - START
                System.out.println("No such option, please try again.");
                //STEP 12 - END
            }
        }

    }

    //STEP 12 - START
    private static Person fillPersonInfo() {

        Person person = new Person();

        while (true) {

            System.out.println("Select info to fill: ");
            System.out.println("(tel) Fill mobile phone");
            System.out.println("(name) Fill name");
            System.out.println("(birthday) Fill birthday");
            System.out.println("(twitter) Fill twitter account");
            System.out.println("(email) Fill twitter account");
            System.out.println("(facebook) Fill facebook account");
            System.out.println("(mobile) Fill mobile number");
            System.out.println("(vk) Fill VK account");
            System.out.println("(profession) Fill profession ");
            System.out.println("(done) Exit this menu");

            String infoType = scanner.nextLine();

            if (infoType.equals("tel")) {

                MobilePhone mobilePhone = new MobilePhone();

                System.out.println("-Mobile phone-");
                System.out.println("country code:");
                mobilePhone.setCountryCode(scanner.nextLine());
                System.out.println("msisdn:");
                mobilePhone.setMsisdn(scanner.nextLine());

                person.addContact(mobilePhone);

            } else if (infoType.equals("name")) {

                Name name = new Name();

                System.out.println("-Name-");
                System.out.println("firstname:");
                name.setFirstname(scanner.nextLine());
                System.out.println("surname:");
                name.setSurname(scanner.nextLine());
                System.out.println("paternalname:");
                name.setPaternal(scanner.nextLine());
                System.out.println("maternalname:");
                name.setMaternal(scanner.nextLine());
                System.out.println("prefix:");
                name.setPrefix(scanner.nextLine());

                person.addPersonalInfo(name);

            } else if (infoType.equals("birthday")) {

                Birthday birthday = new Birthday();

                System.out.println("-Birthday-");
                //STEP 13 - START
                setPersonalInfoFieldValue("day", scanner, birthday);
                //STEP 13 - END
                System.out.println("month:");
                birthday.setMonth(scanner.nextInt());
                System.out.println("year:");
                birthday.setYear(scanner.nextInt());

                person.addPersonalInfo(birthday);

            } else if (infoType.equals("twitter")) {

                TwitterAccount twitter = new TwitterAccount();

                System.out.println("-Twitter-");
                //STEP 13 - START
                setContactFieldValue("username", scanner, twitter);
                //STEP 13 - END
                person.addContact(twitter);

            } else if (infoType.equals("profession")) {

                Profession profession = new Profession();

                System.out.println("-Profession-");
                System.out.println("professionGroup:");
                profession.setProfessionGroup(scanner.nextLine());
                System.out.println("professionName:");
                profession.setProfessionName(scanner.nextLine());

                person.addPersonalInfo(profession);
            } else if (infoType.equals("email")) {

                Email email = new Email();

                System.out.println("-Email-");
                System.out.println("username");
                email.setUsername(scanner.nextLine());
                System.out.println("mailProvider");
                email.setMailProvider(scanner.nextLine());
                person.addContact(email);

            } else if (infoType.equals("facebook")) {

                FacebookAccount facebookAccount = new FacebookAccount();

                System.out.println("-Facebook-");
                System.out.println("username");
                facebookAccount.setUsername(scanner.nextLine());

                person.addContact(facebookAccount);

            } else if (infoType.equals("sex")) {

                Sex sex = new Sex();

                System.out.println("-Sex-");
                System.out.println("type: ");
                sex.setType(scanner.nextLine());

                person.addPersonalInfo(sex);
            } else if (infoType.equals("vk")) {

                VkAccount vkAccount = new VkAccount();

                System.out.println("-VK-");
                System.out.println("username");
                vkAccount.setUsername(scanner.nextLine());

                person.addContact(vkAccount);

            } else if (infoType.equals("done")) {
                return person;
            }

        }

    }

    //STEP 13 - START
    private static void setPersonalInfoFieldValue(String fieldName, Scanner scanner, PersonalInfo personalInfo) {

        while (true) {
            try {
                System.out.println(fieldName + ":");
                personalInfo.setFieldValue(fieldName, scanner.nextLine());
                break;
            } catch (InputValidationException e) {
                System.err.print(e.getMessage());
                System.out.println(" Please try again...");
            }
        }

    }

    private static void setContactFieldValue(String fieldName, Scanner scanner, Contact contact) {

        while (true) { //TODO: что если клиет хочет пропустить заполнение этой информации?
            try {
                System.out.println(fieldName + ":");
                contact.setFieldValue(fieldName, scanner.nextLine());
                break;
            } catch (InputValidationException e) {
                System.err.print(e.getMessage());
                System.out.println(" Please try again...");
            }
        }

    }
    //STEP 13 - END
    //STEP 12 - END

    //STEP 12 - START
    //STEP 14 - START
    //removed method showTelBook
    //STEP 14 - END
    //STEP 12 - END

    //STEP 14 - START
    private static void showPersons(Person[] persons) {

        for (int i = 0; i < persons.length; i++) {
            if (persons[i] != null) {
                System.out.println("#" + persons[i].getId() + ":"); //STEP 15
                System.out.println(persons[i].toString());
            }
        }

    }
    //STEP 14 - END
}

