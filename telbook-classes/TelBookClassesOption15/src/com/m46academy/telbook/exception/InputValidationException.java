package com.m46academy.telbook.exception;

public class InputValidationException extends Exception {
    public InputValidationException(String field, String value) {
        super("Incorrect value for "+field+", trying to set value="+value);
    }
}
