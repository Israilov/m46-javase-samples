package com.m46academy.telbook.utils;

import com.m46academy.telbook.model.Person;
import com.m46academy.telbook.model.Searchable;

public class Searcher {

    public static Searchable[] search(Searchable[] objects, String keyword) {

        Searchable[] tmpFoundPersons = new Searchable[objects.length];
        int lastFoundIndex = 0;

        for (int i=0; i<objects.length; i++) {
            if (objects[i]!=null) {
                Searchable searchable = objects[i];
                if (searchable.search(keyword)) {
                    tmpFoundPersons[lastFoundIndex] = objects[i];
                    lastFoundIndex++;
                }
            }
        }

        Searchable[] foundPersons = new Searchable[lastFoundIndex];

        for (int i=0; i<foundPersons.length; i++) {
            foundPersons[i] = tmpFoundPersons[i];
        }

        return foundPersons;
    }
}
