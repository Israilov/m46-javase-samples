package com.m46academy.telbook;

import com.m46academy.telbook.database.Database;
import com.m46academy.telbook.database.implementation.SimpleFileDatabase;
import com.m46academy.telbook.exception.CannotDeletePersonException;
import com.m46academy.telbook.exception.CannotSavePersonException;
import com.m46academy.telbook.exception.EmptyPersonException;
import com.m46academy.telbook.exception.InputValidationException;
import com.m46academy.telbook.exception.PersonNotFoundException;
import com.m46academy.telbook.exception.TelBookFullException;
import com.m46academy.telbook.model.Person;
import com.m46academy.telbook.utils.Searcher;
import java.io.IOException;

public class TelBook {

  private String owner;

  private Person[] persons = new Person[10];
  private int largestIndex = 0;
  private int uniqueIdCounter; //STEP 15

  private Database database = new SimpleFileDatabase();

  public Person[] getPersons() {

    Person[] result = new Person[largestIndex];

    for (int i = 0; i < largestIndex; i++) {
      result[i] = persons[i];
    }

    return result;
  }

  public int length() {
    return largestIndex;
  }

  public void add(Person person) throws IOException, EmptyPersonException, TelBookFullException {

    if (largestIndex < 10 && largestIndex >= 0) {

      //STEP 15 - START
      uniqueIdCounter++;
      person.setId(uniqueIdCounter);
      //STEP 15 - END

      persons[largestIndex] = person;
      database.createPerson(person);

      largestIndex++;

    } else if (person == null
        || (person.getPersonalInfos() == null
        && person.getContacts() == null)
    ) {
      throw new EmptyPersonException();
    } else if (largestIndex >= 10) {
      throw new TelBookFullException();
    }

  }

  //STEP 12 - START
  public void replacePerson(int index, Person person)
      throws ArrayIndexOutOfBoundsException, EmptyPersonException, PersonNotFoundException, CannotSavePersonException {

    if (index <= 0 || index >= 10) {
      ArrayIndexOutOfBoundsException exception = new ArrayIndexOutOfBoundsException();
      throw exception;
    } else if (person == null || (person.getContacts() == null
        && person.getPersonalInfos() == null)) {
      EmptyPersonException exception = new EmptyPersonException();
      throw exception;
    }

    Person oldPerson = persons[index];
    persons[index] = person;

    try {
      database.updatePerson(person);
    } catch (PersonNotFoundException e) {
      throw e;
    } catch (IOException e) {
      persons[index] = oldPerson;
      throw new CannotSavePersonException();
    }

  }
  //STEP 12 - END

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public void loadData() throws IOException, InputValidationException {

    Person[] personsFromReadAll = database.readAll();

    largestIndex = personsFromReadAll.length;

    this.persons = new Person[10];

    for (int i = 0; i < largestIndex; i++) {
      this.persons[i] = personsFromReadAll[i];
      //STEP 15 - START
      if (this.persons[i].getId() > uniqueIdCounter) {
        uniqueIdCounter = this.persons[i].getId(); //setting uniqueIdCounter to the greatest value
      }
      //STEP 15 - END
    }

  }

  public Person getPerson(int index) throws ArrayIndexOutOfBoundsException {

    if (index < 0 || index > largestIndex - 1) {
      throw new ArrayIndexOutOfBoundsException();
    }

    return persons[index];
  }

  //STEP 12 - START
  public void delete(int index)
      throws ArrayIndexOutOfBoundsException, PersonNotFoundException, CannotDeletePersonException {

    database.deletePerson(index);

    for (int i = 0; i < persons.length; i++) {
      if (persons[i] != null && persons[i].getId() == index) {
        persons[i] = null;
      }
    }

  }
  //STEP 12 - END

  //STEP 13 - START
  public Person[] search(String keyword) {
    return (Person[]) Searcher.search(persons, keyword);
  }
  //STEP 13 - END

  //STEP 14 - START
  public Person[] sort(Class clazz) { //Object of a class with name Class

    Person[] sortedPersons = new Person[persons.length];

    sortedPersons = Sorter.sort(persons, clazz);

    return sortedPersons;
  }
  //STEP 14 - END


  public static class Sorter {

    public static Person[] sort(Person[] persons, Class clazz) {

      Person[] sorted = new Person[persons.length];
      int sizeOfNonEmptyPersons = 0;

      for (int i = 0; i < persons.length; i++) {
        if (persons[i] != null) {
          sorted[sizeOfNonEmptyPersons] = persons[i];
          sizeOfNonEmptyPersons++;
        }
      }

      for (int i = 0; i < sizeOfNonEmptyPersons - 1; i++) {
        for (int j = i + 1; j < sizeOfNonEmptyPersons; j++) {
          if (sorted[i].compareTo(sorted[j], clazz) < 0) {
            Person tmp = sorted[i];
            sorted[i] = sorted[j];
            sorted[j] = tmp;
          }
        }
      }

      return sorted;
    }
  }

}
