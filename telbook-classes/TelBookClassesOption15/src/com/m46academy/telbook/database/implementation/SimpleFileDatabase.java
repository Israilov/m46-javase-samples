package com.m46academy.telbook.database.implementation;

import com.google.gson.Gson;
import com.m46academy.telbook.database.Database;
import com.m46academy.telbook.exception.CannotDeletePersonException;
import com.m46academy.telbook.exception.InputValidationException;
import com.m46academy.telbook.exception.PersonNotFoundException;
import com.m46academy.telbook.model.Person;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class SimpleFileDatabase implements Database {

  final String dirPath = "data/";

  @Override
  public void createPerson(Person person) throws IOException {

    File f = new File(dirPath + person.getId() + ".person");

    f.delete();
    f.createNewFile();

    //STEP 12 - START

    String fileContents = personToDbFileContent(person);

    writeToDbFile(f, fileContents);

    //STEP 12 - END

  }

  private void writeToDbFile(File f, String fileContents) throws IOException {

    FileOutputStream fout = new FileOutputStream(f);

    for (int i = 0; i < fileContents.length(); i++) {
      fout.write((int) fileContents.charAt(i));
    }

    fout.close();

  }

  private String personToDbFileContent(Person person) {

    Gson gson = new Gson();

    return gson.toJson(person);

//    PersonalInfo[] personalInfos = person.getPersonalInfos();
//    Contact[] contacts = person.getContacts();
//
//    String fileContents = "";
//
//    //STEP 15 - START
//
//    for (int i = 0; i < personalInfos.length; i++) {
//
//      if (personalInfos[i] instanceof Name) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "Name", false)+"\n";
//        fileContents += formatJsonKeyValue("firstname", ((Name) personalInfos[i]).getFirstname(), false) + "\n";
//        fileContents += formatJsonKeyValue("surname", ((Name) personalInfos[i]).getSurname(), false) + "\n";
//        fileContents += formatJsonKeyValue("paternal", ((Name) personalInfos[i]).getPaternal(), false) + "\n";
//        fileContents += formatJsonKeyValue("maternal", ((Name) personalInfos[i]).getMaternal(), false) + "\n";
//        fileContents += formatJsonKeyValue("prefix", ((Name) personalInfos[i]).getPrefix(), true) + "\n";
//        fileContents += "}";
//
//      } else if (personalInfos[i] instanceof Sex) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "Sex", false) + "\n";
//        fileContents += formatJsonKeyValue("type", ((Sex) personalInfos[i]).getType(), true)+"\n";
//        fileContents += "}";
//
//      } else if (personalInfos[i] instanceof Birthday) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "Birthday", false) + "\n";
//        fileContents += formatJsonKeyValue("day", Integer.toString(((Birthday) personalInfos[i]).getDay()), false) + "\n";
//        fileContents += formatJsonKeyValue("month", Integer.toString(((Birthday) personalInfos[i]).getMonth()), false) + "\n";
//        fileContents += formatJsonKeyValue("year", Integer.toString(((Birthday) personalInfos[i]).getYear()), true) + "\n";
//        fileContents += "}";
//
//      } else if (personalInfos[i] instanceof Profession) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "Profession", false)+"\n";
//        fileContents += formatJsonKeyValue("professionGroup", ((Profession) personalInfos[i]).getProfessionGroup(), false) + "\n";
//        fileContents += formatJsonKeyValue("professionName", ((Profession) personalInfos[i]).getProfessionName(), true) + "\n";
//        fileContents += "}";
//
//      }
//
//    }
//
//    for (int j = 0; j < contacts.length; j++) {
//
//      if (contacts[j] instanceof Email) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class","Email", false)+"\n";
//        fileContents += formatJsonKeyValue("username", ((Email) contacts[j]).getUsername(), false) + "\n";
//        fileContents += formatJsonKeyValue("mailProvider", ((Email) contacts[j]).getMailProvider(), true) + "\n";
//        fileContents += "}";
//
//      } else if (contacts[j] instanceof FacebookAccount) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "FacebookAccount", false)+"\n";
//        fileContents += formatJsonKeyValue("username", ((FacebookAccount) contacts[j]).getUsername(), true) + "\n";
//        fileContents += "}";
//
//      } else if (contacts[j] instanceof MobilePhone) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "MobilePhone", false) + "\n";
//        fileContents += formatJsonKeyValue("countryCode", ((MobilePhone) contacts[j]).getCountryCode(), false) + "\n";
//        fileContents += formatJsonKeyValue("msisdn", ((MobilePhone) contacts[j]).getMsisdn(), true) + "\n";
//        fileContents += "}";
//
//      } else if (contacts[j] instanceof TwitterAccount) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "TwitterAccount", false)+"\n";
//        fileContents += formatJsonKeyValue("username", ((TwitterAccount) contacts[j]).getUsername(), true) + "\n";
//        fileContents += "}";
//
//      } else if (contacts[j] instanceof VkAccount) {
//
//        fileContents += "{";
//        fileContents += formatJsonKeyValue("class", "VkAccount", false)+"\n";
//        fileContents += formatJsonKeyValue("username", ((VkAccount) contacts[j]).getUsername(), true) + "\n";
//        fileContents += "}";
//
//      }
//
//    }

    //STEP 15 - END

//    return fileContents;
  }

  private String formatJsonKeyValue(String key, String value, boolean isLast) {
    return "\"" + key + "\":" + "\"" + value + "\"" + (!isLast ? "" : ",");
  }

  //STEP 12 - START
  @Override
  public Person readPerson(int index)
      throws PersonNotFoundException, IOException, InputValidationException {

    String filename = dirPath + index + ".person";

    Person person = dbFileContentToPerson(filename);

    return person;
  }
  //STEP 12 - END

  //STEP 12 - START
  @Override
  public void updatePerson(Person person) throws PersonNotFoundException, IOException {

    File f = new File(dirPath + person.getId() + ".person");

    if (!f.exists()) {
      throw new PersonNotFoundException();
    }

    f.delete();
    f.createNewFile();

    String fileContents = personToDbFileContent(person);

    writeToDbFile(f, fileContents);
  }
  //STEP 12 - END

  //STEP 12 - START
  @Override
  public void deletePerson(int index) throws PersonNotFoundException, CannotDeletePersonException {

    File f = new File(dirPath + index + ".person");

    if (!f.exists()) {
      throw new PersonNotFoundException();
    }

    boolean isDeleted = f.delete();

    if (!isDeleted) {
      throw new CannotDeletePersonException();
    }

  }
  //STEP 12 - END

  @Override
  public Person[] readAll() throws IOException, InputValidationException {

    File f = new File(dirPath);
    String[] files = f.list();

    Person[] persons = new Person[files.length]; //STEP 15

    for (int i = 0; i < files.length; i++) {

      //STEP 12 - START
      //STEP 13 - START
      try {
        persons[i] = dbFileContentToPerson(files[i]); //STEP 15
      } catch (InputValidationException e) {
        System.err.println("Cannot read file with index:" + i);
        throw e;
      }

      //STEP 13 - END
      //STEP 12 - END
    }

    return persons;
  }

  private int getFileIndex(String filename) {

    String strIndex = filename.substring(0, filename.lastIndexOf("."));
    int index = Integer.parseInt(strIndex);

    return index;
  }

  //STEP 12 - START
  private Person dbFileContentToPerson(String filename)
      throws IOException, InputValidationException {

    FileInputStream fin = new FileInputStream(dirPath + "/" + filename);

    BufferedReader bin = new BufferedReader(new InputStreamReader(fin));

    String json = "";

    while (true) {
      String line = bin.readLine();
      if (line == null) {
        break;
      }
      json += line;
    }

    Gson gson = new Gson();

    return gson.fromJson(json, Person.class);

//    int c;
//
//    String line = "";
//    String className = "";
//    Contact[] contacts = new Contact[10];
//    int contactsCounter = 0;
//    PersonalInfo[] personalInfos = new PersonalInfo[10];
//    int personalInfosCounter = 0;
//
//    while (true) {
//
//      c = fin.read();
//
//      if (c < 0 || c == '\n') {
//
//        if (line != null && !line.isEmpty()) {
//          if (line.contains("class")) {
//
//            String[] keyValue = line.split(":");
//            className = keyValue[1];
//
//            if (className.equals("Birthday")) {
//              personalInfos[personalInfosCounter] = new Birthday();
//              personalInfosCounter++;
//            } else if (className.equals("Name")) {
//              personalInfos[personalInfosCounter] = new Name();
//              personalInfosCounter++;
//            } else if (className.equals("Profession")) {
//              personalInfos[personalInfosCounter] = new Profession();
//              personalInfosCounter++;
//            } else if (className.equals("Sex")) {
//              personalInfos[personalInfosCounter] = new Sex();
//              personalInfosCounter++;
//            }
//
//            if (className.equals("Email")) {
//              contacts[contactsCounter] = new Email();
//              contactsCounter++;
//            } else if (className.equals("FacebookAccount")) {
//              contacts[contactsCounter] = new FacebookAccount();
//              contactsCounter++;
//            } else if (className.equals("MobilePhone")) {
//              contacts[contactsCounter] = new MobilePhone();
//              contactsCounter++;
//            } else if (className.equals("TwitterAccount")) {
//              contacts[contactsCounter] = new TwitterAccount();
//              contactsCounter++;
//            } else if (className.equals("VkAccount")) {
//              contacts[contactsCounter] = new VkAccount();
//              contactsCounter++;
//            }
//
//          } else {
//
//            String[] keyValue = line.split(":");
//
//            //STEP 12 - START - UPDATE
//            switch (className) {
//              case "Name":
//              case "Birthday":
//              case "Profession":
//              case "Sex":
//                personalInfos[personalInfosCounter - 1].setFieldValue(keyValue[0], keyValue[1]);
//            }
//
//            switch (className) {
//              case "Email":
//              case "FacebookAccout":
//              case "MobilePhone":
//              case "TwitterAccount":
//              case "VkAccount":
//                contacts[contactsCounter - 1].setFieldValue(keyValue[0], keyValue[1]);
//            }
//            //STEP 12 - END - UPDATE
//
//          }
//        }
//
//        line = "";
//
//        if (c < 0) { //end of file, EOF symbol
//          break; //do not append EOF symbol to the 'line' string
//        } else if (c == '\n') { //end of line, NEW LINE symbol
//          continue; //don't append to the 'line' string NEW LINE symbol
//        }
//      }
//
//      line += (char) c;
//    }
//
//    Person person = new Person();
//
//    person.setId(getFileIndex(filename));
//
//    for (int j = 0; j < personalInfosCounter; j++) {
//      person.addPersonalInfo(personalInfos[j]);
//    }
//
//    for (int j = 0; j < contactsCounter; j++) {
//      person.addContact(contacts[j]);
//    }
//
//    fin.close();
//
//    return person;
  }
  //STEP 12 - END
}