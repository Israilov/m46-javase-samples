package com.m46academy.telbook.database;

import com.m46academy.telbook.exception.CannotDeletePersonException;
import com.m46academy.telbook.exception.InputValidationException;
import com.m46academy.telbook.exception.PersonNotFoundException;
import com.m46academy.telbook.model.Person;

import java.io.IOException;

public interface Database {
    // CRUD - Create Read Update Delete
    void createPerson(Person person) throws IOException;
    Person readPerson(int index) throws PersonNotFoundException, IOException, InputValidationException;
    void updatePerson(Person person) throws PersonNotFoundException, IOException;
    void deletePerson(int index) throws PersonNotFoundException, CannotDeletePersonException;
    Person[] readAll() throws IOException, InputValidationException;
}
