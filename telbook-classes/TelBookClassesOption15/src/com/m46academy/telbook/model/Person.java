package com.m46academy.telbook.model;

import com.m46academy.telbook.model.contact.*;
import com.m46academy.telbook.model.personalinfo.*;

public class Person implements Searchable { //STEP 13 MODIFICATION

    private int id;

    private PersonalInfo[] personalInfos = new PersonalInfo[10];
    private int lastFreeCellInPersonalInfos = 0;

    private Contact[] contacts = new Contact[10];
    private int lastFreeCellInContacts = 0;

    public PersonalInfo[] getPersonalInfos() {
        return personalInfos;
    }

    public void setPersonalInfos(PersonalInfo[] personalInfos) {
        this.personalInfos = personalInfos;
    }

    public Contact[] getContacts() {
        return contacts;
    }

    public void setContacts(Contact[] contacts) {
        this.contacts = contacts;
    }

    public void addPersonalInfo(PersonalInfo personalInfo) {
        personalInfos[lastFreeCellInPersonalInfos] = personalInfo;
        lastFreeCellInPersonalInfos++;
    }

    public void addContact(Contact contact) {
        contacts[lastFreeCellInContacts] = contact;
        lastFreeCellInContacts++;
    }

    public String toString() {

        String str = "Personal Info: \n";

        int i;

        for (i = 0; i < personalInfos.length; i++) {
            if (personalInfos[i] != null) {
                str += personalInfos[i].toString() + "\n";
            }
        }

        if (i == 0) {
            str += "No Personal Info Found";
        }

        str += "Contacts Info: \n";

        int j;

        for (j = 0; j < contacts.length; j++) {
            if (contacts[j] != null) {
                str += contacts[j].toString() + "\n";
            }
        }

        if (j == 0) {
            str += "No Contacts Found";
        }

        return str;
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {

        for (int i = 0; i < lastFreeCellInPersonalInfos; i++) {
            Searchable searchable = personalInfos[i];
            if (searchable.search(keyword)) {
                return true;
            }
        }

        for (int i = 0; i < lastFreeCellInContacts; i++) {
            Searchable searchable = contacts[i];
            if (searchable.search(keyword)) {
                return true;
            }
        }

        return false;
    }

    public Contact getContactByClass(Class clazz) {

        if (contacts != null) {

            if (clazz != null) {

                for (int i = 0; i < lastFreeCellInContacts; i++) {
                    if (clazz.equals(contacts[i].getClass())) {
                        return contacts[i];
                    }
                }

            }
        }

        return null;
    }

    public PersonalInfo getPersonalInfoByClass(Class clazz) {

        if (personalInfos != null) {

            if (clazz != null) {

                for (int i = 0; i < lastFreeCellInPersonalInfos; i++) {
                    if (personalInfos[i].getClass().equals(clazz)) {
                        return personalInfos[i];
                    }
                }

            }
        }

        return null;
    }

    //STEP 14 - START
    public int compareTo(Person otherPerson, Class clazz) {

        PersonalInfo pinfoA = this.getPersonalInfoByClass(clazz);
        PersonalInfo pinfoB = otherPerson.getPersonalInfoByClass(clazz);

        Contact contactA = this.getContactByClass(clazz);
        Contact contactB = otherPerson.getContactByClass(clazz);

        if (pinfoA == null && pinfoB == null && contactA == null && contactB == null) {
            return 0;
        } else {

            if (pinfoA != null || pinfoB != null) {

                if (pinfoA != null && pinfoB == null) {
                    return 1;
                } else if (pinfoA == null && pinfoB != null) {
                    return -1;
                } else {
                    return pinfoA.compareTo(pinfoB);
                }

            } else {

                if (contactA != null && contactB == null) {
                    return 1;
                } else if (contactA == null && contactB != null) {
                    return -1;
                } else {
                    return contactA.compareTo(contactB);
                }

            }

        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //STEP 14 - END

} //STEP 13 - END
