package com.m46academy.telbook.model.contact;

import com.m46academy.telbook.exception.InputValidationException;

public class TwitterAccount extends Contact {

    private String username;

    public String getUsername() {
        return username;
    }

    //STEP 13 - START
    public void setUsername(String username) throws InputValidationException {

        //TODO
        if (username.matches("@[a-zA-Z0-9_]+")) { //any username that is like @Obama2014_, i.e. contains letters, numbers, _ sign and starts with @ sign
            this.username = username;
        } else {
            throw new InputValidationException("username", username);
        }

    }
    //STEP 13 - END

    public String toString() {
        return "Twitter username: " + username;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException  {
        setUsername(value);
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return username.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}