package com.m46academy.telbook.model.contact;

import com.m46academy.telbook.exception.InputValidationException;

public class MobilePhone extends Contact {

    private String countryCode; //+7, +0, +1...
    private String msisdn; //7015657645

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String toString() {
        return "Mobile phone: " + countryCode + " " + msisdn;
    }


    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {

        switch (fieldName) {
            case "countryCode":
                countryCode = value;
                break;
            case "msisdn":
                msisdn = value;
                break;
        }
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return (countryCode+msisdn).contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}
