package com.m46academy.telbook.model.contact;

import com.m46academy.telbook.model.AnyFieldValueSetter;
import com.m46academy.telbook.model.Comparable;
import com.m46academy.telbook.model.Searchable;

public abstract class Contact implements AnyFieldValueSetter, Searchable, Comparable {

}
