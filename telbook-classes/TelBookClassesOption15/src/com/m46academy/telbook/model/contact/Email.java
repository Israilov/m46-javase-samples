package com.m46academy.telbook.model.contact;

import com.m46academy.telbook.exception.InputValidationException;

public class Email extends Contact {

    private String username; //maxim.semenov
    private String mailProvider; //gmail.com

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMailProvider() {
        return mailProvider;
    }

    public void setMailProvider(String mailProvider) {
        this.mailProvider = mailProvider;
    }

    public String toString() {
        return "Email: " + username + "@" + mailProvider;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {
        switch (fieldName) {
            case "username":
                username = value;
                break;
            case "mailProvider":
                mailProvider = value;
                break;
        }
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return username.contains(keyword) || mailProvider.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {

        Email email = (Email) obj;

        return email.getUsername().compareTo(this.getUsername());
    }
    //STEP 13 - END
}
