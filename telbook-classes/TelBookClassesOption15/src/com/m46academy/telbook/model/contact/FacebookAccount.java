package com.m46academy.telbook.model.contact;

import com.m46academy.telbook.exception.InputValidationException;

public class FacebookAccount extends Contact {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String toString() {
        return "Facebook username: "+username;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {
        username = value;
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return username.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}
