package com.m46academy.telbook.model;

import com.m46academy.telbook.exception.InputValidationException;

//STEP 12 - START
public interface AnyFieldValueSetter {
    void setFieldValue(String fieldName, String value)
            //STEP 13 - START
            throws InputValidationException;
            //STEP 13 - END
}
//STEP 12 - END
