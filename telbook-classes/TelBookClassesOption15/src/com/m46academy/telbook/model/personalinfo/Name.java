package com.m46academy.telbook.model.personalinfo;

import com.m46academy.telbook.exception.InputValidationException;

public class Name extends PersonalInfo {

    private String firstname;
    private String surname;
    private String paternal;
    private String maternal;
    private String prefix; //Mr, Mrs, Bey, Aga, Professor

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPaternal() {
        return paternal;
    }

    public void setPaternal(String paternal) {
        this.paternal = paternal;
    }

    public String getMaternal() {
        return maternal;
    }

    public void setMaternal(String maternal) {
        this.maternal = maternal;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String toString() {
        return "Name: "+prefix+" "+surname+" "+firstname+" "+paternal+", mother surname: "+maternal;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {
        switch (fieldName) {
            case "firstname": firstname = value; break;
            case "surname": surname = value; break;
            case "paternal": paternal = value; break;
            case "maternal": maternal = value; break;
            case "prefix": prefix = value; break;
        }
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return firstname.contains(keyword) ||
                surname.contains(keyword)  ||
                paternal.contains(keyword) ||
                maternal.contains(keyword) ||
                prefix.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}