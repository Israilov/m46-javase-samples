package com.m46academy.telbook.model.personalinfo;

import com.m46academy.telbook.exception.InputValidationException;

public class Profession extends PersonalInfo {

    private String professionGroup; //medical
    private String professionName; //dentist

    public String getProfessionGroup() {
        return professionGroup;
    }

    public void setProfessionGroup(String professionGroup) {
        this.professionGroup = professionGroup;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    public String toString() {
        return "Profession: "+professionName+"/"+professionGroup;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {
        switch (fieldName) {
            case "professionalGroup": professionGroup = value; break;
            case "professionName": professionName = value; break;
        }
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return professionName.contains(keyword) || professionGroup.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}
