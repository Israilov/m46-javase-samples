package com.m46academy.telbook.model.personalinfo;

import com.m46academy.telbook.exception.InputValidationException;

public class Sex extends PersonalInfo {

    private String type; //w, m

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "Sex: "+type;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException {
        type = value;
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {
        return type.contains(keyword);
    }

    @Override
    public int compareTo(Object obj) {
        return 0; //TODO STEP 14
    }
    //STEP 13 - END
}
