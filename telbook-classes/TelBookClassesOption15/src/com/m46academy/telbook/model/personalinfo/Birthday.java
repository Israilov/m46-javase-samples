package com.m46academy.telbook.model.personalinfo;

import com.m46academy.telbook.exception.InputValidationException;

public class Birthday extends PersonalInfo {

    private int day;
    private int month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) throws InputValidationException {
        if (day > 0 && day < 31) {
            this.day = day;
        } else {
            //STEP 13 - START
            InputValidationException exception = new InputValidationException("day", String.valueOf(day));
            throw exception;
            //STEP 13 - END
        }
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString() {
        return "Birthday: " + day + "/" + month + "/" + year;
    }

    @Override
    public void setFieldValue(String fieldName, String value) throws InputValidationException { //value="100"

        switch (fieldName) {
            case "day":
                setDay(Integer.parseInt(value)); //STEP 13 MODIFICATION, before: day = Integer.parseInt(value)
                break;
            case "month":
                month = Integer.parseInt(value);
                break;
            case "year":
                year = Integer.parseInt(value);
                break;
        }
    }

    //STEP 13 - START
    @Override
    public boolean search(String keyword) {

        if (keyword.matches("[0-9]+")) {

            int someNumber = Integer.parseInt(keyword);

            if (someNumber == day || someNumber == month || someNumber == year) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    @Override
    public int compareTo(Object obj) {

        Birthday other = (Birthday) obj;

        if (this.getYear() > other.getYear()) {
            return 1;
        } else if (this.getYear() == other.getYear()) {
            if (this.getMonth() > other.getMonth()) {
                return 1;
            } else if (this.getMonth() == other.getMonth()) {
                if (this.getDay() > other.getDay()) {
                    return 1;
                } else if (this.getDay() == other.getDay()) {
                    return 0;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            return -1;
        }

    }
    //STEP 13 - END
}
