package com.m46academy.telbook.model.personalinfo;

import com.m46academy.telbook.model.AnyFieldValueSetter;
import com.m46academy.telbook.model.Comparable;
import com.m46academy.telbook.model.Searchable;

public abstract class PersonalInfo implements AnyFieldValueSetter, Searchable, Comparable {

}
