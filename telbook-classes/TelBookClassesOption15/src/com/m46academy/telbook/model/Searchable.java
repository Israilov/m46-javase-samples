package com.m46academy.telbook.model;

public interface Searchable {

    boolean search(String keyword);

}
