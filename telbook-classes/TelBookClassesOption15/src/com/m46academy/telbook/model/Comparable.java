package com.m46academy.telbook.model;

public interface Comparable {

    int compareTo(Object obj);
}
