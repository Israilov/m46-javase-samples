public class ClassAMain {

  public static void main(String[] args) {
    int a = 9;
    String s = "abc";
    double d = 8.8;

    Box b = new Box();

    b.height = 10;
    b.depth = 45;
    b.width = 25;

    Box b1 = new Box();

    b1.height = 89;
    b1.depth = 78;
    b1.width = 56;

  }

}

class Box {

  int height;
  int depth;
  int width;

}
