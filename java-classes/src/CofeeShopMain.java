public class CofeeShopMain {

  public static void main(String[] args) {

    CoffeeShop coffeeShopObjectA = new CoffeeShop();
    coffeeShopObjectA.coffeeShopName = "Gloria Jeans";
    coffeeShopObjectA.coffeeShopAddress = "Abylay Khan str.";
    coffeeShopObjectA.numberOfRooms = 2;

    CoffeeShop coffeeShopObjectB = new CoffeeShop();
    coffeeShopObjectB.coffeeShopName = "Gloria Jeans";
    coffeeShopObjectB.coffeeShopAddress = "Panfilov str.";
    coffeeShopObjectB.numberOfRooms = 3;

    CoffeeShop coffeeShopObjectC = new CoffeeShop();
    coffeeShopObjectC.coffeeShopName = "Marono Rosso";
    coffeeShopObjectC.coffeeShopAddress = "Baytursynova str.";
    coffeeShopObjectC.numberOfRooms = 4;

    coffeeShopObjectA.numberOfVisitors++;
    coffeeShopObjectA.numberOfVisitors++;

    coffeeShopObjectB.numberOfVisitors++;

    System.out.println("Coffee Shop Name: "+coffeeShopObjectA.coffeeShopName);
    System.out.println("Coffee Shop Address: "+coffeeShopObjectA.coffeeShopAddress);
    System.out.println("Number of Rooms: "+coffeeShopObjectA.numberOfRooms);
    System.out.println("Visitors: "+coffeeShopObjectA.numberOfVisitors);

    System.out.println();

    System.out.println("Coffee Shop Name: "+coffeeShopObjectB.coffeeShopName);
    System.out.println("Coffee Shop Address: "+coffeeShopObjectB.coffeeShopAddress);
    System.out.println("Number of Rooms: "+coffeeShopObjectB.numberOfRooms);
    System.out.println("Visitors: "+coffeeShopObjectB.numberOfVisitors);

  }

}
