public class HumanoidComplexRobot {

  private Hand rightHand;
  private Hand leftHand;
  private Leg rightLeg;
  private Leg leftLeg;
  private boolean isRightLegInFront = true;
  private boolean isLeftLegInFront = false;

  public void rightHandPull() {
    rightHand.grab();
    rightHand.moveBackward();
  }

  public void leftHandPull() {

  }

  public void stepForward() {

    if (isRightLegInFront) {
      leftLeg.stepForward();
    } else if (isLeftLegInFront) {
      rightLeg.stepForward();
    }

  }

  public void stepBackward() {

    if (isRightLegInFront) {
      leftLeg.stepBackword();
    } else if (isLeftLegInFront) {
      rightLeg.stepBackword();
    }

  }

}
