import java.util.Scanner;

public class HumanoidRobotMain {

  public static void main(String[] args) {

    HumanoidRobot robot = new HumanoidRobot();

    robot.stepBack();
    robot.stepForward();

    Scanner scan = new Scanner(System.in);

    String s = scan.next();

    System.out.println("Line read: "+s);

  }

}
