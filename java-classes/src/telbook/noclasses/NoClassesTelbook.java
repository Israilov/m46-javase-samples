package telbook.noclasses;

import java.util.Scanner;

public class NoClassesTelbook {

  public static void main(String[] args) {

    String[][] contacts = new String[5][100];
    int last = 0;

    while (true) {

      System.out.println("What do you whish to do?");
      System.out.println("1. List");
      System.out.println("2. Add");

      Scanner scan = new Scanner(System.in);

      String action = scan.next();

      if (action.equalsIgnoreCase("List")) {
        System.out.println("surname: \t\t name: \t\t tel: \t\t fbacc: \t\t twitter:");

        for (int i=0; i<last; i++) {
          System.out.println(contacts[0][i]+" \t\t"
              +contacts[1][i]
              +" \t\t"+contacts[2][i]
              +" \t\t"+contacts[3][i]
              +" \t\t"+contacts[4][i]);
        }

      } else if (action.equalsIgnoreCase("Add")) {

        System.out.println("name: ");
        String name = scan.next();
        System.out.println("surname: ");
        String surname = scan.next();
        System.out.println("facebook account: ");
        String fbacc = scan.next();
        System.out.println("twitter account: ");
        String twitter = scan.next();
        System.out.println("tel: ");
        String tel = scan.next();

        contacts[0][last] = name;
        contacts[1][last] = surname;
        contacts[2][last] = tel;
        contacts[3][last] = fbacc;
        contacts[4][last] = twitter;

        last++;

      }

    }

  }

}
