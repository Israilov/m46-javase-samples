public class CofeeShopMainPrivate {

  public static void main(String[] args) {

    CoffeeShopPrivate coffeeShopObjectA = new CoffeeShopPrivate();
    coffeeShopObjectA.coffeeShopName = "Gloria Jeans";
    coffeeShopObjectA.coffeeShopAddress = "Abylay Khan str.";
    coffeeShopObjectA.numberOfRooms = 2;

    CoffeeShopPrivate coffeeShopObjectB = new CoffeeShopPrivate();
    coffeeShopObjectB.coffeeShopName = "Gloria Jeans";
    coffeeShopObjectB.coffeeShopAddress = "Panfilov str.";
    coffeeShopObjectB.numberOfRooms = 3;

    CoffeeShopPrivate coffeeShopObjectC = new CoffeeShopPrivate();
    coffeeShopObjectC.coffeeShopName = "Marono Rosso";
    coffeeShopObjectC.coffeeShopAddress = "Baytursynova str.";
    coffeeShopObjectC.numberOfRooms = 4;

    coffeeShopObjectA.incrementVisitors();
    coffeeShopObjectA.incrementVisitors();
    coffeeShopObjectB.incrementVisitors();

    System.out.println("Coffee Shop Name: " + coffeeShopObjectA.coffeeShopName);
    System.out.println("Coffee Shop Address: " + coffeeShopObjectA.coffeeShopAddress);
    System.out.println("Number of Rooms: " + coffeeShopObjectA.numberOfRooms);
    System.out.println("Visitors: " + coffeeShopObjectA.getNumberOfVisitors());

    System.out.println();

    System.out.println("Coffee Shop Name: " + coffeeShopObjectB.coffeeShopName);
    System.out.println("Coffee Shop Address: " + coffeeShopObjectB.coffeeShopAddress);
    System.out.println("Number of Rooms: " + coffeeShopObjectB.numberOfRooms);
    System.out.println("Visitors: " + coffeeShopObjectB.getNumberOfVisitors());

  }

}
