public class CoffeeShopPrivate {

  public String coffeeShopName;
  public String coffeeShopAddress;
  public int numberOfRooms;
  private int numberOfVisitors;

  public void incrementVisitors() {

    if (numberOfVisitors<1) {
      numberOfVisitors++;
    } else {
      System.out.println("No more places in "+coffeeShopName);
    }

  }

  public int getNumberOfVisitors() {
    return numberOfVisitors;
  }

}
