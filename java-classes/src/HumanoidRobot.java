public class HumanoidRobot {

  private int x;

  public void stepBack() {
    if ((x-1)!=-1) {
      x--;
    }
  }

  public void stepForward() {
    if ((x+1)!=6) {
      x++;
    }
  }

}
