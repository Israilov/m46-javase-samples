package contact;

public class ContactArrayMain {

  public static void main(String[] args) {

    Contact[] contacts = new Contact[10];

    contacts[0] = new Contact();
    contacts[0].name = "Alex";
    contacts[0].surname = "Mc Genry";
    contacts[0].address = "Oxford Str. 124";
    contacts[0].phoneNumber = "+180111234567";

    contacts[1] = new Contact();
    contacts[1].name = "John";
    contacts[1].surname = "Sweet";
    contacts[1].address = "Oxford Str. 125";
    contacts[1].phoneNumber = "+180111234577";

    System.out.println(contacts[0].name);
    System.out.println(contacts[0].surname);
    System.out.println(contacts[0].address);
    System.out.println(contacts[0].phoneNumber);

    System.out.println(contacts[1].name);
    System.out.println(contacts[1].surname);
    System.out.println(contacts[1].address);
    System.out.println(contacts[1].phoneNumber);

  }

}
