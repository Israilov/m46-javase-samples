package house;

public class HouseMain {

  public static void main(String[] args) {

    House house1 = new House("Oxford street 123", "red", 4, 5);

//    house1.address = "Oxford street 124";
//    house1.color = "red";
//    house1.roomsNumber = 4;
//    house1.windowsNumber = 5;

    House house2 = new House();

    house2.address = "Oxford street 125";
    house2.setColor("red");
    house2.roomsNumber = 4;
    house2.windowsNumber = 5;

    House house3 = new House();

    house3.address = "Oxford street 126";
    house3.setColor("rad");
    house3.roomsNumber = 4;
    house3.windowsNumber = 5;

    System.out.println(house1.address+", "+house1.getColor()+", "+house1.roomsNumber+", "+house1.windowsNumber);
    System.out.println(house2.address+", "+house2.getColor()+", "+house2.roomsNumber+", "+house2.windowsNumber);
    System.out.println(house3.address+", "+house3.getColor()+", "+house3.roomsNumber+", "+house3.windowsNumber);

  }
}
