package house;

public class House {

  public String address;
  private String color;
  public int roomsNumber;
  public int windowsNumber;

  public House(String address, String color, int roomsNumber, int windowsNumber) {
    this.address = address;
    this.color = color;
    this.roomsNumber = roomsNumber;
    this.windowsNumber = windowsNumber;
  }

  public House() {

  }

  public void setColor(String color) {
     if (color.equals("red") || color.equals("green")) {
       this.color = color;
     }
  }

  public String getColor() {
    return color;
  }

}
