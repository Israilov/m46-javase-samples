package academy.m46.oop.sample1;

public class TwoDimObjectsPolymorphism {
    public static void main(String[] args) {

        AbstractTwoDimObject[] objectsArr = new AbstractTwoDimObject[6];

        objectsArr[0] = new Circle(10.5);
        objectsArr[1] = new Rectangle(4.5,8.5);
        objectsArr[2] = new Square(12.5);
        objectsArr[3] = new Line(30.5);
        objectsArr[4] = new Circle(12.5);
        objectsArr[5] = new Rectangle(11.5, 12.5);

        for (int i=0; i<objectsArr.length; i++) {
            System.out.println("Object type: \""+objectsArr[i].getClass().getSimpleName()+"\", area="+objectsArr[i].area());
        }

    }
}
