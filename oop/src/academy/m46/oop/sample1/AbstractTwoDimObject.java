package academy.m46.oop.sample1;

public abstract class AbstractTwoDimObject {
    public abstract double area();
}
