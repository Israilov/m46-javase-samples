package academy.m46.oop.sample1;

public class Square extends AbstractTwoDimObject {

    private double aSideLength;

    public Square(double aSideLength) {
        this.aSideLength = aSideLength;
    }

    @Override
    public double area() {
        return aSideLength*aSideLength;
    }

    public double getaSideLength() {
        return aSideLength;
    }

    public void setaSideLength(double aSideLength) {
        this.aSideLength = aSideLength;
    }

}
