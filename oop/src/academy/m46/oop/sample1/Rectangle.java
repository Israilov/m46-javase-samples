package academy.m46.oop.sample1;

public class Rectangle extends AbstractTwoDimObject {

    private double aSideLength;
    private double bSideLength;

    public Rectangle(double aSideLength, double bSideLength) {
        this.aSideLength = aSideLength;
        this.bSideLength = bSideLength;
    }

    @Override
    public double area() {
        return aSideLength*bSideLength;
    }

    public double getaSideLength() {
        return aSideLength;
    }

    public void setaSideLength(double aSideLength) {
        this.aSideLength = aSideLength;
    }

    public double getbSideLength() {
        return bSideLength;
    }

    public void setbSideLength(double bSideLength) {
        this.bSideLength = bSideLength;
    }

}
