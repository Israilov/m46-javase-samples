package academy.m46.oop.sample1;

public class Line extends AbstractTwoDimObject {

    private double length;

    public Line(double length) {
        this.length = length;
    }

    @Override
    public double area() {
        return 0;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

}
