package academy.m46.oop.sample2;

public class StarredFiguresApp {

    public static void main(String[] args) {

        AbstractStarredFigure[] starredFigures = new AbstractStarredFigure[3];

        starredFigures[0] = new StarredTriangle(10);
        starredFigures[1] = new StarredRectangle(12, 13);
        starredFigures[2] = new StarredRectangleAndLine(8,8, 10);

        for (int i=0; i<starredFigures.length; i++) {
            starredFigures[i].drawFigure();
            System.out.println();
        }

    }

}
