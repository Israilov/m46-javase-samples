package academy.m46.oop.sample2;

public class StarredRectangle extends AbstractStarredFigure {

    private int aSideSize;
    private int bSideSize;

    public StarredRectangle(int aSideSize, int bSideSize) {
        this.aSideSize = aSideSize;
        this.bSideSize = bSideSize;
    }

    @Override
    public void drawFigure() {

        for (int i=0; i<aSideSize; i++) {
            for (int j=0; j<bSideSize; j++) {
                System.out.print('*');
            }
            System.out.println();
        }

    }

    public int a(){
        return 1;
    }

    public int getaSideSize() {
        return aSideSize;
    }

    public void setaSideSize(int aSideSize) {
        this.aSideSize = aSideSize;
    }

    public int getbSideSize() {
        return bSideSize;
    }

    public void setbSideSize(int bSideSize) {
        this.bSideSize = bSideSize;
    }
}
