package academy.m46.oop.sample2;

public abstract class AbstractStarredFigure {

    public abstract void drawFigure();

}
