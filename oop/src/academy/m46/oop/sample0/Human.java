public class Human {

  public String name;
  public String surname;
  private int age;

  public void setAge(int a) {
    age = a;
  }

  public void increment() {
    age = age+1;
  }

  public void decrement() {
    age = age-1;
  }

  public int getAge() {
    return age;
  }

}