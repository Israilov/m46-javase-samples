public class SimpleClassMethod {

  public static void main(String[] args) {

    Human h = new Human();

    h.name = "Vasiliy";
    h.surname = "Vasilyev";
    h.setAge(17);

    Human d = new Human();

    d.name = "";
    d.surname = "";
    d.setAge(20);

    h.increment();
    h.increment();

    System.out.println(h.getAge());

    h.increment();
    h.increment();

    System.out.println(h.getAge());

    h.decrement();

  }

}
