public class SimpleStaticMethod {

    public static void main(String[] args) {

        int a1 = parabola(5);
        int a2 = parabola(6);
        int a3 = parabola(7);
        int a4 = parabola(8);

        System.out.println(a1);
        System.out.println(a2);
        System.out.println(a3);
        System.out.println(a4);

    }

    public static int parabola(int x) {
        int result = 3*x*x+2;
        return result;
    }

}
