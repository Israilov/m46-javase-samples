package academy.m46.oop.sample3;

public class StarredRectangleAndLine extends StarredRectangle {

    private int lineSize;

    public StarredRectangleAndLine(int aSideSize, int bSideSize, int lineSize) {
        super(aSideSize, bSideSize);
        this.lineSize = lineSize;
    }

    @Override
    public void drawFigure() {

        super.drawFigure();

        System.out.println();

        for (int i=0; i<lineSize; i++) {
            System.out.print('*');
        }

    }

    public int a(){
        int z = super.a();
        return z++;
    }
}
