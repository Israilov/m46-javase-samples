package academy.m46.oop.sample3;

public class StarredTriangle implements ICompound {

    private double sideSize;

    public StarredTriangle(double sideSize) {
        this.sideSize = sideSize;
    }

    @Override
    public void drawFigure() {

        for (int i=0; i<sideSize; i++) {
            for (int j=0; j<i+1; j++) {
                System.out.print('*');
            }
            System.out.println();
        }

    }

    public double getSideSize() {
        return sideSize;
    }

    public void setSideSize(double sideSize) {
        this.sideSize = sideSize;
    }

    @Override
    public void doSomething() {
        System.out.println("Did something");
    }
}
